package com.unoiatech.outofoptions.util.BankId;

import android.content.Context;

import com.unoiatech.outofoptions.R;

/**
 * Created by unoiaAndroid on 4/4/2017.
 */

public class ExceptionHelper {

    private  Context context;

    public ExceptionHelper(Context current){
        this.context = current;
    }


    public String getErrorMessage(String constant) {

        switch (constant)
        {

            case "OUTSTANDING_TRANSACTION":
                 return context.getResources().getString(R.string.RFA1);

            case "NO_CLIENT":
                return context.getResources().getString(R.string.RFA1);

            case "ALREADY_IN_PROGRESS":
                return context.getResources().getString(R.string.RFA3);

            case "CANCELLED":
                return context.getResources().getString(R.string.RFA3);

            case "RETRY":
                return context.getResources().getString(R.string.RFA5);

            case "INTERNAL_ERROR":
                return context.getResources().getString(R.string.RFA5);

            case  "USER_CANCEL":
                return context.getResources().getString(R.string.RFA6);

            case "EXPIRED_TRANSACTION":
                return context.getResources().getString(R.string.RFA8);

            case "USER_SIGN":
                return context.getResources().getString(R.string.RFA9);

            case "CLIENT_ERR":
                return context.getResources().getString(R.string.RFA12);

            case "CERTIFICATE_ERR":
                return context.getResources().getString(R.string.RFA16);

            case "START_FAILED":
                return context.getResources().getString(R.string.RFA17);

            case "STARTED_A":
                return context.getResources().getString(R.string.RFA1);

            case "STARTED_B":
                return context.getResources().getString(R.string.RFA1);

            case "STARTED_C":
                return context.getResources().getString(R.string.RFA1);

            case "STARTED_D":
                return context.getResources().getString(R.string.RFA1);

             default:
                 return constant;

        }
    }
}
