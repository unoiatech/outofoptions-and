package com.unoiatech.outofoptions.util.locationhelper;



import android.Manifest;
import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 1/19/2018.
 */

public class LocationHelper {


    void checkPermissions(Context con) {

        ArrayList<String> permissions = new ArrayList<>();
        PermissionUtils permissionUtils;

        permissionUtils = new PermissionUtils(con);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionUtils.check_permission(permissions, "Need GPS permission for getting your location", 1);


    }

}
