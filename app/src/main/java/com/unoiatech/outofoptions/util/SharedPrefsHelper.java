package com.unoiatech.outofoptions.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;

import com.allenliu.badgeview.BadgeFactory;
import com.allenliu.badgeview.BadgeView;
import com.google.gson.Gson;
import com.google.gson.internal.Streams;
import com.google.gson.reflect.TypeToken;
import com.unoiatech.outofoptions.Application;
import com.unoiatech.outofoptions.model.Login;
import com.unoiatech.outofoptions.model.SearchJobsRequest;
import com.unoiatech.outofoptions.model.SearchRequest;
import com.unoiatech.outofoptions.model.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by unoiaAndroid on 4/5/2017.
 */

public class SharedPrefsHelper {
    private static SharedPrefsHelper instance;
    public static final String USER_INFO_PREF = "userinfo";
    public static final String FILTER_OPTIONS_PREF = "filteroptions";
    private static final String USER_PREF_OBJECT = "user";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String DISTANCE = "distance";
    private static final String CATEGORY = "category";
    private static final String MAX_BUDGET = "max_budget";
    private static final String MIN_BUDGET = "min_budget";
    private static final String RATING = "rating";
    private static final String CATEGORY_TEXT = "category_text";
    private static final String LOCATION_TEXT = "location_text";
    private static final String FCM_PREF_OBJECT = "fcmId";
    private static final String ADD_ABOUT = "about_data";
    private static final String NOTIFICATION_PREF_OBJECT = "notification";
    private static final String STAFF_MEMBER_IDS = "member_ids";
    private static final String STAFF_MEMBERS_NAME = "members_name";

    //Job Location data
    private static final String JOB_LAT = "lat";
    private static final String JOB_LNG = "lng";
    private static final String JOB_PLACE = "place";

    //User Location
    private static final String BUSINESS_DAY = "day";
    private static final String STARTING_HOUR = "starting";
    private static final String ENDING_HOUR = "ending";

    private SharedPreferences sharedPreferences;

    public static synchronized SharedPrefsHelper getInstance(String prefName) {
        if (instance == null) {
            instance = new SharedPrefsHelper(prefName);
        }
        return instance;
    }

    //prefName : pass the shared Pref name you need to access
    private SharedPrefsHelper(String prefName) {
        instance = this;
        sharedPreferences = Application.getInstance().getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }


    /***************
     * user pref
     *********/
    public User readUserPref() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(USER_PREF_OBJECT, "");
        User user = gson.fromJson(json, User.class);
        return user;
    }

    public void writeUserPref(User user) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString(USER_PREF_OBJECT, json);
        prefsEditor.commit();
    }

    /***************
     * filter pref
     *********/
    public Object readSearchRequestByKey(String key) {
        Object object = null;
        if (key.equals(MAX_BUDGET))
            object = sharedPreferences.getInt(MAX_BUDGET, 10000);
        if (key.equals(MIN_BUDGET))
            object = sharedPreferences.getInt(MIN_BUDGET, 0);
        if (key.equals(CATEGORY)) {
            String categories = sharedPreferences.getString(CATEGORY, null);
            Log.e("TAG", "ReadjsonCars = " + categories);
            // object = new Gson().fromJson(categories,ArrayList.class);
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            object = new Gson().fromJson(categories, type);
        }
        if (key.equals(DISTANCE)) {
            object = Double.longBitsToDouble(sharedPreferences.getLong(DISTANCE, 0));
            Log.d("TAG", "DISTANCE read = " + object);
        }
        if (key.equals(LATITUDE))
            object = Double.longBitsToDouble(sharedPreferences.getLong(LATITUDE, 0));
        if (key.equals(LONGITUDE))
            object = Double.longBitsToDouble(sharedPreferences.getLong(LONGITUDE, 0));
        if (key.equals(RATING))
            object = Double.longBitsToDouble(sharedPreferences.getLong(RATING, 0));
        if (key.equals(CATEGORY_TEXT))
            object = sharedPreferences.getString(CATEGORY_TEXT, "All");
        if (key.equals(LOCATION_TEXT))
            object = sharedPreferences.getString(LOCATION_TEXT, "");
        return object;
    }

    public SearchRequest readSearchRequest() {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setMaxBudget(sharedPreferences.getInt(MAX_BUDGET, 10000));
        searchRequest.setMinBudget(sharedPreferences.getInt(MIN_BUDGET, 100));
        searchRequest.setLatitude(Double.longBitsToDouble(sharedPreferences.getLong(LATITUDE, 0)));
        searchRequest.setLongitude(Double.longBitsToDouble(sharedPreferences.getLong(LONGITUDE, 0)));
        searchRequest.setDistance(Double.longBitsToDouble(sharedPreferences.getLong(DISTANCE, 0)));
        searchRequest.setRating(Double.longBitsToDouble(sharedPreferences.getLong(RATING, 0)));
        searchRequest.setCategoryText(sharedPreferences.getString(CATEGORY_TEXT, "All"));
        searchRequest.setLocationText(sharedPreferences.getString(LOCATION_TEXT, ""));

        //reading arraylist from shared prefernce as string
        String jsonFavorites = sharedPreferences.getString(CATEGORY, null);
        ArrayList<String> categories = new Gson().fromJson(jsonFavorites, ArrayList.class);
        searchRequest.setCategory(categories);
        return searchRequest;
    }

    public void writeSearchRequest(String key, Object value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        if (key.equals(MAX_BUDGET))
            prefsEditor.putInt(MAX_BUDGET, (Integer) value);
        if (key.equals(MIN_BUDGET))
            prefsEditor.putInt(MIN_BUDGET, (Integer) value);
        if (key.equals(CATEGORY)) {
            String jsonCat = new Gson().toJson((ArrayList<String>) value);
            prefsEditor.putString(CATEGORY, jsonCat);
            Log.d("TAG", "WritejsonCars = " + jsonCat);
        }
        if (key.equals(DISTANCE)) {
            Log.d("TAG", "DISTANCE wrote = " + value);
            prefsEditor.putLong(DISTANCE, Double.doubleToLongBits((Double) value));
        }
        if (key.equals(LATITUDE))
            prefsEditor.putLong(LATITUDE, Double.doubleToLongBits((Double) value));
        if (key.equals(LONGITUDE))
            prefsEditor.putLong(LONGITUDE, Double.doubleToLongBits((Double) value));
        if (key.equals(RATING))
            prefsEditor.putLong(RATING, Double.doubleToLongBits((Double) value));
        if (key.equals(LOCATION_TEXT))
            prefsEditor.putString(LOCATION_TEXT, (String) value);
        if (key.equals(CATEGORY_TEXT))
            prefsEditor.putString(CATEGORY_TEXT, (String) value);
        prefsEditor.commit();
    }

    public void removeUserPref() {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        if (sharedPreferences.contains(USER_PREF_OBJECT)) {
            prefsEditor.remove(USER_PREF_OBJECT).commit();
        }
    }

    /******* new Method ******/
    public void saveFcm(String fcm) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(FCM_PREF_OBJECT, fcm);
        prefsEditor.commit();
    }

    public String getFcmId() {
        String fcm = sharedPreferences.getString(FCM_PREF_OBJECT, null);
        return fcm;
    }

    public void saveNotificationStatus(boolean isEnable) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean(NOTIFICATION_PREF_OBJECT, isEnable);
        prefsEditor.commit();
    }

    public Boolean isNotificationEnable() {
        boolean isEnable = sharedPreferences.getBoolean(NOTIFICATION_PREF_OBJECT, true);
        return isEnable;
    }

    public void removeIsEnable() {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        if (sharedPreferences.contains(NOTIFICATION_PREF_OBJECT)) {
            prefsEditor.remove(NOTIFICATION_PREF_OBJECT).commit();
        }
    }

    public void saveJobLocationData(String key, Object val) {
        Log.e("SAVE", "JOB_LOCATION" + key + " " + val);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        if (key.equals(JOB_LAT))
            prefsEditor.putLong(JOB_LAT, Double.doubleToLongBits((Double) val));

        if (key.equals(JOB_LNG))
            prefsEditor.putLong(JOB_LNG, Double.doubleToLongBits((Double) val));

        if (key.equals(JOB_PLACE))
            prefsEditor.putString(JOB_PLACE, (String) val);
        if (key.equals(ADD_ABOUT))
            prefsEditor.putString(ADD_ABOUT, (String) val);
        prefsEditor.commit();
    }

    public Object readJobLocationData(String key) {
        Object object = null;
        if (key.equals(JOB_LAT))
            object = Double.longBitsToDouble(sharedPreferences.getLong(JOB_LAT, 0));
        if (key.equals(JOB_LNG))
            object = Double.longBitsToDouble(sharedPreferences.getLong(JOB_LNG, 0));
        if (key.equals(JOB_PLACE))
            object = sharedPreferences.getString(JOB_PLACE, "");
        if (key.equals(ADD_ABOUT))
            object = sharedPreferences.getString(ADD_ABOUT, "");
        return object;
    }

    public void removePostedJobLocationData() {
        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
        if (sharedPreferences.contains(JOB_LAT)) {
            prefEditor.remove(JOB_LAT).commit();
        }
        if (sharedPreferences.contains(JOB_LNG)) {
            prefEditor.remove(JOB_LNG).commit();
        }
        if (sharedPreferences.contains(JOB_PLACE)) {
            prefEditor.remove(JOB_PLACE).commit();
        }
        if (sharedPreferences.contains(ADD_ABOUT)) {
            prefEditor.remove(ADD_ABOUT).commit();
        }
        if (sharedPreferences.contains(STAFF_MEMBER_IDS)) {
            prefEditor.remove(STAFF_MEMBER_IDS).commit();
        }
        if (sharedPreferences.contains(STAFF_MEMBERS_NAME)) {
            prefEditor.remove(STAFF_MEMBERS_NAME).commit();
        }
    }

    public void saveStaffMember(String key, Object selectedItems) {
        Log.e("Save_member_Data", "Save_Members" + key + " " + STAFF_MEMBER_IDS + "  " + STAFF_MEMBERS_NAME + "  " + selectedItems);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        if (key.equals(STAFF_MEMBER_IDS)) {
            String jsonIds = new Gson().toJson((ArrayList<String>) selectedItems);
            prefsEditor.putString(STAFF_MEMBER_IDS, jsonIds);
        }
        if (key.equals(STAFF_MEMBERS_NAME)) {
            prefsEditor.putString(STAFF_MEMBERS_NAME, (String) selectedItems);
        }
        prefsEditor.commit();
    }

    public Object readMemberIds(String key) {
        Log.e("ReadIds", "ReadIds" + key);
        Object object = null;
        if (key.equals(STAFF_MEMBER_IDS)) {
            String memberIds = sharedPreferences.getString(STAFF_MEMBER_IDS, null);
            Log.e("TAG", "ReadjsonCars = " + memberIds);
            // object = new Gson().fromJson(categories,ArrayList.class);
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            object = new Gson().fromJson(memberIds, type);
        }
        if (key.equals(STAFF_MEMBERS_NAME)) {
            object = sharedPreferences.getString(STAFF_MEMBERS_NAME, null);
        }
        return object;
    }

    public void saveBusinessHour(String key, String value) {
        Log.e("SaveBusinessHour","SaveBusinessHour"+"   "+value);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        if (key.equals(BUSINESS_DAY))
            prefsEditor.putString(BUSINESS_DAY, (String) value);

        if (key.equals(STARTING_HOUR))
            prefsEditor.putString(STARTING_HOUR, (String) value);

        if (key.equals(ENDING_HOUR))
            prefsEditor.putString(ENDING_HOUR, (String) value);
        prefsEditor.commit();
    }

    public String readBusinessHour(String key) {
        String address = null;
        if (key.equals(BUSINESS_DAY)) {
            address = sharedPreferences.getString(BUSINESS_DAY, null);
        }
        if (key.equals(STARTING_HOUR)) {
            address = sharedPreferences.getString(STARTING_HOUR, null);
        }
        if (key.equals(ENDING_HOUR)) {
            address = sharedPreferences.getString(ENDING_HOUR, null);
        }
        Log.e("ReadBusinessHour","Read"+"  "+address);
        return address;
    }
}