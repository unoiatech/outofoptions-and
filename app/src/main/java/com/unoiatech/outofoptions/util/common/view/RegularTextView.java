package com.unoiatech.outofoptions.util.common.view;

/**
 * Created by Unoiatech on 3/15/2016.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;


public class RegularTextView extends TextView {

    public RegularTextView(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public RegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public RegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        //Typeface customFont = FontCache.getTypeface("fonts/Montserrat-Regular.ttf", context);
        Typeface customFont=FontCache.getTypeface("fonts/Montserrat-Bold.ttf",context);
        Log.e("ApplyFont","ApplyFont"+"  "+customFont);
        setTypeface(customFont);
    }
}

