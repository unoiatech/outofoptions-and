package com.unoiatech.outofoptions.util;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import com.allenliu.badgeview.BadgeFactory;
import com.allenliu.badgeview.BadgeView;

/**
 * Created by Unoiatech on 11/13/2017.
 */

public class GetBadgeView {
    public static BadgeView getBadgeAtLeft(Context context) {
        com.allenliu.badgeview.BadgeView badgeView= BadgeFactory.create(context);
        badgeView.setTextColor(Color.WHITE)
                .setWidthAndHeight(20,20)
                .setBadgeBackground(Color.parseColor("#40c758"))
                .setTextSize(10)
                .setBadgeGravity(Gravity.LEFT|Gravity.TOP);
        return badgeView;
    }

    public static BadgeView getBadgeAtRight(Context context) {
        com.allenliu.badgeview.BadgeView badgeView= BadgeFactory.create(context);
        badgeView.setTextColor(Color.WHITE)
                .setWidthAndHeight(20,20)
                .setBadgeBackground(Color.parseColor("#40c758"))
                .setTextSize(10)
                .setBadgeGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
        return badgeView;
    }
}
