package com.unoiatech.outofoptions.util;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by unoiaAndroid on 4/17/2017.
 */

public class GPSPlotter  implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener
 {

    private static GPSPlotter gpsPlotterInstance;
    GoogleApiClient mGoogleApiClient;
    private  static  Location mLastLocation;
     private static final String TAG="GPSPlotter";



    /**
     * Private constructor.
     */
    private GPSPlotter(Context theContext) {
        initializeInstance(theContext);
       }

    /**
     * Returns an instance of the GPS Plotter.
     */
    public static GPSPlotter getInstance(Context theContext) {

        if (gpsPlotterInstance == null)
            return new GPSPlotter(theContext);
        else
            return gpsPlotterInstance;

    }

     @Override
     public void onConnected(@Nullable Bundle bundle) {

         try {
             mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                     mGoogleApiClient);
         }
         catch (SecurityException e)
         {

         }
         Log.e(TAG,"onConnected");
     }

     @Override
     public void onConnectionSuspended(int i) {
         Log.e(TAG,"onConnectionSuspended");

     }

     @Override
     public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
         Log.e(TAG,"onConnectionFailed");

     }

     public void initializeInstance(Context theContext)
     {
         if (mGoogleApiClient == null) {
             mGoogleApiClient = new GoogleApiClient.Builder(theContext)
                     .addConnectionCallbacks(this)
                     .addOnConnectionFailedListener(this)
                     .addApi(LocationServices.API)
                     .build();
         }

     }

     public void connect()
     {
         mGoogleApiClient.connect();
         Log.e(TAG,"connect");
     }

     public void disconnect()
     {
         mGoogleApiClient.disconnect();
         Log.e(TAG,"disconnect");
     }

     public  static Location getLocation()
     {
         Log.e(TAG,"getLocation");
          return mLastLocation;
     }


 }
