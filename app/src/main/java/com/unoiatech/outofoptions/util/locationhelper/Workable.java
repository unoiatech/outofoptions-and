package com.unoiatech.outofoptions.util.locationhelper;

/**
 * Created by Unoiatech on 1/19/2018.
 */
public interface Workable<T> {

    public void work(T t);
}