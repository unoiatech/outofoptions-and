package com.unoiatech.outofoptions.util;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by unoiaAndroid on 3/21/2017.
 */

public class TimeConverter {
    public static String LongMillisecondsToTime(Long millis) {
        DateTime start = new DateTime(millis);
        DateTime end = new DateTime();

        // period of chats year and 7 days
        Period period = new Period(start, end);

        // calc will equal end
        DateTime calc = start.plus(period);

        // able to calculate whole days between two dates easily
        Days days = Days.daysBetween(start, end);

        // able to calculate whole years between two dates easily
        Years years = Years.yearsBetween(start, end);

        // able to calculate whole weeks between two dates easily
        Weeks weeks = Weeks.weeksBetween(start, end);

        // able to calculate whole months between two dates easily
        Months months = Months.monthsBetween(start, end);

        // able to calculate whole hours between two dates easily
        Hours hours = Hours.hoursBetween(start, end);
        // able to calculate whole Minutes between two dates easily
        Minutes minutes = Minutes.minutesBetween(start, end);

        // able to calculate whole seconds between two dates easily
        Seconds seconds = Seconds.secondsBetween(start, end);

        if (years.getYears() > 1)
            return years.getYears() + " years ago ";
        else if (years.getYears() == 1)
            return years.getYears() + " year ago ";
        else if (months.getMonths() > 1)
            return months.getMonths() + " months ago ";
        else if (months.getMonths() == 1)
            return months.getMonths() + " month ago ";
        else if (weeks.getWeeks() > 1)
            return weeks.getWeeks() + " weeks ago ";
        else if (weeks.getWeeks() == 1)
            return weeks.getWeeks() + " week ago ";
        else if (days.getDays() > 1)
            return days.getDays() + " days ago ";
        else if (days.getDays() == 1)
            return days.getDays() + " day ago ";
        else if (hours.getHours() > 1)
            return hours.getHours() + " hours ago ";
        else if (hours.getHours() == 1)
            return hours.getHours() + " hour ago ";
        else if (minutes.getMinutes() > 1)
            return minutes.getMinutes() + " minutes ago ";
        else if (minutes.getMinutes() == 1)
            return minutes.getMinutes() + " minute ago ";
        else if (seconds.getSeconds() > 1)
            return seconds.getSeconds() + " seconds ago ";
        else
            return seconds.getSeconds() + " second ago ";
    }

    public static String longMillisecondsToDate(Long millis) {
        Date date = new Date(millis);
        DateFormat parser1 = new SimpleDateFormat("dd-MMM-yyyy");
        return parser1.format(date);
    }
}
