package com.unoiatech.outofoptions.constant;

/**
 * Created by unoiaAndroid on 3/14/2017.
 */

public enum JobStatus {


    PENDING("pending"),COMPLETED("completed"),ACTIVE("accepted"),EXPIRED("expired"),CANCELLED("cancelled");

    private final String val;

    private JobStatus(String val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return val;
    }

}
