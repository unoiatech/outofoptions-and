package com.unoiatech.outofoptions.jobdetail.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TabHost;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.UserRating;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by unoiaAndroid on 4/24/2017.
 */

public class RatingDialogFragment extends DialogFragment {
    private static String TAG="RatingDialogFragment";
    private static final String DESCRIBABLE_KEY = "describable_key";
    protected static UserRating userRating;
    interface DataListener{
        void methodToPassData(Object data);
    }

    static DataListener listnerObj;

    public void setListnerObj(DataListener listnerObj) {
        this.listnerObj = listnerObj;
    }

    public static RatingDialogFragment newInstance(UserRating userRating) {
        RatingDialogFragment fragment = new RatingDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DESCRIBABLE_KEY, userRating);
        fragment.setArguments(bundle);
        return fragment;
    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rating_dialog_fragment, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        final FragmentTabHost mTabHost = (FragmentTabHost)view.findViewById(android.R.id.tabhost);

        userRating = (UserRating) getArguments().getSerializable(DESCRIBABLE_KEY);
        Log.e(TAG,"userRating"+userRating.getSeller_reviews().size());

        mTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
        mTabHost.setBackgroundColor(getResources().getColor(android.R.color.white));

        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);

        /**Adding Fragments in Tab Host**/
        View view1 = customTabView(getActivity().getLayoutInflater(), getResources().getString(R.string.posted_job_text));
        TabHost.TabSpec tabSpec1 = mTabHost.newTabSpec("buyer_reviews").setIndicator(view1);
        mTabHost.addTab(tabSpec1, BuyerRatingFragment.class, null);

        View view2 = customTabView(getActivity().getLayoutInflater(), getResources().getString(R.string.applied_job_text_on_rating));
        TabHost.TabSpec tabSpec2 = mTabHost.newTabSpec("seller_reviews").setIndicator(view2);
        mTabHost.addTab(tabSpec2, SellerRatingFragment.class, null);

       /* mTabHost.addTab(mTabHost.newTabSpec("buyer_reviews").setIndicator(getString(R.string.posted_job_text)),
                BuyerRatingFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("seller_reviews").setIndicator(getString(R.string.applied_job_text_on_rating)),
                SellerRatingFragment.class, null);
*/
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            mTabHost.getTabWidget().getChildAt(i)
                    .setBackgroundResource(R.drawable.offer_help_drawable); // unselected
        }
        mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
                .setBackgroundResource(R.drawable.help_requested_drawable); // selected


        /*SellerRatingFragment sellerRatingFragment = (SellerRatingFragment)
                getChildFragmentManager().getFragments().get(chats);*/


      /* if(sellerRatingFragment !=null)
        {
            //Set listner
            //sellerRatingFragment.setListnerObj((SellerRatingFragment)sellerRatingFragment);

        }*/
        if(listnerObj!=null){
            //Pass your data
            listnerObj.methodToPassData(userRating);
        }
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String arg0) {
                //for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
                    if(mTabHost.getCurrentTab()==0){
                        mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab()).setBackgroundResource(R.drawable.help_requested_drawable); // unselected
                    }else{
                        mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
                                .setBackgroundResource(R.drawable.offer_help_drawable); // selected
                    }
            }
        });
        return  view;
    }

    private View customTabView(LayoutInflater inflater, String tabText) {
        View view= inflater.inflate(R.layout.tab_layout_for_tab1,null);
        TextView text= (TextView) view.findViewById(R.id.tab_text);
        text.setTextColor(getResources().getColorStateList(R.color.tab_text_color));
        text.setText(tabText);
        text.setBackgroundResource(android.R.color.transparent);
        return view;
    }
}
