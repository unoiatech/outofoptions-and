package com.unoiatech.outofoptions.jobdetail;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.common.view.CustomTypefaceSpan;

import org.w3c.dom.Text;

/**
 * Created by Unoiatech on 6/20/2017.
 */
public class CancellationTermsFragment extends Fragment
{
    String cancellationHour;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.cancellation_terms,container,false);
        RadioButton cancellation_btn=(RadioButton)view.findViewById(R.id.cancellation_term);
        TextView title=(TextView)view.findViewById(R.id.title);
        title.setText(getString(R.string.cancellation_term_title));

        //set Cancellation term text
        if(getArguments().getString("pre_cancellation_hour")!=null) {
            if (getArguments().getString("pre_cancellation_hour").equals("4")) {
                cancellationHour = getString(R.string.flexible_text);
            } else if (getArguments().getString("pre_cancellation_hour").equals("12")) {
                cancellationHour = getString(R.string.moderate_text);
            } else {
                cancellationHour = getString(R.string.strict_text);
            }
        }
        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Regular.ttf");
        SpannableStringBuilder cancellationTerm = new SpannableStringBuilder(cancellationHour);
        cancellationTerm.setSpan (new CustomTypefaceSpan("", font2), 0,4, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        cancellationTerm.setSpan(new RelativeSizeSpan(1.7f), 0, 4,0);
        cancellation_btn.setText(cancellationTerm);
        return view;
    }
}
