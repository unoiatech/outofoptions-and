package com.unoiatech.outofoptions.jobdetail.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unoiatech.outofoptions.R;

/**
 * Created by unoiaAndroid on 5/8/2017.
 */

public class ProfileFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.profile_screen,container,false);
        //  ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        return view;

    }
}

