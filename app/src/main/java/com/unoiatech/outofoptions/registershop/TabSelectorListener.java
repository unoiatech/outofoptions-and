package com.unoiatech.outofoptions.registershop;

/**
 * Created by Unoiatech on 11/16/2017.
 */
public interface TabSelectorListener {

    void selectDetailsTab();
    void selectContactTab();
    void selectImagesTab();

    void unselectDetailsTab();
    void unselectContactTab();
    void unselectImagesTab();
}
