package com.unoiatech.outofoptions.registershop;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.ShopData;
import com.unoiatech.outofoptions.util.common.activities.MyFileContentProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Unoiatech on 11/9/2017.
 */
public class ShopDetailsFragment extends Fragment implements View.OnClickListener {
    private EditText shopName,shopCategory,shopDes;
    private ImageButton addLogo;
    private ImageView shopLogo;
    private Button uploadButton;
    private FrameLayout nextButton;
    private TabSelectorListener mCallback;
    private static final int GET_FROM_GALLERY = 3;
    private static  final  int GET_FROM_CAMERA=2;
    private Uri selectedImage;
    private Bitmap bitmap;
    private File avatar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mCallback=(TabSelectorListener)getActivity();
        }catch (ClassCastException ex){
            ex.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.shop_details_layout,container,false);

        initViews(view);
        return view;
    }

    private void initViews(View view) {
        shopName=(EditText)view.findViewById(R.id.shop_name);
        shopCategory=(EditText)view.findViewById(R.id.shop_category);
        shopDes=(EditText)view.findViewById(R.id.shop_description);
        nextButton=(FrameLayout)view.findViewById(R.id.next_button);
        addLogo=(ImageButton)view.findViewById(R.id.add_images);
        shopLogo=(ImageView)view.findViewById(R.id.logo);
        uploadButton=(Button)view.findViewById(R.id.upload_logo);

        //OnClickListeners
        nextButton.setOnClickListener(this);
        shopCategory.setOnClickListener(this);
        addLogo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.next_button:
                checkShopDetails();
                break;
            case R.id.shop_category:
                //Categories are not decided yet
                //selectCategory();
                break;
            case R.id.add_images:
                showDialog();
                break;
        }
    }

    private void selectCategory() {
        //Hide Keyboard
        InputMethodManager inputmanager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputmanager.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        DialogFragment categoryFrag = ShopCategoryDialog.newInstance();
        // newFragment.show(getSupportFragmentManager(), "dialog");

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        /*//Add Data To sent
        Bundle bundle= new Bundle();
        bundle.putString("view","JobList");
        newFragment.setArguments(bundle);*/

        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        transaction.add(android.R.id.content, categoryFrag,"shop_category")
                .addToBackStack(null).commit();
    }

    private void checkShopDetails() {
        //&&!shopCategory.getText().toString().isEmpty()&&shopCategory.getText().toString()!=null
        if(!TextUtils.isEmpty(shopName.getText())&&!TextUtils.isEmpty(shopDes.getText())&&!TextUtils.isEmpty(shopCategory.getText())){
            ShopData shopDetails= ShopData.getInstance();
            shopDetails.setName(shopName.getText().toString().trim());
            shopDetails.setCategory("Salon");
            shopDetails.setAbout(shopDes.getText().toString().trim());

            FragmentTabHost mhost = (FragmentTabHost)getActivity().findViewById(R.id.fragment_tabhost);
            mhost.setCurrentTab(1);
            mCallback.selectDetailsTab();
        }
        else{
            notifyUser(getString(R.string.validation_check));
        }
    }

    private void notifyUser(String msg) {
        Snackbar.make(getView(),msg,Snackbar.LENGTH_SHORT).show();
    }

    private void showDialog() {
        View view = getActivity().getLayoutInflater().inflate (R.layout.add_images_dialog_layout, null);
        TextView fromGallery = (TextView)view.findViewById( R.id.gallery);
        TextView fromCamera = (TextView)view.findViewById( R.id.camera);
        TextView cancel=(TextView)view.findViewById(R.id.cancel);
        final Dialog addImageDialog = new Dialog(getActivity(), R.style.Theme_Dialog);
        addImageDialog.setContentView (view);
        addImageDialog.setCancelable(true);
        addImageDialog.show();

        /************** Listeners on Dialog**********/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageDialog.dismiss();
            }
        });
        fromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {addImageDialog.dismiss();
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                        GET_FROM_GALLERY);
            }
        });
        fromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {PackageManager pm = getActivity().getPackageManager();
                addImageDialog.dismiss();
                if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                    startActivityForResult(i, GET_FROM_CAMERA);
                }
                else {
                    notifyUser("Camera is not Available");
                }
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("OnActivityResult","OnActivityResult"+data.getData());
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                uploadButton.setVisibility(View.GONE);
                shopLogo.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if(requestCode==GET_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            File out = new File(getContext().getFilesDir(), "newImage.jpg");
            if(!out.exists()) {
                notifyUser("Error while capturing image");
                return;
            }
            bitmap= BitmapFactory.decodeFile(out.getAbsolutePath());
            selectedImage=getImageUri(getContext(),bitmap);
            uploadButton.setVisibility(View.GONE);
            shopLogo.setImageBitmap(bitmap);
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] imageBytes = bytes.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        //String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(encodedImage);
    }
}
