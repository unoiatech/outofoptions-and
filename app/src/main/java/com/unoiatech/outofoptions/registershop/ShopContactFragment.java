package com.unoiatech.outofoptions.registershop;

import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.instantjob.SetJobLocation;
import com.unoiatech.outofoptions.home.fragment.filter.FilterDialogFragment;
import com.unoiatech.outofoptions.model.BusinessHour;
import com.unoiatech.outofoptions.model.ShopData;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.view.AutoAddTextWatcher;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 11/9/2017.
 */
public class ShopContactFragment extends Fragment implements View.OnClickListener{
    private TabSelectorListener mCallBack;
    private EditText phnNumber,website,shopLocation,businessHour;
    private FrameLayout nextButton,backButton;
    private Double lat,lng;
    private static final String TAG="ShopContactFragment";
    private String day=null,openingTime=null,closingTime=null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
         try{
             mCallBack=(TabSelectorListener)getActivity();
         }catch (ClassCastException ex){
             ex.printStackTrace();
         }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.shop_contact_fragment,container,false);

        mCallBack.unselectContactTab();
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        phnNumber=(EditText)view.findViewById(R.id.phn_number);
        website=(EditText)view.findViewById(R.id.website);
        shopLocation=(EditText)view.findViewById(R.id.shop_loc);
        businessHour=(EditText)view.findViewById(R.id.business_hour);
        nextButton=(FrameLayout)view.findViewById(R.id.next_button);
        backButton=(FrameLayout)view.findViewById(R.id.back_button);

        phnNumber.addTextChangedListener(new AutoAddTextWatcher(phnNumber,"+",0));
        phnNumber.addTextChangedListener(new AutoAddTextWatcher(phnNumber,"-",3));

        String cityName=((ShopRegistrationScreen)getActivity()).getCityName();
        if(cityName!=null)
        shopLocation.setText(cityName);

        //Click Listener
        nextButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        shopLocation.setOnClickListener(this);
        businessHour.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.next_button:
                checkShopContactDetails();
                break;
            case R.id.back_button:
                FragmentTabHost mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
                mhost.setCurrentTab(0);
                mCallBack.unselectContactTab();
                break;
            case R.id.shop_loc:
                Intent intent= new Intent(getActivity(),SetJobLocation.class);
                Bundle bundle= new Bundle();
                bundle.putDouble("lat",((ShopRegistrationScreen)getActivity()).getLat());
                bundle.putDouble("lng",((ShopRegistrationScreen)getActivity()).getLng());
                bundle.putString("location_name",shopLocation.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.business_hour:
                showBusinessHourDialog();
                break;
        }
    }

    private void showBusinessHourDialog() {
        DialogFragment newFragment = BusinessHourDialog.newInstance();
        // newFragment.show(getSupportFragmentManager(), "dialog");

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        /*//Add Data To sent
        Bundle bundle= new Bundle();
        bundle.putString("view","JobList");
        newFragment.setArguments(bundle);*/

        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        transaction.add(android.R.id.content, newFragment,"business_hour")
                .addToBackStack(null).commit();
    }

    private void checkShopContactDetails() {
        Log.e("CheckContact Data","Check_contact Data"+phnNumber.getText().toString().isEmpty()+"  "+phnNumber.getText().toString());
         //&&!businessHour.getText().toString().isEmpty()&&businessHour.getText().toString()!=null

        if(!TextUtils.isEmpty(phnNumber.getText())&&!TextUtils.isEmpty(website.getText())&&!TextUtils.isEmpty(shopLocation.getText())) {
            if(phnNumber.getText().length()<13) {
                notifyUser(getString(R.string.swish_phn_text));
            }
            else if(!Patterns.WEB_URL.matcher(website.getText().toString().toLowerCase()).matches()){
                notifyUser(getString(R.string.invalid_website));
                Log.e(" check  Website Address",""+Patterns.WEB_URL.matcher(website.getText().toString()).matches());
            }
            else {
                getCompanyContactData();
                FragmentTabHost mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
                mhost.setCurrentTab(2);
                mCallBack.selectContactTab();
            }
        }
        else{
            notifyUser(getString(R.string.validation_check));
        }
    }

    private void getCompanyContactData() {
        String day=businessHour.getText().toString().substring(0, businessHour.getText().toString().indexOf(' '));
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        Log.e("Day","Day"+"  "+day);
        ShopData data=ShopData.getInstance();
        BusinessHour timing= new BusinessHour();
        if(day.equals("Sunday")){
            timing.setDayOfWeek(0);
            timing.setCloseTime(closingTime);
            timing.setOpenTime(openingTime);
        }
        else if(day.equals("Monday")){
            timing.setDayOfWeek(1);
            timing.setCloseTime(closingTime);
            timing.setOpenTime(openingTime);
        }
        else if(day.equals("Tuesday")){
            timing.setDayOfWeek(2);
            timing.setCloseTime(closingTime);
            timing.setOpenTime(openingTime);
        }
        else if(day.equals("Wednesday")){
            timing.setDayOfWeek(3);
            timing.setCloseTime(closingTime);
            timing.setOpenTime(openingTime);
        }
        else if(day.equals("Thursday")){
            timing.setDayOfWeek(4);
            timing.setCloseTime(closingTime);
            timing.setOpenTime(openingTime);
        }
        else if(day.equals("Friday")){
            timing.setDayOfWeek(5);
            timing.setCloseTime(closingTime);
            timing.setOpenTime(openingTime);
        }
        else if(day.equals("Saturday")){
            timing.setDayOfWeek(6);
            timing.setCloseTime(closingTime);
            timing.setOpenTime(openingTime);
        }
        data.setPhoneNumber(phnNumber.getText().toString().trim());
        data.setWebsite(website.getText().toString().trim());
        data.setLocation(shopLocation.getText().toString().trim());
        lat=((ShopRegistrationScreen)getActivity()).getLat();
        lng=((ShopRegistrationScreen)getActivity()).getLng();
        data.setUserId(user.getId());
        data.setLat(lat);
        data.setLng(lng);
        ArrayList<BusinessHour> businessTime= new ArrayList<>();
        //businessTime.add(timing);
        data.setBusinessHourss(timing);
        Log.e("Data","Data"+timing.getDayOfWeek());
    }

    private void notifyUser(String msg) {
        Snackbar.make(getView(),msg,Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("OnResume","OnResume");
        try{
            day= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readBusinessHour("day");
            openingTime=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readBusinessHour("starting");
            closingTime=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readBusinessHour("ending");
        }
        catch (NullPointerException ex){
            ex.printStackTrace();
        }
        if(day!=null&&openingTime!=null&&closingTime!=null) {
            //Storing Data at business hour diaglog box
            //businessHour.setText(day+" "+openingTime+" to "+closingTime);
           // businessHour.setText(" ");

        }
    }
}
