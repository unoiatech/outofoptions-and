package com.unoiatech.outofoptions.registershop;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allenliu.badgeview.BadgeView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.companymodule.registration.CompanyDetailsFragment;
import com.unoiatech.outofoptions.fragments.instantjob.AddVideoFragment;
import com.unoiatech.outofoptions.fragments.instantjob.InstantDetailsFragment;
import com.unoiatech.outofoptions.fragments.instantjob.InstantPreview;
import com.unoiatech.outofoptions.fragments.instantjob.TermsFragment;
import com.unoiatech.outofoptions.util.GetBadgeView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 11/8/2017.
 */

public class ShopRegistrationScreen extends AppCompatActivity implements TabSelectorListener {
    private ImageView firstTickImage, secondTickImage, thirdTickImage;
    private View firstView, secondView, thirdView;
    private FragmentTabHost mTabHost;
    private Double lat,lng;
    private String cityname = null;
    private LinearLayout cancelImage;
    private static String TAG="ShopRegistrationScreen";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_shop);

        //Initialize Xml Components
        initViews();

        /*****Adding Tabs*******/
        mTabHost = (FragmentTabHost) findViewById(R.id.fragment_tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.addTab(mTabHost.newTabSpec("details").setIndicator(""), ShopDetailsFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("contact").setIndicator(""), ShopContactFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("images").setIndicator(""), ShopImagesFragment.class, null);
        mTabHost.setCurrentTab(0);

        //Get Current Location
        getCurrentLocation();
    }

    private void initViews() {
        firstTickImage = (ImageView) findViewById(R.id.first_tick);
        secondTickImage = (ImageView) findViewById(R.id.second_tick);
        thirdTickImage = (ImageView) findViewById(R.id.third_tick);
        cancelImage=(LinearLayout)findViewById(R.id.cancel_image);

        firstView = (View) findViewById(R.id.firstview);
        secondView = (View) findViewById(R.id.second_line);
        thirdView = (View) findViewById(R.id.third_line);

        //OnDetails Fragment
        firstView.setVisibility(View.VISIBLE);
        cancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getCurrentLocation() {
        //Location Manager is used to figure out which location provider needs to be used.
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //Best location provider is decided by the criteria
        Criteria criteria = new Criteria();
        //location manager will take the best location from the criteria
        locationManager.getBestProvider(criteria, true);
        //once you know the name of the LocationProvider, you can call getLastKnownPosition() to find out where you were recently.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
        Geocoder gcd=new Geocoder(getBaseContext(), Locale.getDefault());
        if(location!=null) {
            lat = location.getLatitude();
            lng = location.getLongitude();
            Log.d("Tag", "1");
            List<Address> addresses = null;

            try {
                addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    cityname = addresses.get(0).getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "LocationData" + "  " + lat + "  " + lng + "  " + cityname + "  " + addresses.get(0));

        }
    }

    public Double getLat(){
        return lat;
    }

    public Double getLng(){
        return lng;
    }

    public String getCityName(){
        return cityname;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void selectDetailsTab() {
        firstTickImage.setVisibility(View.VISIBLE);
        firstTickImage.setBackgroundResource(R.drawable.round_tick);
        firstView.setVisibility(View.GONE);
    }

    @Override
    public void selectContactTab() {
        secondView.setVisibility(View.GONE);
        secondTickImage.setVisibility(View.VISIBLE);
        secondTickImage.setBackgroundResource(R.drawable.round_tick);
    }

    @Override
    public void selectImagesTab() {
        thirdView.setVisibility(View.GONE);
        thirdTickImage.setVisibility(View.VISIBLE);
        thirdTickImage.setBackgroundResource(R.drawable.round_tick);
    }

    @Override
    public void unselectDetailsTab() {
        firstView.setVisibility(View.VISIBLE);
        firstTickImage.setVisibility(View.GONE);
    }

    @Override
    public void unselectContactTab() {
        secondView.setVisibility(View.VISIBLE);
        secondTickImage.setVisibility(View.GONE);
    }

    @Override
    public void unselectImagesTab() {
        thirdView.setVisibility(View.VISIBLE);
        thirdTickImage.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().findFragmentByTag("business_hour")!=null&&getSupportFragmentManager().getBackStackEntryCount()==1) {
        }
        else{
            super.onBackPressed();
        }
    }
}
