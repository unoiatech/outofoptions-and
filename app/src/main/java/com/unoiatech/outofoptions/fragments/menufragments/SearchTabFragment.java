package com.unoiatech.outofoptions.fragments.menufragments;

/**
 * Created by unoiatech on 5/2/2016.
 */

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.StyleableRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.ListFragment;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.MapScreen;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.home.fragment.filter.FilterDialogFragment;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by unoiatech on 4/22/2016.
 */

public class SearchTabFragment extends Fragment{
    private FragmentTabHost mTabHost;
    private static final String TAG = "SearchTabFragment";
    Double latitude,longitude;
    private static SearchTabFragment searchFrag;
    private static String emailVerified="no";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //Check Login User Account is Verified or Not
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        getVerifiedIds(user.getId());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_job_layout,container,false);
        TextView filterButton=(TextView)view.findViewById(R.id.filter_button);

        mTabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);
        mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);
        mTabHost.getTabWidget().setDividerDrawable(null);

        /**Adding Fragments in Tab Host**/
        View view1=customTabView(inflater,"Map");
        TabHost.TabSpec tabSpec1=mTabHost.newTabSpec("Tab1").setIndicator(view1);
        mTabHost.addTab(tabSpec1,MapScreen.class, null);

        View view2= customTabView(inflater,"List");
        TabHost.TabSpec tabSpec3= mTabHost.newTabSpec("Tab2").setIndicator(view2);
        mTabHost.addTab(tabSpec3, ListFragment.class,null);

        mTabHost.setCurrentTab(0);
        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.map_selected_drawable);

        /*******TabClickListener****************/
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                int tab = mTabHost.getCurrentTab();
                if(tab==0){
                    Log.e(TAG,"IF");
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.map_selected_drawable);
                }
                else{
                    Log.e(TAG,"ELSE");
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.list_selected_drawable);
                }
            }
        });

        //Filter Button Click Listener
            filterButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("OnClick","OnClick");
                    /*DialogFragment newFragment = FilterDialogFragment.newInstance();
                    // newFragment.show(getSupportFragmentManager(), "dialog");
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

                    // For a little polish, specify a transition animation
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

                    // To make it fullscreen, use the 'content' root view as the container
                    // for the fragment, which is always the root view for the activity
                    transaction.add(android.R.id.tabcontent, newFragment,"search_filter")
                            .addToBackStack(null).commit();*/

                    FilterDialogFragment frag=(FilterDialogFragment) getChildFragmentManager().findFragmentByTag("filter");
                    if(frag==null)
                        frag = FilterDialogFragment.newInstance();
                    FragmentManager fragMan = getChildFragmentManager();

                    // add Fragment A to the main linear layout
                    FragmentTransaction fragTrans = fragMan.beginTransaction();
                    fragTrans.add(android.R.id.tabcontent, frag,"filter");
                    fragTrans.addToBackStack("filter");
                    fragTrans.commit();
                }
            });
        getLocation();
        return view;
    }

    private View customTabView(LayoutInflater inflater, String tabText) {
        View view= inflater.inflate(R.layout.tab_layout_for_search,null);
        TextView text= (TextView) view.findViewById(R.id.tab_text);
        text.setTextColor(getResources().getColorStateList(R.color.tab_text_color_on_search));
        text.setText(tabText);
        text.setBackgroundResource(android.R.color.transparent);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //HomeScreenActivity.showTabHost();
    }

    @Override
    public void onPause() {
        super.onPause();
        //HomeScreenActivity.removeTabHost();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabHost = null;}

    public FragmentTabHost getFragmentTabHost() {
        return mTabHost;
    }

    public void getLocation() {
        GPSTracker gps = new GPSTracker(getActivity());

        // check if GPS enabled
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude,longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }
    public Double getLatitude(){
        return latitude;
    }
    public Double getLongitude(){
        return longitude;
    }


    public static SearchTabFragment getSearchFrag() {
        if(searchFrag==null){
            searchFrag=new SearchTabFragment();
        }
        return searchFrag;
    }
    private void getVerifiedIds(Long id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.verifyIdsOnProfile(id);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d("response","response"+" body" +response.body());
                showAccountVerification(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    public  void showAccountVerification(ArrayList<User> body) {
        for (User item : body) {
            if (item.getVerificationType().equals("email")) {
                if(item.getIsVerified()==1){
                    emailVerified="yes";
                }
            }
            Log.e("Email","Emails"+"  "+emailVerified);
        }
    }
}
