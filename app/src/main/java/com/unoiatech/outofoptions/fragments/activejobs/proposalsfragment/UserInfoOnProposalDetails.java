package com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.URL;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 4/14/2017.
 */

public class UserInfoOnProposalDetails extends Fragment {
    TextView name,reviews,tagline,about,emailText,phoneText,facebookText,bankText;
    ImageView phone_image,bank_image,fb_image,email_image,profile_Image;
    SimpleRatingBar ratingBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.seller_user_info,container,false);

        ratingBar=(SimpleRatingBar)view.findViewById(R.id.rating_bar);
        reviews=(TextView)view.findViewById(R.id.reviews);
        profile_Image=(ImageView)view.findViewById(R.id.profile_image);
        name=(TextView)view.findViewById(R.id.name);
        tagline=(TextView)view.findViewById(R.id.tagline);
        about=(TextView)view.findViewById(R.id.about);
        emailText=(TextView)view.findViewById(R.id.email);
        bankText=(TextView)view.findViewById(R.id.bank);
        facebookText=(TextView)view.findViewById(R.id.facebook);
        phoneText=(TextView)view.findViewById(R.id.phone);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getArguments().getLong("userId");
        getUserProfile(getArguments().getLong("userId"));
    }

    public void getUserProfile(Long userId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<User> call= apiService.getUserProfile(userId);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.code()==200) {
                    showRetroResult(response.body().getUser());
                    getAccountVerificationInfo(response.body().getId());
                    getUserReviews(response.body().getId());
                }
                else{
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                    Log.d("Code" + "Code", "" + response);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    private void getUserReviews(Long userId){
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<User> call= apiService.getUserReviews(userId);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                /*String rating = response.body().getAverage_review();
                ratingBar.setRating(Float.parseFloat(rating));
                int userReviews = (response.body().getBuyer_reviews().size() + response.body().getSeller_reviews().size());
                reviews.setText("( " + userReviews + " reviews )");*/
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("Failure","Failure"+t.toString());
            }
        });
    }

    private void getAccountVerificationInfo(Long userId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.verifyIdsOnProfile(userId);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d("response","response"+" body" +response.body());
                showAccountVerification(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    public  void showRetroResult(User body) {
        Log.e("ShowReslt","Show_Result"+body+" "+body.getFirstName());
        try {
            String username = body.getFirstName()+" "+body.getLastName();
            Long userId=body.getId();
            String imagePath=body.getImagePath();
            name.setText(username);
            about.setText(body.getAbout());
            tagline.setText(body.getTagline());
            if(imagePath!=null) {
                if (imagePath.contains("\\")) {
                    String lastIndex = imagePath.replace("\\", " ");
                    String[] Stamp = lastIndex.split("\\s");
                    String timeStamp = Stamp[3];
                    String userImageUrl = URL.BASE_URL + "/users/download/" + userId + "/image/" + timeStamp;
                    Glide.with(getActivity()).load(userImageUrl).into(profile_Image);
                } else {
                    String imageUrl = S3Utils.getDownloadUserImageURL(userId, imagePath);
                    Glide.with(getActivity()).load(imageUrl).into(profile_Image);
                }
            }
            else{
                profile_Image.setImageResource(R.drawable.dummy_img);
            }
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }
    public  void showAccountVerification(ArrayList<User> body) {
        ArrayList<String> verified = new ArrayList<>();
        ArrayList<User> verifyIdsList = new ArrayList<User>(body);
        int size = verifyIdsList.size();
        for (int i = 0; i < size; i++) {
            String verificationType = verifyIdsList.get(i).getVerificationType();
            verified.add(verificationType);
        }
        for (String item : verified) {
            if (item.equals("bankId")) {
                phoneText.setText("  BankID");
            }
            if (item.equals("facebook")) {
                facebookText.setText("  Facebook");
            }
            if (item.equals("email")) {
                emailText.setText("Email");
            }
            if (item.equals("phone")) {
                phoneText.setText("  Phone");
            }
        }
    }
}
