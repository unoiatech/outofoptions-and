package com.unoiatech.outofoptions.fragments.menufragments;

import android.support.v4.app.Fragment;

/**
 * Created by unoiatech on 10/14/2016.
 */
public class MapScreenNew extends Fragment //implements OnMapReadyCallback,ClusterManager.OnClusterClickListener<JobListResponse>,ClusterManager.OnClusterItemClickListener<JobListResponse>,ClusterManager.OnClusterInfoWindowClickListener<JobListResponse>,ClusterManager.OnClusterItemInfoWindowClickListener {

{/* private String TAG = "MapScreenNew";
    ArrayList<User> arrayList;
    ArrayList<JobListResponse> job_data;
    GoogleMap mMap;
    MapView mapView;
    ClusterManager<JobListResponse> mClusterManager;
    JobListResponse jobListResponse;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment_layout, container, false);
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        // needed to get the map to display immediately
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(MapScreenNew.this);
        return view;
    }

    public void filterSearch() {
        if (mMap != null) {
            mMap.clear();
        }
        job_data = new ArrayList<>();
        User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        SearchTabFragment parentFragment = (SearchTabFragment) getParentFragment();
        final SearchJobsRequest jobs = new SearchJobsRequest();
        jobs.setCategory(new ArrayList<String>());
        jobs.setMinBudget("0");
        jobs.setMaxBudget("50000");
        jobs.setRating("0");
        jobs.setDistance("1000");
        jobs.setLatitude(parentFragment.getLatitude());
        jobs.setLongitude(parentFragment.getLongitude());
        Log.e("LAt", "NLong" + jobs.getLatitude() + " " + jobs.getLongitude());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<JobListResponse>> call = apiService.getJobsOnMap(user.getId(), jobs);
        call.enqueue(new Callback<ArrayList<JobListResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<JobListResponse>> call, Response<ArrayList<JobListResponse>> response) {
                Log.e(TAG, "MApResponse" + response.body() + " " + response.body());

                for (int i = 0; i < response.body().size(); i++) {
                    jobListResponse = new JobListResponse();
                    jobListResponse.setAverage_review(response.body().get(i).getAverage_review());
                    jobListResponse.setTotalReviews(String.valueOf(response.body().get(i).getBuyer_reviews().size() + response.body().get(i).getSeller_reviews().size()));
                    jobListResponse.setJob(response.body().get(i).getJob());
                    jobListResponse.setUser(response.body().get(i).getUser());
                    job_data.add(jobListResponse);
                }
                setUpClusterItem(job_data);
                addItems(job_data);
            }

            @Override
            public void onFailure(Call<ArrayList<JobListResponse>> call, Throwable t) {
                Log.d(TAG, "FAilure" + t.toString());
            }
        });
    }

    private void addItems(ArrayList<JobListResponse> job_data) {
        Log.e("Add_Items", "Add_Items" + job_data.size());
        if (job_data.size() > 0) {
            JobListResponse offsetItem;
            for (JobListResponse item : job_data) {
                Log.e("For_Loop", "For_Loop0" + item.getJob().getBudget() + " c" + item.getJob().getCategory() + " " + item.getJob().getLat() + " " + item.getJob().getLng() + " " + item.getJob().getTimeline() + " " + item.getJob().getTitle() + item.getTotalReviews() + item.getAverage_review() + item.getUser().getImagePath());
                offsetItem = new JobListResponse(item.getJob().getLat(), item.getJob().getLng(),item.getJob().getCategory(), item.getJob().getTimeline(), item.getJob().getTitle(), item.getJob().getBudget(), item.getJob().getPostingTime(), item.getTotalReviews(), item.getAverage_review(), item.getUser().getImagePath());
                mClusterManager.addItem(offsetItem);
                mClusterManager.setRenderer(new JobRenderer());
            }
        }
    }

    private void setUpClusterItem(ArrayList<JobListResponse> job_data) {
        Log.e("Clustering", "Clustering" + job_data.size());
        if (job_data.size() > 0) {
            for (JobListResponse items : job_data) {
                Log.e("Add_Clustering", "Add_Clustering" + this.mMap + items.getJob().getLat() + " " + items.getJob().getLng());
                this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(items.getJob().getLat(),items.getJob().getLng()), 9.5f));
                mClusterManager = new ClusterManager<JobListResponse>(getActivity(), this.mMap);
                mClusterManager.setRenderer(new JobRenderer());
                mMap.setOnMarkerClickListener(mClusterManager);
                mMap.setOnInfoWindowClickListener(mClusterManager);
                mClusterManager.setOnClusterClickListener(this);
                mClusterManager.setOnClusterInfoWindowClickListener(this);
                mClusterManager.setOnClusterItemClickListener(this);
                mClusterManager.setOnClusterItemInfoWindowClickListener(this);            }
               mClusterManager.cluster();

        } else {
            Toast.makeText(getActivity(), "No Job is Available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        arrayList = new ArrayList<>();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.setMyLocationEnabled(true);
        filterSearch();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMap != null) {
            mMap.clear();
        }
    }

    private void showDialog(String budget, final String timeline, String title, final String BuyerId, final String jobId, final String category, final String lat, final String lng, final String postingTime, String userReviews, String rating, String userImage) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.search_list_view_item, null);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.item_background);
        layout.setBackgroundColor(getResources().getColor(R.color.background_color));
        TextView jobTitle = (TextView) view.findViewById(R.id.title);
        View view1 = (View) view.findViewById(R.id.view);
        view1.setVisibility(View.GONE);
        TextView jobBudget = (TextView) view.findViewById(R.id.budget);
        TextView reviews = (TextView) view.findViewById(R.id.reviews);
        TextView distance = (TextView) view.findViewById(R.id.distance);
        TextView jobtimeline = (TextView) view.findViewById(R.id.date_to_complete);
        CircleImageView circleImageView = (CircleImageView) view.findViewById(R.id.profile_image);
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating);
        ratingBar.setRating(Float.parseFloat(rating));
        reviews.setText(userReviews);
        String image = S3Utils.getDownloadUserImageURL(Long.valueOf(BuyerId), userImage);
        Glide.with(getActivity()).load(image).into(circleImageView);

        try {
            jobtimeline.setText(DateTimeConverter.getTimeToComplete(Long.valueOf(timeline)));
        } catch (ParseException e) {
            Log.e("ParseException", e.toString());
        }
        jobBudget.setTextSize(17);
        jobBudget.setText(budget + " SEK");
        jobTitle.setTextSize(15);
        jobTitle.setText(title);
        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SingleJobActivity.class);
                intent.putExtra("job_id", jobId);
                intent.putExtra("buyer_id", BuyerId);
                intent.putExtra("category", category);
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
                intent.putExtra("user_status", "Buyer");
                intent.putExtra("View", "SearchScreen");
                intent.putExtra("time_to_complete", timeline);
                intent.putExtra("posting_time", postingTime);
                getActivity().startActivity(intent);
                mBottomSheetDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onClusterClick(Cluster<JobListResponse> cluster) {
        return false;
    }

    @Override
    public boolean onClusterItemClick(JobListResponse myJobs) {
        //showDialog(myJobs.getJob().getBudget(), myJobs.getJob().getTimeline(), myJobs.getJob().getTitle(), myJobs.getJob().getBuyerId(), myJobs.getJob().getJobId(), myJobs.getJob().getCategory(), myJobs.getLat(), myJobs.getJob().getLng(), myJobs.getJob().getPostingTime(), myJobs.getTotalReviews(), myJobs.getAverage_review(), myJobs.getUser().getImagePath());
        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<JobListResponse> cluster) {
    }

    @Override
    public void onClusterItemInfoWindowClick(ClusterItem clusterItem) {
    }

    private class JobRenderer extends DefaultClusterRenderer<JobListResponse> implements ClusterRenderer<JobListResponse> {
        private  IconGenerator mIconGenerator = new IconGenerator(getActivity());
        private  IconGenerator mClusterIconGenerator = new IconGenerator(getActivity());
        private ImageView mImageView;
        private  int mDimension;

        public JobRenderer() {
            super(getActivity(), mMap, mClusterManager);
            Log.e("Job_Renderer","Job_Renedrer");
            mImageView = new ImageView(getContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }
        @Override
        protected boolean shouldRenderAsCluster(Cluster<JobListResponse> cluster) {
            return cluster.getSize()>1;
        }

        @Override
        protected void onBeforeClusterItemRendered(JobListResponse item, MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            Log.e("BeforeCluster", "BeforeClusterRendered" + item.getCategory());
            if (item.getCategory().equals("Animals Passing")) {
                mImageView.setImageResource(R.drawable.house);
            } else if (item.getCategory().equals("Computer & Network")) {
                mImageView.setImageResource(R.drawable.job_detail);
            } else {
                mImageView.setImageResource(R.drawable.animals);
            }
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<JobListResponse> cluster, MarkerOptions markerOptions) {
            super.onBeforeClusterRendered(cluster, markerOptions);
            Log.e("setClusterSize", "setClusterSize" + cluster.getSize());

            final Drawable clusterIcon = getActivity().getResources().getDrawable(R.drawable.white_background);
            clusterIcon.setColorFilter(getActivity().getResources().getColor(R.color.app_color), PorterDuff.Mode.SRC_ATOP);

            *//***to set green round image**//*
            mClusterIconGenerator.setBackground(clusterIcon);
            LayoutInflater myInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View activityView = myInflater.inflate(R.layout.cluster_layout,null, false);
            mClusterIconGenerator.setContentView(activityView);
            mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(mClusterIconGenerator.makeIcon());
            markerOptions.icon(icon);
        }
    }*/
}

