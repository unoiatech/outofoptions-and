package com.unoiatech.outofoptions.fragments.offerhelp;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.home.HomeScreenActivity;

/**
 * Created by Unoiatech on 5/29/2017.
 */
public class ProposalSentDialog extends DialogFragment {
    Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.proposal_sent_dialog,container,false);
        Button okBtn=(Button)view.findViewById(R.id.ok_btn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Intent intent= new Intent(getActivity(), HomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        return  view;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public static DialogFragment newInstance() {
        ProposalSentDialog dialog = new ProposalSentDialog();
        return dialog;
    }
}
