package com.unoiatech.outofoptions.fragments.instantjob;

import com.unoiatech.outofoptions.model.RepostData;

/**
 * Created by Unoiatech on 3/24/2017.
 */

public interface OnViewChangeListener{
    RepostData getRepostData();
    void hideFirstView();
    void showSecondView();
    void hideSecondView();
    void showFourthView();
    void hideFourthView();
    void showThirdView();
    void hideThirdView();
}
