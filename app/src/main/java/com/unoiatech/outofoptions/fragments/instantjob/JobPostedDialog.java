package com.unoiatech.outofoptions.fragments.instantjob;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Unoiatech on 5/19/2017.
 */
public class JobPostedDialog extends DialogFragment{
    Dialog dialog;
    public static DialogFragment newInstance() {
        JobPostedDialog dialog= new JobPostedDialog();
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view= inflater.inflate(R.layout.job_posted_dialog,container,false);
        Button okayBtn=(Button)view.findViewById(R.id.okay_btn);
        getDialog().setCanceledOnTouchOutside(false);
        okayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new DataReceivedEvent("exit"));
                dialog.dismiss();
                //getActivity().finish();
                SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).removePostedJobLocationData();
                Intent intent = new Intent(getActivity(), HomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        return view;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog=super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
}
