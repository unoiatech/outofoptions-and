package com.unoiatech.outofoptions.fragments.menufragments.searchfragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.activity.RegisterActivity;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.SearchJobsScreen;
import com.unoiatech.outofoptions.fragments.menufragments.adapter.SearchJobListAdapter;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.home.fragment.JobsListFragment;
import com.unoiatech.outofoptions.home.fragment.adapter.JobAdapter;
import com.unoiatech.outofoptions.home.fragment.adapter.JobListResponse;
import com.unoiatech.outofoptions.home.fragment.filter.FilterDialogFragment;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.NotificationResponse;
import com.unoiatech.outofoptions.model.PageDetail;
import com.unoiatech.outofoptions.model.SearchListResponse;
import com.unoiatech.outofoptions.model.SearchRequest;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.PlaceUtil;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 8/31/2017.
 */

public class SearchListFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private RecyclerView mRecyclerView;
    private SearchJobListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final String TAG="SearchListFragment";
    private List<SearchListResponse> jobListResponse;
    private SearchListResponse pageDetail;
    private int currentPage=0;
    private GoogleApiClient mGoogleApiClient;
    private TextView noJobsTextView;
    private Location mLastLocation;
    private AVLoadingIndicatorView progressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.list_fragment_layout, container, false);

        // initialize views
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list_recycler_view);
        noJobsTextView=(TextView) view.findViewById(R.id.no_jobs_text_view);
        progressBar = (AVLoadingIndicatorView) view.findViewById(R.id.progress);

        //set listView Items
        setmRecyclerView();

         /* Google Play Services Check       */
        if(isGooglePlayServicesAvailable(getContext())) {
        }

        /**** initialize GoogleAPI Client ***/
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /********TouchListener on RecyclerView***********************/
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Job job= jobListResponse.get(position).getJob();
                SearchJobsScreen parentFragment= (SearchJobsScreen)getActivity();
                if(parentFragment.getIsEmailVerified().equals("verified")){
                    Intent intent = new Intent(getActivity(), SingleJobActivity.class);
                    Bundle bundle= new Bundle();
                    bundle.putString("status","Detail");
                    bundle.putLong("jobId",job.getId());
                    bundle.putLong("userId",jobListResponse.get(position).getUser().getId());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                else{
                    if(parentFragment.getIsEmailVerified().equals("completeProfile")) {
                        Snackbar.make(getView(), getString(R.string.complete_profile_check), Snackbar.LENGTH_LONG).setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent1 = new Intent(getActivity(), RegisterActivity.class);
                                startActivity(intent1);
                            }
                        }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
                    }
                    else{
                        Snackbar.make(getView(),getString(R.string.verify_email_check),Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        }));
    }

    void setmRecyclerView(){

         // in content do not change the layout size of the RecyclerView
        // mRecyclerView.setHasFixedSize(true);
        jobListResponse = new ArrayList<>();
        mAdapter = new SearchJobListAdapter(getActivity(),jobListResponse);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.e(TAG,"onResume"+"  "+mLastLocation);

        //reset pages
        currentPage=0;
        if(mLastLocation!=null){
            SearchRequest searchRequest= SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).readSearchRequest();

            // fetch userId from preferences
            User user=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            if(user!=null)
            listJobs(searchRequest,user.getId());
        }

        mAdapter.setMoreDataAvailable(true);
        // add load more listener
        mAdapter.setLoadMoreListener(new SearchJobListAdapter.OnLoadMoreListener(){
        @Override
        public void onLoadMore(){
            Log.e(TAG,"onLoadMore");
            mRecyclerView.post(new Runnable(){
                @Override
                public void run(){
                    currentPage=currentPage+1;
                    if(currentPage<=pageDetail.getTotalPages()){
                    SearchRequest searchRequest=SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).readSearchRequest();

                        //searchRequest.setPageNo(jobListResponse.size() - chats);
                    searchRequest.setPageNo(currentPage);

                         // fetch userId from preferences
            User user=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            loadMore(searchRequest,user.getId());// a method which requests remote data
            Log.e(TAG,"current Page "+currentPage+searchRequest.getCategory()+" "+searchRequest.getRating()+" "+searchRequest.getLatitude()
            +" "+searchRequest.getDistance()+" "+searchRequest.getLongitude()+"  "+searchRequest.getPageNo()+"  "+searchRequest.getMaxBudget()+"  "+searchRequest.getMinBudget());
            Log.e(TAG,"total pages"+pageDetail.getTotalPages());
                    }
                }
            });

            //Calling loadMore function in Runnable to fix the
            // java.lang.IllegalStateException: Cannot call this method while RecyclerView is computing a layout or scrolling error
            }
        });
    }

    // get jobs from server
    public void listJobs(SearchRequest searchRequest,Long userId){
        Log.e("LIST_JOBS","LIST_JOBS");
        Log.e("searchRequest",searchRequest.getCategory().toArray().toString()+"\n"+
                searchRequest.getLatitude()+"\n"+
                searchRequest.getLongitude()+"\n"+
                searchRequest.getMaxBudget()+"\n"+
                searchRequest.getMinBudget()+"\n"+
                searchRequest.getRating()+"\n"+
                searchRequest.getPageNo()+"\n"+
                searchRequest.getDistance()+"\n");

            // clear previous response
        jobListResponse.clear();
        mAdapter.notifyDataChanged();

            // start animating progress bar
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<SearchListResponse> call=apiService.getJobsOnList(userId,searchRequest);
        call.enqueue(new Callback<SearchListResponse>() {
            @Override
            public void onResponse(Call<SearchListResponse> call, Response<SearchListResponse> response) {
                Log.e(TAG,"Response "+response.body()+"");
                if(response!=null){
                    pageDetail= response.body();
                    List<SearchListResponse>result=response.body().getContent();
                    jobListResponse.addAll(result);
                    // notify adapter about the list update
                    mAdapter.notifyDataChanged();

                    // stop animating progress bar
                    progressBar.setVisibility(View.INVISIBLE);

                    /***  Show "no jobs layout " if no jobs is found matching the user's criteria  ***/
                    if(result.size()==0){
                        noJobsTextView.setVisibility(View.VISIBLE);
                    }
                    else{
                        noJobsTextView.setVisibility(View.INVISIBLE);
                    }
                }
                else {
                    Snackbar.make(getView(), "Server error", Snackbar.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<SearchListResponse> call, Throwable t) {
                Snackbar.make(getView(),"Server error",Snackbar.LENGTH_SHORT);
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG,"failure"+t.toString());
            }
        });
    }

    /***
     * Function to handle pagination
     *****/
    private void loadMore(SearchRequest searchRequest,Long userId) {
        Log.e(TAG, "LOAD MORE" + "LOAD MORE");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        jobListResponse.add(new SearchListResponse("load"));
        mAdapter.notifyItemInserted(jobListResponse.size() - 1);
        Call<SearchListResponse> call = apiService.getJobsOnList(userId,searchRequest);
        call.enqueue(new Callback<SearchListResponse>() {
            @Override
            public void onResponse(Call<SearchListResponse> call, Response<SearchListResponse> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG, "Response" + response.body());

                    //remove loading view
                    jobListResponse.remove(jobListResponse.size() - 1);
                    pageDetail = response.body();
                    if (response.body().getContent().size() > 0) {
                        jobListResponse.addAll(response.body().getContent());
                    } else {
                        Log.e(TAG, "Response_Else");
                        //result size 0 means there is no more data available at server
                        mAdapter.setMoreDataAvailable(false);
                    }
                    mAdapter.notifyDataChanged();
                }
            }

            @Override
            public void onFailure(Call<SearchListResponse> call, Throwable t) {
                Log.e(TAG," Load More Response Error "+t.getMessage());
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Log.e(TAG,"onCreate");
    }

    @Override
    public void onViewCreated(View view,@Nullable Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);
        Log.e(TAG,"onViewCreated");
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.e(TAG,"onPause");
    }

    @Override
    public void onStart(){
        mGoogleApiClient.connect();
        super.onStart();
        Log.e(TAG,"onStart");
    }

    @Override
    public void onStop(){
        mGoogleApiClient.disconnect();
        super.onStop();
        Log.e(TAG,"onStop");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e(TAG,"onDestroy");
    }

    @Override
    public void onDetach(){
        super.onDetach();
        Log.e(TAG,"onDetach");
    }

    public void setDefaultValues(){
        // set distance here as default not works for , doubel to long conversion
        if(((Double)(SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).readSearchRequestByKey("distance")))==0.0){
            SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("distance",200.0);
        }

        //if category empty in preference , pass new array list with size 0
        if(((ArrayList<String>)(SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).readSearchRequestByKey("category")))==null)
            SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("category", new ArrayList<String>());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle){
        try{
            if(mLastLocation==null){
                mLastLocation= LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if(mLastLocation!=null){

                 // update location values in the shared preferences
                updateLocation();

                    SearchRequest searchRequest=SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).readSearchRequest();

                // fetch userId from preferences
                    Log.e("size of jobs list",jobListResponse.size()+"");
                    User user=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
                    if(user!=null)
                    listJobs(searchRequest,user.getId());
                }
                else{
                    showLocationSettingsAlert();
                }
            }
            else{
            }
        }
        catch(SecurityException e) {
            showLocationSettingsAlert();
        }
        Log.e(TAG,"onConnected");
    }

    @Override
    public void onConnectionSuspended(int i){
        Log.e(TAG,"onConnectionSuspended");
    }

    public boolean isGooglePlayServicesAvailable(Context context){
        GoogleApiAvailability googleApiAvailability= GoogleApiAvailability.getInstance();
        int resultCode=googleApiAvailability.isGooglePlayServicesAvailable(context);
        return resultCode==ConnectionResult.SUCCESS;
    }

    public void showLocationSettingsAlert(){
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(getActivity());
            alertDialogBuilder
            .setMessage("GPS is disabled in your device. Enable it?")
            .setCancelable(false)
            .setPositiveButton("Enable GPS", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int id){
                    Intent callGPSSettingIntent=new Intent(
                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    getActivity().startActivity(callGPSSettingIntent);
                    }
                });
            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog,int id){
                    dialog.cancel();
                }
            });
            AlertDialog alert=alertDialogBuilder.create();
            alert.show();
    }

    void updateLocation(){
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("latitude", mLastLocation.getLatitude());

        //  set longitude
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("longitude", mLastLocation.getLongitude());
         //set Location text
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("location_text", PlaceUtil.getCompleteAddressString(mLastLocation.getLatitude(),mLastLocation.getLongitude(),
            getContext()));
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}
