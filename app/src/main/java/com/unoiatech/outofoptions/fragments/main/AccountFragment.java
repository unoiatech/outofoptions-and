package com.unoiatech.outofoptions.fragments.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.unoiatech.outofoptions.activity.LoginActivity;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.activity.MainActivity;


/**
 * Created by Unoiatech on 4/8/2016.
 */
public class AccountFragment extends Fragment implements View.OnTouchListener
{
    OnAccountSelectedListener mCallback;
    View view;
    Button backButton;
    EditText emailEditText,pwdEditText,cfmEditText;
    public static final String EMAIL_PATTERN="[^\\s@]+@[^\\s@]+\\.[a-zA-z][a-zA-Z][a-zA-Z]*";
    public static final String PASSWORD_PATTERN="([a-zA-Z]+[0-9]+)";
    ViewPager viewPager;
    OnRegisterViewListener mViewChangeListener;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.account_fragment_layout,container,false);
        Button registerButton =(Button) view.findViewById(R.id.register_button);
        TextView signinButton=(TextView) view.findViewById(R.id.sign_in);
        viewPager= (ViewPager) getActivity().findViewById(R.id.viewpager);

        mViewChangeListener.accountTabSelected();
        emailEditText=(EditText) view.findViewById(R.id.email_edit_text);
        pwdEditText=(EditText) view.findViewById(R.id.telephone_text);
        cfmEditText=(EditText) view.findViewById(R.id.cfm_pwd_edit_text);
        backButton=(Button)view.findViewById(R.id.back_button);

        /*************Set OnClickListener**************/
        emailEditText.setOnTouchListener(this);
        pwdEditText.setOnTouchListener(this);
        cfmEditText.setOnTouchListener(this);

        /**********onEditorAction on EditText*****************/
        emailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(emailEditText,"Email address field cannot be empty","Please enter a valid email address");
                }
                return false;
            }
        });
        pwdEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(pwdEditText, "Password field cannot be empty", "Password must be minimum 8 characters long including numbers and alphabets.\n Avoid special characters.");
                }
                return false;
            }
        });
        cfmEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    checkEnteredData(cfmEditText, "Confirm Password field cannot be empty", "Password does not match");
                }
                return false;
            }
        });

       // Move to User Info Screen
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("OnClick","OnClick"+emailEditText.getText().toString());
               if(!emailEditText.getText().toString().isEmpty()&&!pwdEditText.getText().toString().isEmpty()&&!cfmEditText.getText().toString().isEmpty()) {
                    if (!emailEditText.getText().toString().isEmpty()) {
                        checkEnteredData(emailEditText,getString(R.string.empty_email_address),getString(R.string.invalid_email_address));
                    }
                    if (!pwdEditText.getText().toString().isEmpty()) {
                        checkEnteredData(pwdEditText,getString(R.string.empty_paswrd),getString(R.string.invalid_paswrd));
                    }
                    if (!cfmEditText.getText().toString().isEmpty()) {
                        checkEnteredData(cfmEditText,getString(R.string.paswrd_mismatch),getString(R.string.paswrd_mismatch));
                    }
                    else {
                        mCallback.onAccountSelected(emailEditText.getText().toString(),pwdEditText.getText().toString());

                        /********Custom SlidingTAbStripeView*********/
                        InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null : getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        mViewChangeListener.userInfoSelected();
                        viewPager.setCurrentItem(1);
                    }
               }
               else {
                   if(emailEditText.getText().toString().isEmpty()) {
                       notifyUserForInvalidFields(getString(R.string.empty_email_address));
                   }
                   else if(cfmEditText.getText().toString().isEmpty()){
                       notifyUserForInvalidFields(getString(R.string.paswrd_mismatch));
                   }
                   else if(pwdEditText.getText().toString().isEmpty()) {
                       notifyUserForInvalidFields(getString(R.string.empty_paswrd));
                   }
               }
            }
        });
        //Back Button
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getActivity(), MainActivity.class);
                getActivity().finish();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);
            }
        });

        /*******Move User to SignInScreen********/
        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                getActivity().finish();
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
        });
    return view;
}

    private void checkEnteredData(EditText editText, String message, String msg) {
        if(!editText.getText().toString().isEmpty()) {
            if (editText.equals(emailEditText)) {
                if (!editText.getText().toString().matches(EMAIL_PATTERN)) {
                    notifyUserForInvalidFields(msg);
                    setFocus(editText);
                }
                else {
                    pwdEditText.setFocusableInTouchMode(true);
                    pwdEditText.setFocusable(true);
                    pwdEditText.setCursorVisible(true);
                }
            }
            else if (editText.equals(pwdEditText)) {
                if(editText.getText().toString().length()< 8){
                   notifyUserForInvalidFields(msg);
                   setFocus(editText);
                }
                /*else if(!editText.getText().toString().matches(PASSWORD_PATTERN)){
                    notifyUserForInvalidFields(msg);
                    setFocus(editText);
                }*/
                else{
                    cfmEditText.setFocusableInTouchMode(true);
                    cfmEditText.setFocusable(true);
                }
            }
            else if (editText.equals(cfmEditText)) {
                if (!editText.getText().toString().matches(pwdEditText.getText().toString())) {
                    notifyUserForInvalidFields(msg);
                    setFocus(editText);
                }
                else {
                    mCallback.onAccountSelected(emailEditText.getText().toString(),pwdEditText.getText().toString());
                    mViewChangeListener.userInfoSelected();
                    viewPager.setCurrentItem(1);
                }
            }
        }
        else {
            notifyUserForInvalidFields(message);
            setFocus(editText);
        }
    }

    private void setFocus(EditText editText) {
        if(editText.equals(emailEditText)) {
            pwdEditText.setFocusable(false);
            cfmEditText.setFocusable(false);}
        else if(editText.equals(pwdEditText)){
            cfmEditText.setFocusable(false);
            emailEditText.setFocusable(false);}
        else if(editText.equals(cfmEditText)){
            pwdEditText.setFocusable(false);
            emailEditText.setFocusable(false);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.email_edit_text:
                emailEditText.setFocusableInTouchMode(true);
                emailEditText.setCursorVisible(true);
                emailEditText.setFocusable(true);
                break;
            case R.id.telephone_text:
                checkEnteredData(emailEditText,getString(R.string.empty_email_address),getString(R.string.invalid_email_address));
                break;
            case R.id.cfm_pwd_edit_text:
                checkEnteredData(pwdEditText,getString(R.string.empty_paswrd),getString(R.string.invalid_paswrd));
                break;
        }
        return false;
    }

    //Container Activity must implement this interface
    public interface OnAccountSelectedListener {
        public void onAccountSelected(String email, String password);
        public void  getUserId(Long userId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = context instanceof Activity ? (Activity) context : null;
        try {
            mCallback = (OnAccountSelectedListener) activity;

            /****Hide View*******/
            mViewChangeListener=(OnRegisterViewListener) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAccountSelectedListener");
        }
    }

    void notifyUserForInvalidFields(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                })
                .setActionTextColor(getResources().getColor(R.color.app_color))
                .show();
    }
}
