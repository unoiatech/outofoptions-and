package com.unoiatech.outofoptions.fragments.activejobs.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.unoiatech.outofoptions.R;

/**
 * Created by unoiatech on 9/15/2016.
 */
public class AcceptProposalDialog extends DialogFragment implements View.OnClickListener {
    Dialog acceptProposalDialog,dialog;
    String currentTime;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.submit_payment_dialog_layout,container,false);
        ImageView cancelImage=(ImageView)view.findViewById(R.id.cancel);
        Button submitBtn=(Button)view.findViewById(R.id.submit_payment);

        cancelImage.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        return  view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public static DialogFragment newInstance() {
        AcceptProposalDialog dialog= new AcceptProposalDialog();
        return dialog;
    }

    @Override
    public void onClick(View v) {
       switch (v.getId()) {
           case R.id.cancel:
               dialog.dismiss();
               break;
       }
    }
}
