package com.unoiatech.outofoptions.fragments.menufragments.paymentfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 9/22/2017.
 */

public class PaymentSettingFragment extends AppCompatActivity{
    private static final String TAG="PaymentSettingFragment";
    private static PaymentSettingFragment paymentSettingFrag;
    private FragmentTabHost mTabHost;

    public static PaymentSettingFragment getPaymentSettings(){
        if(paymentSettingFrag==null)
            paymentSettingFrag= new PaymentSettingFragment();
        return paymentSettingFrag;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_jobs_layout);

        /******Fragment TabHost***********/
        mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup(this,getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);

        /**Adding Fragments in Tab Host**/
        View view1 = customTabView(this.getLayoutInflater(), getResources().getString(R.string.individual_text));
        TabHost.TabSpec tabSpec1 = mTabHost.newTabSpec("Tab1").setIndicator(view1);
        mTabHost.addTab(tabSpec1, PaymentIndividual.class, null);

        View view2 = customTabView(this.getLayoutInflater(), getResources().getString(R.string.company_text_field));
        TabHost.TabSpec tabSpec2 = mTabHost.newTabSpec("Tab2").setIndicator(view2);
        mTabHost.addTab(tabSpec2, PayFragment.class, null);

        mTabHost.setCurrentTab(0);
        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.help_requested_drawable);

        /*******TabClickListener****************/
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                int tab = mTabHost.getCurrentTab();
                if(tab==0) {
                    Log.e("TAb","TAB");
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.help_requested_drawable);
                }
                else{
                    Log.e("ElseTAB","ElseTAB"+tab);
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.offer_help_drawable);
                }
            }
        });
    }

  /*  @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.active_jobs_layout,container,false);

        *//******Fragment TabHost***********//*
        mTabHost = (FragmentTabHost)view.findViewById(android.R.id.tabhost);
        mTabHost.setup(getActivity(),getChildFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);

        *//**Adding Fragments in Tab Host**//*
        View view1 = customTabView(getActivity().getLayoutInflater(), getResources().getString(R.string.individual_text));
        TabHost.TabSpec tabSpec1 = mTabHost.newTabSpec("Tab1").setIndicator(view1);
        mTabHost.addTab(tabSpec1, PaymentIndividual.class, null);

        View view2 = customTabView(getActivity().getLayoutInflater(), getResources().getString(R.string.company_text_field));
        TabHost.TabSpec tabSpec2 = mTabHost.newTabSpec("Tab2").setIndicator(view2);
        mTabHost.addTab(tabSpec2, PayFragment.class, null);

        mTabHost.setCurrentTab(0);
        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.help_requested_drawable);

        *//*******TabClickListener****************//*
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                int tab = mTabHost.getCurrentTab();
                if(tab==0) {
                    Log.e("TAb","TAB");
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.help_requested_drawable);
                }
                else{
                    Log.e("ElseTAB","ElseTAB"+tab);
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.offer_help_drawable);
                }
            }
        });
        return view;
    }*/

    private View customTabView(LayoutInflater inflater, String tabText) {
        View view= inflater.inflate(R.layout.tab_layout_for_tab1,null);
        TextView text= (TextView) view.findViewById(R.id.tab_text);
        text.setTextColor(getResources().getColorStateList(R.color.tab_text_color));
        text.setText(tabText);
        text.setBackgroundResource(android.R.color.transparent);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
