package com.unoiatech.outofoptions.fragments.instantjob;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.RepostData;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 3/6/2017.
 */

public class InstantJobActivity extends AppCompatActivity implements OnViewChangeListener {
    private FragmentTabHost mTabHost;
    ImageView firstTickImage,secondTickImage,thirdTickImage,fourthTickImage;
    TextView firstView,secondView,thirdView,fourthView;
    String ret;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instant_job);
        mTabHost = (FragmentTabHost) findViewById(R.id.fragment_tabhost);
        firstTickImage = (ImageView) findViewById(R.id.first_tick);
        secondTickImage = (ImageView) findViewById(R.id.second_tick);
        thirdTickImage = (ImageView) findViewById(R.id.third_tick);
        fourthTickImage=(ImageView)findViewById(R.id.fourth_tick);

        firstView = (TextView) findViewById(R.id.firstview);
        secondView = (TextView) findViewById(R.id.second_line);
        thirdView = (TextView) findViewById(R.id.third_line);
        fourthView=(TextView)findViewById(R.id.fourth_line);
        getLocation();

        /*****Adding Tabs*******/
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.addTab(mTabHost.newTabSpec("video").setIndicator(""), AddVideoFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("details").setIndicator(""), InstantDetailsFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("tems").setIndicator(""), TermsFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("preview").setIndicator(""), InstantPreview.class, null);
        mTabHost.setCurrentTab(0);

        firstView.setVisibility(View.VISIBLE);
    }

    private void getLocation() {
        GPSTracker gpsTracker= new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            Double latitude= gpsTracker.getLatitude();
            Double longitude= gpsTracker.getLongitude();
            getAddress(latitude,longitude);
        }
        else {
            gpsTracker.showSettingsAlert();
        }
    }

    /*******Get Address from Latitude n Longitude
     * @param longitude***************/
    private String getAddress(Double latitude, Double longitude) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude,longitude, 1);
            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                Log.e("StringBuilder","StringBuilder"+"  "+returnedAddress.getMaxAddressLineIndex());
                StringBuilder strReturnedAddress = new StringBuilder();
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    Log.e("For","For");
                    strReturnedAddress.append(returnedAddress.getAddressLine(i));//.append("\n");
                }
                ret = strReturnedAddress.toString()+":"+returnedAddress.getLatitude()+":"+returnedAddress.getLongitude();
                Log.e("Address","Address"+ret+"Address"+addresses);
              /*  if(returnedAddress!=null){
                    updateLocation(returnedAddress);
                }*/
            }
            else{
                ret = "No Address returned!";
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ret = "Can't get Address!";
        }
        return ret;
    }

    public String showAddress(){
        Log.e("ShowAddress","ShowAddress");
        return ret;
    }

    public void hideFirstView() {
        firstTickImage.setVisibility(View.VISIBLE);
        firstTickImage.setBackgroundResource(R.drawable.round_tick);
        firstView.setVisibility(View.GONE);
    }
    public void hideSecondView() {
        secondView.setVisibility(View.GONE);
        secondTickImage.setVisibility(View.VISIBLE);
        secondTickImage.setBackgroundResource(R.drawable.round_tick);
    }
    public void hideThirdView() {
        thirdTickImage.setVisibility(View.VISIBLE);
        thirdView.setVisibility(View.GONE);
        thirdTickImage.setBackgroundResource(R.drawable.round_tick);
    }

    @Override
    public void hideFourthView() {
        fourthTickImage.setVisibility(View.VISIBLE);
        fourthView.setVisibility(View.GONE);
        fourthTickImage.setBackgroundResource(R.drawable.round_tick);
    }

    public void showSecondView() {
        secondView.setVisibility(View.VISIBLE);
        secondTickImage.setVisibility(View.GONE);
    }

    public void showThirdView() {
        thirdView.setVisibility(View.VISIBLE);
        thirdTickImage.setVisibility(View.GONE);
    }

    @Override
    public void showFourthView() {
        fourthView.setVisibility(View.VISIBLE);
        fourthTickImage.setVisibility(View.GONE);
    }

    @Override
    public RepostData getRepostData() {
        try{
        if(getIntent().getStringExtra("View_id").equals("Repost")){
            RepostData repostData=(RepostData)getIntent().getExtras().getParcelable("job_info");
            return repostData;
        }}
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    public FragmentTabHost getFragmentTabHost()
    {
        return  mTabHost;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        InstantDetailsFragment detailsFragment=(InstantDetailsFragment) getSupportFragmentManager().findFragmentByTag("Details");
        if(detailsFragment!=null){
            if(!detailsFragment.dialog.isVisible()){
                super.onBackPressed();
            }
        }
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTabHost = null;
        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).removePostedJobLocationData();
    }
}


