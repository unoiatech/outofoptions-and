package com.unoiatech.outofoptions.fragments.menufragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 10/21/2016.
 */
public class EditAboutScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_about_screen);
        final EditText editAbout=(EditText)findViewById(R.id.about);
        TextView done=(TextView)findViewById(R.id.edit);
        TextView title=(TextView)findViewById(R.id.title);

        editAbout.setText(getIntent().getStringExtra("about"));
        done.setVisibility(View.VISIBLE);
        done.setText(R.string.done);
        title.setText(R.string.edit_about_title);
        done.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveJobLocationData("about_data",editAbout.getText().toString());
                finish();
               //overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
