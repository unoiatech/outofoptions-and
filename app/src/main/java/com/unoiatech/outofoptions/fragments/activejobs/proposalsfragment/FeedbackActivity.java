package com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 4/24/2017.
 */
public class FeedbackActivity extends Activity implements CompoundButton.OnCheckedChangeListener,View.OnClickListener {
    RadioButton r1,r2,r3,r4,yes,no;
    String job_status_rb,avail_help_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_activity);

        /*****Action Bar********/
        TextView title=(TextView)findViewById(R.id.title);
        title.setText(getResources().getString(R.string.job_completed_title));

        FrameLayout nextButton=(FrameLayout) findViewById(R.id.next_btn);
        yes=(RadioButton)findViewById(R.id.yes);
        no=(RadioButton)findViewById(R.id.no);
        r1=(RadioButton)findViewById(R.id.r1);
        r2=(RadioButton)findViewById(R.id.r2);
        r3=(RadioButton)findViewById(R.id.r3);
        r4=(RadioButton)findViewById(R.id.r4);

        r1.setOnCheckedChangeListener(this);
        r2.setOnCheckedChangeListener(this);
        r3.setOnCheckedChangeListener(this);
        r4.setOnCheckedChangeListener(this);
        yes.setOnCheckedChangeListener(this);
        no.setOnCheckedChangeListener(this);
        nextButton.setOnClickListener(this);

        avail_help_text= r1.getText().toString();
        job_status_rb=yes.getText().toString();
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (buttonView.getId() == R.id.r1) {
                avail_help_text=r1.getText().toString();
            }
            if (buttonView.getId() == R.id.r2) {
                avail_help_text=r2.getText().toString();
            }
            if (buttonView.getId() == R.id.r3) {
                avail_help_text=r3.getText().toString();
            }
            if (buttonView.getId() == R.id.r4) {
                avail_help_text=r4.getText().toString();
            }
            if (buttonView.getId() == R.id.yes){
                job_status_rb=yes.getText().toString();
            }
            if (buttonView.getId() == R.id.no){
                job_status_rb=no.getText().toString();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_btn:
                Intent intent = new Intent(FeedbackActivity.this, JobCompletedActivity.class);
                intent.putExtra("job_status", job_status_rb);
                intent.putExtra("help", avail_help_text);
                intent.putExtra("view_id",getIntent().getStringExtra("view_id"));
                intent.putExtra("job_id",getIntent().getLongExtra("job_id",0));
                intent.putExtra("proposal_id", getIntent().getLongExtra("proposal_id",0));
                intent.putExtra("seller_id",getIntent().getLongExtra("seller_id",0));
                Log.e("data","Data"+getIntent().getLongExtra("proposal_id",0));
                startActivity(intent);
                break;

            case R.id.back_image:
                finish();
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
