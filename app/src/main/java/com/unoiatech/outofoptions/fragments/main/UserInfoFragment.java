package com.unoiatech.outofoptions.fragments.main;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.QBSettings;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.activity.RegisterActivity;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.QuickbloxConstants;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.activities.MyFileContentProvider;
import com.wang.avi.AVLoadingIndicatorView;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 4/8/2016.
 */

public class UserInfoFragment extends Fragment implements View.OnTouchListener{
    public static final int GET_FROM_GALLERY = 3;
    public static final int GET_FROM_CAMERA=2;
    private static final String TAG = "UserInfoFragment";
    static Uri selectedImage;
    private static Long userId;
    private ImageView userImage;
    private Button backButton;
    private static Bitmap bitmap;
    private RelativeLayout addImage;
    private Button imageButton,submitButton;
    private TextView firstName,lastName,progressText;
    private ViewPager viewPager;
    private OnRegisterViewListener mViewChangeListener;
    private AVLoadingIndicatorView progressBar;
    private Double latitude,longitude;
    AccountFragmentNew.OnAccountSelectedListener mCallback;
    private User user = new User();
    private File avatar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = context instanceof Activity?(Activity) context : null;

        //Get LAtitude n Longitude of Current Location
        getLocation();

        try{
            mViewChangeListener=(OnRegisterViewListener) activity;
            mCallback = (AccountFragmentNew.OnAccountSelectedListener) activity;
        } 
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAccountSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_info_fragment_layout, container, false);
        userImage = (ImageView) view.findViewById(R.id.image_view);
        imageButton = (Button)view.findViewById(R.id.image_button);
        addImage=(RelativeLayout)view.findViewById(R.id.add_image);
        firstName = (TextView) view.findViewById(R.id.first_name_edit_text);
        lastName = (TextView) view.findViewById(R.id.last_name_edit_text);
        submitButton = (Button)view.findViewById(R.id.submit_button);
        viewPager=(ViewPager)getActivity().findViewById(R.id.viewpager);
        progressBar= (AVLoadingIndicatorView) view.findViewById(R.id.progress_bar);
        progressText=(TextView)view.findViewById(R.id.progress_text);
        backButton=(Button)view.findViewById(R.id.back_button);

        /************Add Image from Gallery or Camera**************/
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethod=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethod.hideSoftInputFromInputMethod(getView().getWindowToken(),0);
                showDialog();}
        });

        /**********Get First name And last name From SharedPreference********/
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        if(user!=null){
            userId=user.getId();
            firstName.setText(user.getFirstName());
            lastName.setText(user.getLastName());
        }

        /***********Touch listener on EditText**************/
       /* firstName.setOnTouchListener(this);
        lastName.setOnTouchListener(this);*/

        /**********onEditorAction on EditText*****************/
        /*firstName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(firstName,getString(R.string.empty_first_name));
                }
                return false;}
        });
        lastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                   checkEnteredData(lastName,getString(R.string.empty_last_name));
                }
                return false;
            }
        });*/
        /*******************Listener onSubmit Button****************/
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /*if(firstName.getText().toString().isEmpty()) {
                    notifyUser(getString(R.string.empty_first_name));
                }
                else if(lastName.getText().toString().isEmpty()) {
                    notifyUser(getString(R.string.empty_last_name));
                }
                else if(selectedImage== null) {
                    notifyUser(getString(R.string.empty_image_view));
                }
                else{*/
                   InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                   inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null :
                           getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                   mViewChangeListener.verifiedIdSelected();
                   setDataForSignUp();
                //}
            }
        });

        //back Button
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
                mViewChangeListener.accountUnselected();
            }
        });
        return view;
    }

    private void checkEnteredData(EditText editText, String message) {
        if(!editText.getText().toString().isEmpty()) {
           if(editText.equals(firstName)){
              lastName.setFocusableInTouchMode(true);
              lastName.setFocusable(true);
              lastName.setCursorVisible(true);
           }
            /*else{
              if(selectedImage==null) {
                   notifyUser(getString(R.string.empty_image_view));
               }*/
               else{
                   mViewChangeListener.verifiedIdSelected();
                   setDataForSignUp();
              // }
            }
        }
        else {
            notifyUser(message);
            lastName.setFocusable(false);
        }
    }

    private void setDataForSignUp() {
        String email = ((RegisterActivity)getActivity()).getEmail();
        String telephoneNo=((RegisterActivity)getActivity()).getTelephoneNo();
        user.setEmail(email);
        user.setPhoneNo(telephoneNo);
        user.setLat(latitude);
        user.setLng(longitude);
        Log.e("SignUp_Data","SignUp_Data"+"  "+user.getLat()+" "+user.getLng());
        /*user.setDeviceType("android");
        user.setDeviceId(SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).getFcmId());*/
        registerUser(user);
    }

    /******************Dialog to choose Image***********************/
    private void showDialog() {
        View view = getActivity().getLayoutInflater().inflate (R.layout.add_images_dialog_layout, null);
        TextView fromGallery = (TextView)view.findViewById( R.id.gallery);
        TextView fromCamera = (TextView)view.findViewById( R.id.camera);
        TextView cancel=(TextView)view.findViewById(R.id.cancel);
        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow ().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        fromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                        GET_FROM_GALLERY);
            }
        });
        fromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getActivity().getPackageManager();
                mBottomSheetDialog.dismiss();
                if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                    startActivityForResult(i, GET_FROM_CAMERA);
                }
                else {
                    Toast.makeText(getActivity(), "Camera is not available", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Detects request codes
        if (requestCode == GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                userImage.setImageBitmap(bitmap);
                imageButton.setVisibility(View.GONE);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if(requestCode==GET_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            File out = new File(getContext().getFilesDir(), "newImage.jpg");
            if(!out.exists()) {
                Toast.makeText(getActivity(),"Error while capturing image", Toast.LENGTH_LONG).show();
                return;
            }
            Bitmap mBitmap = BitmapFactory.decodeFile(out.getAbsolutePath());
            selectedImage=getImageUri(getContext(),mBitmap);
            userImage.setImageBitmap(mBitmap);
            imageButton.setVisibility(View.GONE);}
    }

    public Uri getImageUri(Context inContext, Bitmap inImage){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
   public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void getLocation() {
        GPSTracker gps = new GPSTracker(getActivity());
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }
    }

    public void registerUser(final User user) {
        progressBar.setVisibility(View.VISIBLE);
        progressText.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Long> call = apiService.registerUser(userId,user);
        call.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long>call, Response<Long> response) {
                if(response.isSuccessful()) {
                    Long userId = response.body();
                    Log.i(TAG, "User created" + response + " UserId" + userId);
                    viewPager.setCurrentItem(2);
                    if (selectedImage != null) {
                        uploadUserImage(getRealPathFromURI(selectedImage), userId);
                    }
                }else{
                    ApiError error= ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
                progressText.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Call<Long>call, Throwable t) {
                // Log error here since request failed
                Log.d(TAG, "error"+t.toString());
                progressText.setVisibility(View.GONE);
            }
        });
    }

    public void uploadUserImage(String imagePath,Long userId) {
        //Create Upload Server Client
        ApiInterface service =   ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        avatar = new File(imagePath);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatar);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", avatar.getName(), requestFile);

        Call<Void> resultCall = service.uploadImage(body,userId);
        resultCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.e("UserImage_Uploaded","UserImage_Uploaded"+response.body()+" "+response.isSuccessful());
                progressBar.setVisibility(View.GONE);
                ViewPager viewpager= (ViewPager)getActivity().findViewById(R.id.viewpager);
                viewpager.setCurrentItem(2);

                // register user on quickblox
                //  new QuickbloxService().registerUserOnQB(getContext(),user,file);
                initializeSession(getContext());
                createSession();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG,"Error"+t.toString());
            }
        });
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
       /* switch (v.getId()) {
            case R.id.first_name_edit_text:
                firstName.setFocusableInTouchMode(true);
                firstName.setCursorVisible(true);
                firstName.setFocusable(true);
                break;
            case R.id.last_name_edit_text:
                checkEnteredData(firstName,getString(R.string.empty_first_name));
                break;
        }*/
        return false;
    }
    void notifyUser(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                })
                .setActionTextColor(getResources().getColor(R.color.app_color))
                .show();
    }

    public void initializeSession(Context context) {
        QBSettings.getInstance().init(context, QuickbloxConstants.APP_ID, QuickbloxConstants.AUTH_KEY,
                QuickbloxConstants.AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(QuickbloxConstants.ACCOUNT_KEY);
    }

    //Create session
    public void createSession() {
        QBAuth.createSession(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                signup(user);
                // You have successfully created the session
                // Now you can use QuickBlox API!
            }

            @Override
            public void onError(QBResponseException errors) {
                Log.e("Error","Error"+errors.toString());
            }
        });
    }
    //Create new User (registration)
    public void signup(final User o3user) {
        final QBUser user = new QBUser();
        user.setExternalId(String.valueOf(o3user.getId()));
        user.setEmail(o3user.getEmail());
        user.setFullName(firstName.getText().toString() + " " +lastName.getText().toString());
        user.setPassword("admin123");

        QBUsers.signUp(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
                Log.d("OnSuccess","OnSuccess"+user.getId());
                signIn(o3user);
            }

            @Override
            public void onError(QBResponseException errors) {
                Log.e("Error","Error"+" "+errors.toString());
            }
        });
    }

    private void signIn(User o3User) {
        QBUser user = new QBUser();
        user.setEmail(o3User.getEmail());
        user.setPassword(o3User.getUserPassword());

        QBUsers.signIn(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle params) {
                uploadProfilePic(avatar,user.getId());
            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }

    //Update profile picture (avatar)
    public void uploadProfilePic(File avatar, final Integer quickbloxUserId) {

    // Upload new avatar to Content module
        Boolean fileIsPublic = false;
        QBContent.uploadFileTask(avatar, fileIsPublic, null, new QBEntityCallback<QBFile>() {
            @Override
            public void onSuccess(QBFile qbFile, Bundle params) {

                int uploadedFileID = qbFile.getId();

                // Connect image to user
                QBUser user = new QBUser();
                user.setId(quickbloxUserId);
                user.setFileId(uploadedFileID);

                QBUsers.updateUser(user, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser user, Bundle args) {

                    }

                    @Override
                    public void onError(QBResponseException errors) {

                    }
                });
            }

            @Override
            public void onError(QBResponseException errors) {

            }
        }, new QBProgressCallback() {
            @Override
            public void onProgressUpdate(int progress) {

            }
        });
    }
}

