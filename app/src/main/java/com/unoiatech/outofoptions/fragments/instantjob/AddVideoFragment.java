package com.unoiatech.outofoptions.fragments.instantjob;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.JobPostingDataHolder;
import com.unoiatech.outofoptions.util.common.activities.MyFileContentProvider;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropSquareTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by Unoiatech on 3/6/2017.
 */
public class AddVideoFragment extends Fragment {
    FrameLayout nextButton;
    public static final int GET_FROM_GALLERY = 3;
    public  static  final  int GET_FROM_CAMERA=2;
    Uri selectedImage;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    ArrayList<Uri> imagesList =new ArrayList();
    OnViewChangeListener mlistener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.add_video_layout, container, false);
        ImageButton addImages=(ImageButton) view.findViewById(R.id.add_images_button);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.budget_recycler_view);
        LinearLayout cancelImage=(LinearLayout)getActivity().findViewById(R.id.cancel_image);

        cancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //getActivity().finish();
                Intent  intent=new Intent(getActivity(),HomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        /*****************Use a linear layout manager*********************/
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter(getActivity(),imagesList);
        mRecyclerView.setAdapter(mAdapter);
        addImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputmanager= (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputmanager.hideSoftInputFromWindow(getView().getWindowToken(),0);
                if(imagesList.size()<=4) {
                    showDialog();
                }
                else {
                    notifyUser("Cannot add more than 5 images");
                }
            }
        });

        if(mlistener.getRepostData()!=null){
            if(mlistener.getRepostData().getImages().size()>0){
                displayImages(mlistener.getRepostData().getImages());
            }
        }

        /*********************Listener on Next Button************************/
        nextButton=(FrameLayout) view.findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /* if(imagesList.size()>0)
              {*/
                JobPostingDataHolder dataHolderClass = JobPostingDataHolder.getInstance();
                Uri[] imagesArray = new Uri[imagesList.size()];
                imagesArray = imagesList.toArray(imagesArray);
                dataHolderClass.setImageUri(imagesArray);
                FragmentTabHost mhost = (FragmentTabHost)getActivity().findViewById(R.id.fragment_tabhost);
                mhost.setCurrentTab(1);
                mlistener.hideFirstView();
             /* }
              else {
                  notifyUser("Please Choose atleast one image");
              }*/
            }
          });
        return view;
    }

    private void displayImages(ArrayList<Job.Images> images){
        ArrayList<Job.Images> jobImages=new ArrayList<>();
        for(int i=0;i<images.size();i++) {
          /*File file = new File(path + File.pathSeparator + images.get(i).getName());
            selectedImage = Uri.fromFile(file);
            imagesList.add(selectedImage);
            mAdapter.notifyDataSetChanged();*/
        }
    }

    void showDialog(){
        View view = getActivity().getLayoutInflater().inflate (R.layout.add_images_dialog_layout, null);
        TextView fromGallery = (TextView)view.findViewById( R.id.gallery);
        TextView fromCamera = (TextView)view.findViewById( R.id.camera);
        TextView cancel=(TextView)view.findViewById(R.id.cancel);
        final Dialog addImageDialog = new Dialog(getActivity(), R.style.Theme_Dialog);
        addImageDialog.setContentView (view);
        addImageDialog.setCancelable(true);
        addImageDialog.show();

        /************** Listeners on Dialog**********/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageDialog.dismiss();
            }
        });
        fromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageDialog.dismiss();
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                        GET_FROM_GALLERY);
            }
        });
        fromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getActivity().getPackageManager();
                addImageDialog.dismiss();
                if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                    startActivityForResult(i, GET_FROM_CAMERA);
                }
                else {
                   notifyUser("Camera is not Available");
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            selectedImage = data.getData();
            imagesList.add(selectedImage);
            mAdapter.notifyDataSetChanged();
        }
        if(requestCode==GET_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            Log.i("DATA", "Receive the camera result"+"  "+getContext().getFilesDir());
            File out = new File(getContext().getFilesDir(), "newImage.jpg");
            if(!out.exists()) {
                notifyUser("Error while capturing image");
                return;
            }
            Bitmap mBitmap = BitmapFactory.decodeFile(out.getAbsolutePath());
            selectedImage=getImageUri(getContext(),mBitmap);
            imagesList.add(selectedImage);
            mAdapter.notifyDataSetChanged();
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mlistener=(OnViewChangeListener)getActivity();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    void notifyUser(String message){
        final Snackbar snackbar = Snackbar.make(getView(),message, Snackbar.LENGTH_LONG);
        snackbar.setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        snackbar.show();
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private ArrayList<Uri> mDataset;
        Activity activity;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public  class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView mImageView;
            public ViewHolder(View v) {
                super(v);
                ImageView imageView=(ImageView) v.findViewById(R.id.image_view);
                mImageView = imageView;}
        }

        public MyAdapter(FragmentActivity activity, ArrayList<Uri> myDataset) {
            mDataset = myDataset;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
                  Glide.with(holder.mImageView.getContext())
                    .load(mDataset.get(position))
                      //bitmapTransform(new BlurTransformation(getContext()))
                           .bitmapTransform(new CropSquareTransformation(getContext()),new RoundedCornersTransformation(holder.mImageView.getContext(),30,10))
                    //.centerCrop()
                    .into(holder.mImageView);
           /* User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            Glide.with(getActivity())
                    .load( S3Utils.getDownloadJobImageURL(
                            user.getId(),"9XFOSE"))
                    //bitmapTransform(new BlurTransformation(getContext()))
                    .bitmapTransform(new CropSquareTransformation(getContext()),new RoundedCornersTransformation(holder.mImageView.getContext(),30,10))
                    //.centerCrop()
                    .into(holder.mImageView);*/
        }
        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }
}
