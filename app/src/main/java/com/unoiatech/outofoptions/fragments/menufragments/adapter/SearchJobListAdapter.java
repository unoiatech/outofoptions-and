package com.unoiatech.outofoptions.fragments.menufragments.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.menufragments.notificationscreen.NotificationScreen;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.home.fragment.adapter.JobAdapter;
import com.unoiatech.outofoptions.home.fragment.adapter.JobListResponse;
import com.unoiatech.outofoptions.jobdetail.JobDetailFragment;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.SearchListResponse;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.S3Utils;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by sab99r
 */
public class SearchJobListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private  String TAG="SearchList";
    public final int TYPE_JOB = 0;
    public final int TYPE_LOAD = 1;
    private List<SearchListResponse> jobs;
    static Context context;
    public OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    public SearchJobListAdapter(Context context, List<SearchListResponse> jobslist) {
        this.context = context;
        this.jobs = jobslist;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        //   Log.e(TAG,"onCreateViewHolder"+viewType);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (viewType == TYPE_JOB) {
            return new JobHolder(inflater.inflate(R.layout.search_list_view_item, parent, false));
        }
        else {
            return new LoadHolder(inflater.inflate(R.layout.progress_bar_layout, parent, false));
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }
        if (getItemViewType(position) == TYPE_JOB) {
            ((JobHolder) holder).bindData(jobs.get(position));
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return jobs.size();
    }

    static class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
        //  Log.e(TAG,"notifyDataChanged ");
    }


    public interface OnLoadMoreListener {
        void onLoadMore();

    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        try{
            if(jobs.get(position).type.equals("load")){
                return TYPE_LOAD;
            }
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        return TYPE_JOB;
    }

      /* View Holder */
    class JobHolder extends RecyclerView.ViewHolder {
        TextView title,
                budget,
                distance,
                reviews,
                timeline;
        CircleImageView circleImageView;
        SimpleRatingBar ratingBar;

        public JobHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            budget = (TextView) v.findViewById(R.id.budget);
            reviews = (TextView) v.findViewById(R.id.reviews);
            distance = (TextView) v.findViewById(R.id.distance);
            timeline = (TextView) v.findViewById(R.id.date_to_complete);
            circleImageView = (CircleImageView) v.findViewById(R.id.profile_image);
            ratingBar = (SimpleRatingBar) v.findViewById(R.id.rating);
        }

        public void bindData(SearchListResponse content) {
            title.setText(content.getJob().getTitle());
            budget.setText(content.getJob().getBudget() + " SEK");
            ratingBar.setRating(Float.parseFloat(content.getAverage_review()));
            int total_reviews = content.getBuyer_reviews().size() + content.getSeller_reviews().size();
            reviews.setText("(" + String.valueOf(total_reviews) + ")");

            /********SetUserImage**************/
            String imageUrl = S3Utils.getDownloadUserImageURL(content.getUser().getId(), content.getUser().getImagePath());
            Log.e("UserImage","UserImage"+" "+imageUrl);
            Glide.with(context).load(imageUrl).into(circleImageView);
            try {
                timeline.setText("By " + DateTimeConverter.getTimeToComplete(content.getJob().getTimeline()));
            }
            catch (ParseException e) {
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            }
        }
    }
}
