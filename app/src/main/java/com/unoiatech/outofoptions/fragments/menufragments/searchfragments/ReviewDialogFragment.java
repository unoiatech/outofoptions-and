package com.unoiatech.outofoptions.fragments.menufragments.searchfragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.JobsFragment;
import com.unoiatech.outofoptions.fragments.activeproposals.ProposalFragment;

/**
 * Created by Unoiatech on 4/19/2017.
 */
public class ReviewDialogFragment extends DialogFragment
{
    FragmentTabHost mTabHost;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.active_jobs_layout,container,false);

        /******Fragment TabHost***********/
        mTabHost=(FragmentTabHost)view.findViewById(android.R.id.tabhost);
        //mTabHost.setup(getActivity(),fm,android.R.id.tabcontent);
        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);


        /**Adding Fragments in Tab Host**/
        View view1=customTabView(inflater,"Posted Job");
        TabHost.TabSpec tabSpec1=mTabHost.newTabSpec("Tab1").setIndicator(view1);
        mTabHost.addTab(tabSpec1,JobsFragment.class, null);

        View view2= customTabView(inflater,"Applied Job");
        TabHost.TabSpec tabSpec3= mTabHost.newTabSpec("Tab2").setIndicator(view2);
        mTabHost.addTab(tabSpec3,ProposalFragment.class,null);

        mTabHost.setCurrentTab(0);
        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_background_drawable);

        /*******TabClickListener****************/
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
            @Override
            public void onTabChanged(String tabId){
                int tab=mTabHost.getCurrentTab();
                mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.tab_background_drawable);
            }
        });
        return view;
    }

    private View customTabView(LayoutInflater inflater, String tabText) {
        View view= inflater.inflate(R.layout.tab_layout_for_tab1,null);
        TextView text= (TextView) view.findViewById(R.id.tab_text);
        text.setText(tabText);
        text.setBackgroundResource(R.drawable.tab_background_drawable);
        return view;
    }
}
