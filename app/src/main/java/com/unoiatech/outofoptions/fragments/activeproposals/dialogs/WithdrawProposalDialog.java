package com.unoiatech.outofoptions.fragments.activeproposals.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;

import org.w3c.dom.Text;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 4/28/2017.
 */

public class WithdrawProposalDialog extends DialogFragment implements View.OnClickListener {
    private static final String TAG ="WithdrawProposalDialog";
    private Dialog dialog;

    public static DialogFragment newInstance() {
        WithdrawProposalDialog dialog= new WithdrawProposalDialog();
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.withdraw_proposal_layout,container,false);
        TextView static_text=(TextView)view.findViewById(R.id.static_text);
        Button cancel_btn=(Button)view.findViewById(R.id.cancel_btn);
        Button yes_btn=(Button)view.findViewById(R.id.yes_btn);

        cancel_btn.setOnClickListener(this);
        yes_btn.setOnClickListener(this);

        String text = "<font color=#40c758>NOTE</font> <font color=#aaaaaa>! Some users take significant time to make decision.</font>";
        static_text.setText(Html.fromHtml(text));
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog=super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_btn:
             dismiss();
             break;

             case R.id.yes_btn:
                 dismiss();
                 getActivity().finish();
                 Long proposalId=getArguments().getLong("proposal_id");
                 removeSelectedproposal(proposalId);
                 break;
        }
    }

    private void removeSelectedproposal(Long proposalId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call=apiService.removeProposal(proposalId);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+ response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
               Log.e(TAG,"Failure"+t.toString());
            }
        });
    }
}
