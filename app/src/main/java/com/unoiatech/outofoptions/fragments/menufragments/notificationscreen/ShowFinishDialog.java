package com.unoiatech.outofoptions.fragments.menufragments.notificationscreen;

/**
 * Created by Unoiatech on 7/29/2017.
 */
public interface ShowFinishDialog {
    void errorDialog(String errorMsg);
}
