package com.unoiatech.outofoptions.fragments.main;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.api.Api;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.activity.NextLoginActivity;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.MyJobsListFragment;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.CollectStatusResponse;
import com.unoiatech.outofoptions.model.Login;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.activity.RegisterActivity;
import com.unoiatech.outofoptions.model.VerifyBankIdResponse;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.BankId.ExceptionHelper;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 4/8/2016.
 */
public class VerifyIdsFragment extends Fragment implements View.OnClickListener{
    private static final String TAG="VerifyIdsFragment";
    ImageView emailImage,fbImage,bankImage;
    Button verifyEmail,verifyFacebook,verifyBankId;
    OnRegisterViewListener mViewChangeListener;
    CallbackManager callbackManager;
    static Long verifiedId=0l;
    private String emailVerified="no";
    static AVLoadingIndicatorView progressBar;
    User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity());
        mViewChangeListener=(OnRegisterViewListener)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.verify_ids_layout,container,false);
        verifyEmail=(Button) view.findViewById(R.id.verify_email_button);
        //verifyPhone=(Button) view.findViewById(R.id.verify_phone_button);
        verifyFacebook=(Button) view.findViewById(R.id.verify_fb_button);
        verifyBankId=(Button) view.findViewById(R.id.verify_bank_id_button);
        emailImage=(ImageView)view.findViewById(R.id.email_image);
        fbImage=(ImageView)view.findViewById(R.id.facebook_image);
        //phoneImage=(ImageView)view.findViewById(R.id.phone_image);
        bankImage=(ImageView)view.findViewById(R.id.bank_image);
        emailImage.setBackgroundResource(R.drawable.inactive);
        fbImage.setBackgroundResource(R.drawable.facebook);
        //phoneImage.setBackgroundResource(R.drawable.phone);
        bankImage.setBackgroundResource(R.drawable.verified_bankid);
        Button getStarted=(Button) view.findViewById(R.id.get_started_button);
        progressBar= (AVLoadingIndicatorView) view.findViewById(R.id.progress_bar);

        //Get SharedPreferences Data
        user=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();

        //Method To Set already Verified BankId text and Image
        bankIdIsAlreadyVerified();

        //Get verified Ids
        getVerifiedIds(user.getId());

        //Set Onclick Listeners
        verifyFacebook.setOnClickListener(this);
        verifyEmail.setOnClickListener(this);
        //verifyPhone.setOnClickListener(this);
        getStarted.setOnClickListener(this);
        return view;
    }

    private void bankIdIsAlreadyVerified() {
        verifyBankId.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        verifyBankId.setText(getString(R.string.verified_text));
        verifyBankId.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
        verifyBankId.setAllCaps(true);
        verifyBankId.setTextColor(getResources().getColor(R.color.text_color));
    }

    private void AuthenticateUserViaFB() {
        Log.e("Authenticate_User","Authenticate_User");
        callbackManager= CallbackManager.Factory.create();

        /***set FaceBook Permissions***/
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("Success", "Success");
                    GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject json, GraphResponse response) {
                            if (response.getError() != null) {
                                // handle error
                                System.out.println("ERROR");
                            } else {
                                System.out.println("Success");
                                try {
                                    String jsonresult = String.valueOf(json);
                                    System.out.println("JSON Result" + jsonresult);
                                    String str_id = json.getString("id");
                                    if (str_id != null) {
                                        verifyIdsViaFb(str_id);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).executeAsync();
                }
                @Override
                public void onCancel() {
                    Log.d("On_cancel","On cancel");
                }
                @Override
                public void onError(FacebookException error) {
                    Log.d("on_Error",error.toString());
                }
            });
    }

    private void verifyIdsViaFb(String user_id) {
        final Login login= new Login();
        login.setIdValue(user_id);
        login.setIsVerified("0");
        login.setUserId(user.getId());
        login.setVerificationType(getString(R.string.fb_verification));

        Log.d(TAG,"VerifyIds"+" "+login.getUserId()+"  "+login.getIsVerified()+"  "+login.getIdValue()+"  "+login.getVerificationType());
        ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.verifyIdViaFacebook(login);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody>call, Response<ResponseBody> response) {
                Log.d(TAG, "VerifiedFacebook" + response+" "+response.message()+"   "+response.isSuccessful()+login.getUserId());
                if(response.code()==200){
                    fbImage.setBackgroundResource(R.drawable.fb);
                    verifyFacebook.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    verifyFacebook.setText(getString(R.string.verified_text));
                    verifyFacebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
                    verifyFacebook.setAllCaps(true);
                    verifyFacebook.setTextColor(getResources().getColor(R.color.text_color));
                }
                else{
                    ApiError apiError=ErrorUtils.parseError(response);
                    notifyUser(apiError.getMessage());
                }
            }
            @Override
            public void onFailure(Call<ResponseBody>call, Throwable t) {
                // Log error here since request failed
                Log.d(TAG, t.toString());}
        });
    }

    private void notifyUser(String message) {
        Snackbar.make(getView(),message,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(callbackManager.onActivityResult(requestCode, resultCode, data)){
            return;
        }
    }

    public void verifyEmail(String email) {
        Login login= new Login();
        login.setIdValue(email);
        login.setUserId(user.getId());
        login.setIsVerified("0");
        login.setVerificationType(getString(R.string.email_text));
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.verifyEmail(login);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void>call, Response<Void> response) {
                Log.d(TAG, "Verified" + response);
                if(response.isSuccessful()) {
                    emailImage.setBackgroundResource(R.drawable.email);
                    verifyEmail.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    verifyEmail.setText(getString(R.string.email_status));
                    //verifyEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
                    verifyEmail.setTextColor(getResources().getColor(R.color.unselected_menu));
                    verifyEmail.setAllCaps(true);
                    verifyEmail.setTextColor(getResources().getColor(R.color.text_color));
                    notifyUser(getString(R.string.verify_email));
                }
                else{
                    ApiError error=ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                    Log.e(TAG,"Erorr"+response.message());
                }
            }
            @Override
            public void onFailure(Call<Void>call, Throwable t) {
                Log.e(TAG, t.toString());}
        });
    }

    public void loginUser() {
        String email=((RegisterActivity)getActivity()).getEmail();
        String telNo=((RegisterActivity)getActivity()).getTelephoneNo();
        User user = new User();
        user.setEmail(email);
        user.setPhoneNo(telNo);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<User> call = apiService.loginUser(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User>call, Response<User> response) {
                if(response.code()==200) {
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).writeUserPref(response.body());
                    User userId= response.body();
                    Log.d(TAG, "LoginUser"+response.body()+userId.getId());
                    resisterDeviceId(userId.getId());
                }
                // Handle Error
                else {
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User>call, Throwable t) {
                // Log error here since request failed
                Log.d(TAG, "error"+t.toString());
            }
        });
    }

    /*********************Register Device Token
     * @param userId*********************/
    private void resisterDeviceId(Long userId) {
        String  fcmId=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).getFcmId();
        User user= new User();
        user.setDeviceId(fcmId);
        user.setDeviceType(getString(R.string.device_type));
        ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiInterface.registerDeviceId(userId,user);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void>call, Response<Void> response) {
                Log.d(TAG, "Registered" + response);
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getActivity(),NextLoginActivity.class);
                    getActivity().finish();
                    getActivity().startActivity(intent);
                }
                else{
                    ApiError error = ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }
            @Override
            public void onFailure(Call<Void>call, Throwable t) {
                // Log error here since request failed
                Log.d(TAG, t.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.verify_fb_button:
                AuthenticateUserViaFB();
                break;
            case R.id.verify_bank_id_button:
                //showAlert();
                break;
            case R.id.verify_email_button:
                String email = ((RegisterActivity)getActivity()).getEmail();
                verifyEmail(email);
                break;
            case R.id.get_started_button:
                mViewChangeListener.verifyIdsSelected();
                if(emailVerified.equals("verified")) {
                    //loginUser();
                   /* Intent intent = new Intent(getActivity(),HomeScreenActivity.class);
                    getActivity().finish();
                    getActivity().startActivity(intent);*/
                    getActivity().finish();
                }else{
                    notifyUser(getString(R.string.email_verification_text));
                }
                break;
        }
    }

    private void showAlert() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bank_id_alert_dialog_layout);

        Button anotherDevice=(Button) dialog.findViewById(R.id.another_device);
        Button thisDevice=(Button) dialog.findViewById(R.id.this_device);
        anotherDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showIdNumberAlert();
            }
        });

        thisDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start verification
                dialog.dismiss();
               // bankIdVerification();
            }
        });
        //show dialog
        dialog.show();
    }

    private void showIdNumberAlert() {
        final Dialog dialog = new Dialog(getActivity());
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.id_number_alert_dialog);

        final EditText idNumber=(EditText) dialog.findViewById(R.id.id_number);
        Button submit=(Button)dialog.findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //bankIdVerificationWithIdNumber(idNumber.getText().toString());
            }
        });
        //show dialog
        dialog.show();
    }

    private void getVerifiedIds(Long id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.verifyIdsOnProfile(id);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d("response","response"+" body" +response.body());
                showAccountVerification(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    public  void showAccountVerification(ArrayList<User> body) {
        for (User item : body) {
            if (item.getVerificationType().equals("email")) {
                if(item.getIsVerified()==1){
                    emailVerified="verified";
                }
            }
            else if(item.getVerificationType().equals("facebook")) {
                fbImage.setBackgroundResource(R.drawable.fb);
                verifyFacebook.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                verifyFacebook.setText(getString(R.string.verified_text));
                verifyFacebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
                verifyFacebook.setAllCaps(true);
                verifyFacebook.setTextColor(getResources().getColor(R.color.text_color));
            }
            Log.e("Email","Email"+"  "+emailVerified);
        }
    }

   /* private void bankIdVerificationWithIdNumber(String idNumber) {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<VerifyBankIdResponse> call = apiService.verifyBankIdWithIdNumber(idNumber,247l);
        call.enqueue(new Callback<VerifyBankIdResponse>() {
            @Override
            public void onResponse(Call<VerifyBankIdResponse>call, Response<VerifyBankIdResponse> response) {
                progressBar.setVisibility(View.GONE);
                if(response.code()==200) {
                    String autoStartToken = response.body().getAutoStartToken();
                    orderRef = response.body().getOrderRef();
                    verifiedId=response.body().getVerifiedId();
                    Log.d(TAG,"orderRef"+orderRef);
                    Log.d(TAG,"verifiedId"+verifiedId.toString());
                    showInstructionAlert();
                }

                else {
                    Log.d(TAG,"Message"+response.message());
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG,"Error Body"+error.toString());
                    Log.d(TAG+"Code",""+response.code());
                    Snackbar.make(getView(),error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VerifyBankIdResponse>call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, t.toString());
            }
        });
    }*/

    /*private void showInstructionAlert() {
        final Dialog dialog = new Dialog(getActivity());
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bank_id_app_instruction_layout);

        Button done=(Button) dialog.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                collectStatus(orderRef);
            }
        });
        //show dialog
        dialog.show();
    }*/

   /* private void bankIdVerification(){
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<VerifyBankIdResponse> call = apiService.verifyBankIdWithoutIdNumber(247l);
        call.enqueue(new Callback<VerifyBankIdResponse>() {
            @Override
            public void onResponse(Call<VerifyBankIdResponse>call, Response<VerifyBankIdResponse> response) {
                progressBar.setVisibility(View.GONE);
                if(response.code()==200) {
                    isBankIdVerification=true;
                    String autoStartToken = response.body().getAutoStartToken();
                    orderRef = response.body().getOrderRef();
                    verifiedId=response.body().getVerifiedId();
                    Log.d(TAG,"orderRef"+orderRef);
                    Log.d(TAG,"verifyId"+verifiedId.toString());

                    Intent intent = new Intent();
                    intent.setPackage("com.bankid.bus");
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("bankid:///?autostarttoken=" + autoStartToken + "&redirect=null "));
                    startActivity(intent);
                }
                else {
                    Log.d(TAG,"Message"+response.message());
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG,"Error Body"+error.toString());
                    Log.d(TAG+"Code",""+response.code());
                    Snackbar.make(getView(), error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<VerifyBankIdResponse>call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, t.toString());
            }
        });
    }
*/
    /*private void collectStatus(String orderRef) {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<User> call = apiService.collectStatus(orderRef);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User>call, Response<User> response) {
                progressBar.setVisibility(View.GONE);
                isBankIdVerification=false;
                if(response.code()==200) {
                    /*String ocspResponse = response.body().getOcspResponse();
                    String progressStatus = response.body().getProgressStatus();
                    Snackbar.make(getView(),
                            new ExceptionHelper(getContext()).getErrorMessage(progressStatus), Snackbar.LENGTH_SHORT).show();
                    Log.d(TAG ,"response code was 200");
                    if(progressStatus.equalsIgnoreCase("complete"))
                        setVerifiedImage("bankid");
                }
                else {
                    Log.d(TAG,"Message"+response.message());
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG,"Error Body"+error.toString());
                    Log.d(TAG+"Code",""+response.code());
                    Snackbar.make(getView(),
                            new ExceptionHelper(getContext()).getErrorMessage(error.getMessage()), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User>call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Snackbar.make(getView(),
                        "Server Error", Snackbar.LENGTH_SHORT).show();
                Log.e(TAG, t.toString());
            }
        });
    }*/

    private void setVerifiedImage(String type){
        switch (type) {
            case "facebook":
                fbImage.setImageResource(R.drawable.verified_fb);
                break;
            case "email":
                emailImage.setImageResource(R.drawable.verified_email);
                break;
            case "bankid":
                bankImage.setImageResource(R.drawable.verified_bankid);
                break;
            /*case "phone":
                phoneImage.setImageResource(R.drawable.verified_phone);
                break;*/
            default:
        }
    }
   @Override
    public void onResume() {
        super.onResume();
       /* if(isBankIdVerification){
            Log.d(TAG,"orderRef"+orderRef);
            collectStatus(orderRef);
        }*/
       EventBus.getDefault().register(VerifyIdsFragment.this);
   }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(VerifyIdsFragment.this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(DataReceivedEvent event){
        Log.e(TAG,"EVENt"+"  Event");
        emailVerified="verified";
        emailImage.setBackgroundResource(R.drawable.email);
        verifyEmail.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        verifyEmail.setText(getString(R.string.verified_text));
        verifyEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
        verifyEmail.setTextColor(getResources().getColor(R.color.unselected_menu));
        verifyEmail.setAllCaps(true);
        verifyEmail.setTextColor(getResources().getColor(R.color.text_color));
        notifyUser(getString(R.string.email_verified));
    }
}
