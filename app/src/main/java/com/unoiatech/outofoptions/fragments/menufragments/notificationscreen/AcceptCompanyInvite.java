package com.unoiatech.outofoptions.fragments.menufragments.notificationscreen;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.companymodule.OnDialogFinishListener;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.SearchJobsRequest;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 7/28/2017.
 */

public class AcceptCompanyInvite extends DialogFragment implements View.OnClickListener {
    private static String TAG = "AcceptCompanyInvite";
    AVLoadingIndicatorView progressBar,progressBar1;
    Long companyId;
    Dialog dialog;
    TextView accept,decline;
    ShowFinishDialog mCallback;

   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accept_company_invite_layout);

        TextView companyName=(TextView)findViewById(R.id.company_name);
        TextView companyLocation=(TextView)findViewById(R.id.company_location);
        ImageView companyLogo=(ImageView)findViewById(R.id.company_logo);

        //getData from intent
        Bundle bundle= getIntent().getExtras();
        companyName.setText(bundle.getString("company_name"));
        companyLocation.setText(bundle.getString("company_location"));
        companyId=bundle.getLong("company_id");
        Glide.with(this).load(S3Utils.getCompanylogo(companyId,bundle.getString("logo"))).into(companyLogo);

        TextView staticText=(TextView)findViewById(R.id.static_text);
        Button accept=(Button)findViewById(R.id.accept_btn);
        Button decline=(Button)findViewById(R.id.decline_btn);
        ImageView cancelImage=(ImageView)findViewById(R.id.cancel_image);
        //progressBar=(AVLoadingIndicatorView)findViewById(R.id.progress_bar);

        //set Text on other terms
        Spannable wordtoSpan=new SpannableString(getString(R.string.staff_member_static_text));
        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), 0,5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        staticText.setText(wordtoSpan);

        this.setFinishOnTouchOutside(false);

        accept.setOnClickListener(this);
        cancelImage.setOnClickListener(this);
        decline.setOnClickListener(this);
    }*/
    public static DialogFragment newInstance() {
        AcceptCompanyInvite frag = new AcceptCompanyInvite();
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.accept_company_invite_layout, container, false);

        TextView companyName=(TextView)view.findViewById(R.id.company_name);
        TextView companyLocation=(TextView)view.findViewById(R.id.company_location);
        ImageView companyLogo=(ImageView)view.findViewById(R.id.company_logo);

        //getData from intent
        Bundle bundle= getArguments();
        companyName.setText(bundle.getString("company_name"));
        companyLocation.setText(bundle.getString("company_location"));
        companyId=bundle.getLong("company_id");
        Glide.with(this).load(S3Utils.getCompanylogo(companyId,bundle.getString("logo"))).into(companyLogo);

        ImageView cancelImage=(ImageView)view.findViewById(R.id.cancel_image);
        TextView staticText=(TextView)view.findViewById(R.id.static_text);
        accept=(TextView)view.findViewById(R.id.accept_btn);
        decline=(TextView)view.findViewById(R.id.decline_btn);
        progressBar=(AVLoadingIndicatorView)view.findViewById(R.id.progress_bar);
        progressBar1=(AVLoadingIndicatorView)view.findViewById(R.id.progress_bar1);

        //set Text on other terms
        Spannable wordtoSpan=new SpannableString(getString(R.string.staff_member_static_text));
        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), 0,5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        staticText.setText(wordtoSpan);

        mCallback = (ShowFinishDialog) getActivity();

        accept.setOnClickListener(this);
        cancelImage.setOnClickListener(this);
        decline.setOnClickListener(this);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.accept_btn:
                acceptCompanyInvite("yes",companyId,progressBar1);
                break;

            case R.id.decline_btn:
                acceptCompanyInvite("no",companyId,progressBar);
                break;
            case R.id.cancel_image:
                dialog.dismiss();
                break;
        }
    }

    private void acceptCompanyInvite(String response, Long companyId, final AVLoadingIndicatorView progress_Bar) {
        progress_Bar.setVisibility(View.VISIBLE);
        if(response.equals("yes")){
            accept.setText("");
        }else{
            decline.setText("");
        }
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        Call<Void> call = apiService.inviteUserResponse(response,companyId,user.getId());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                progress_Bar.setVisibility(View.GONE);
                Log.e(TAG, "Response" + response + " " + response.body()+ " "+response.isSuccessful());
                if (response.isSuccessful()) {
                    Log.e("Successful","Successful");
                    markNotificationAsRead(getArguments().getLong("noti_id"));
                }
                else{
                    dialog.dismiss();
                    final ApiError error = ErrorUtils.parseError(response);
                    mCallback.errorDialog(error.getMessage());
                    Log.e("Response","REsponse"+error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "Error" + t.toString());
            }
        });
    }

    private void markNotificationAsRead(Long noti_id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        ArrayList<Long> list=new ArrayList<>();
        list.add(noti_id);
        Call<ResponseBody> call=apiService.markNotificationAsRead(list);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("response",""+response.body()+" "+response.message());
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Failure","Failure"+call.toString());
            }
        });
    }
}

