package com.unoiatech.outofoptions.fragments.instantjob;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.JobPostingDataHolder;
import com.unoiatech.outofoptions.util.common.view.CustomTypefaceSpan;

/**
 * Created by Unoiatech on 6/6/2017.
 */
public class TermsFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    OnViewChangeListener mlistener;
    FragmentTabHost mhost;
    String cancellation_hours,individual_proposal;
    RadioButton strict_radio_text,moderate_radio_text,flexible_radio_text;
    CheckBox individual_radio_btn;
    @Override
    public void  onAttach(Context context) {
        super.onAttach(context);
        mlistener = (OnViewChangeListener) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.terms_layout,container,false);
        TextView cancellation_term=(TextView)view.findViewById(R.id.cancellation_term_text);
        TextView other_terms=(TextView)view.findViewById(R.id.cancellation_terms);
        strict_radio_text=(RadioButton)view.findViewById(R.id.first_time_slot);
        moderate_radio_text=(RadioButton)view.findViewById(R.id.second_time_slot);
        flexible_radio_text=(RadioButton)view.findViewById(R.id.third_time_slot);
        individual_radio_btn=(CheckBox)view.findViewById(R.id.individual);

        /*****SlidingTAblayoutStrip***********/
        mlistener.showThirdView();
        mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
        FrameLayout nextButton = (FrameLayout)view.findViewById(R.id.next_button);
        FrameLayout backButton = (FrameLayout)view.findViewById(R.id.back_button);

        //Click listeners
        nextButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        individual_radio_btn.setOnClickListener(this);
        strict_radio_text.setOnCheckedChangeListener(this);
        moderate_radio_text.setOnCheckedChangeListener(this);
        flexible_radio_text.setOnCheckedChangeListener(this);

        //set text on cancellation terms
        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Montserrat-Bold.ttf");
        SpannableStringBuilder SS = new SpannableStringBuilder(getResources().getString(R.string.cancellation_term_text));
        SS.setSpan (new CustomTypefaceSpan("", font2), 8, 14,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        cancellation_term.setText(SS);

        //set Text on other terms
        Spannable wordtoSpan=new SpannableString(getString(R.string.cancellation_terms));
        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), wordtoSpan.length()-35, wordtoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        other_terms.setText(wordtoSpan);

        //set text on strict radio button
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Regular.ttf");
        SpannableStringBuilder radiobtn_text = new SpannableStringBuilder(getResources().getString(R.string.strict_text));
        radiobtn_text.setSpan (new CustomTypefaceSpan("", font),0 , 4,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        radiobtn_text.setSpan(new RelativeSizeSpan(1.7f), 0, 4,0);
        strict_radio_text.setText(radiobtn_text);

        //set text on moderate radio button
        SpannableStringBuilder moderate_radiobtn_text = new SpannableStringBuilder(getResources().getString(R.string.moderate_text));
        moderate_radiobtn_text.setSpan (new CustomTypefaceSpan("", font),0 ,4,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        moderate_radiobtn_text.setSpan(new RelativeSizeSpan(1.7f), 0, 4,0);
        moderate_radio_text.setText(moderate_radiobtn_text);

        //set text on Flexible radio button
        SpannableStringBuilder flexible_radiobtn_text = new SpannableStringBuilder(getResources().getString(R.string.flexible_text));
        flexible_radiobtn_text.setSpan (new CustomTypefaceSpan("", font),0 ,4,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        flexible_radiobtn_text.setSpan(new RelativeSizeSpan(1.7f), 0, 4,0);
        flexible_radio_text.setText(flexible_radiobtn_text);
        
        //Check if Reposted Data
        dataToRepost();
        return view;
    }

    private void dataToRepost() {
            if (mlistener.getRepostData()!=null) {
                try {
                    if (mlistener.getRepostData().getPreCancellationHours().equals("24")) {
                        flexible_radio_text.setChecked(true);
                    }
                    else if (mlistener.getRepostData().getPreCancellationHours().equals("12")) {
                        moderate_radio_text.setChecked(true);
                    }
                    else {
                        strict_radio_text.setChecked(true);
                    }
                    if (mlistener.getRepostData().getWhoCanSendProposal().equals("0")) {
                        individual_radio_btn.setChecked(false);
                    } else {
                        individual_radio_btn.setChecked(true);
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }
            }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.cancel:
                //getActivity().finish();
                Intent intent=new Intent(getActivity(),HomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.next_button:
                saveData();
                mhost.setCurrentTab(3);
                mlistener.hideThirdView();
                break;

            case R.id.back_button:
                mhost.setCurrentTab(1);
                mlistener.showThirdView();
                break;

           /* case R.id.individual:
                if(individual_radio_btn.isChecked()){
                    Log.e("IF_CHECKED","IF_CHECKED");
                    individual_radio_btn.setChecked(false);
                }
                else{
                    Log.e("ELSE_CHECKED","ELSE_CHECKED");
                    individual_radio_btn.setChecked(true);
                }
                break;*/
        }
    }

    private void saveData() {
        JobPostingDataHolder dataHolder= JobPostingDataHolder.getInstance();
        if(strict_radio_text.isChecked()){
            cancellation_hours="4";
        }else{cancellation_hours=cancellation_hours;}
        dataHolder.setCancellation_hours(cancellation_hours);
        if(individual_radio_btn.isChecked()){
           individual_proposal="1";
        }else{
            individual_proposal="0";
        }
        dataHolder.setIndividual(individual_proposal);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked) {
            if (compoundButton.getId() == R.id.first_time_slot) {
                cancellation_hours = "4";
            }
            if (compoundButton.getId() == R.id.second_time_slot) {
                cancellation_hours = "12";
            }
            if (compoundButton.getId() == R.id.third_time_slot) {
                cancellation_hours = "24";
            }
        }
    }
}
