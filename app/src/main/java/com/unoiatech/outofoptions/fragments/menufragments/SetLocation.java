package com.unoiatech.outofoptions.fragments.menufragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.FieldNamingPolicy;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.companymodule.registration.CompanyLocation;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.GlobalVariables;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 2/22/2017.
 */

public class SetLocation extends AppCompatActivity implements OnMapReadyCallback,View.OnClickListener,GoogleMap.OnMapClickListener
{
    GoogleMap mMap;
    MapView mapView;
    Marker marker;
    Double lat,lng;
    private LatLng location;
    private TextView placeAutoComplete;
    private static final String TAG="SetLocation";
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    String ret="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_location_layout);
        mapView=(MapView)findViewById(R.id.map);
        Button setLocation=(Button)findViewById(R.id.set_location_button);
        TextView title=(TextView)findViewById(R.id.title);
        placeAutoComplete=(TextView)findViewById(R.id.places_autocomplete);

        title.setText(getString(R.string.set_location_title));

        //Get Location
        getLocation();

        //OnClick Listener
        setLocation.setOnClickListener(this);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(SetLocation.this.getApplicationContext());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(this);
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        // start google's autocomplete widget
        placeAutoComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(SetLocation.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    Log.e(TAG, "GooglePlayServicesRepairableException" + e.toString());
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                    Log.e(TAG, "GooglePlayServicesNotAvailableException" + e.toString());
                }
            }
        });
    }

    private void getLocation() {
        GPSTracker gps = new GPSTracker(SetLocation.this);
        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
        }
        else{
            gps.showSettingsAlert();}
    }

    @Override
    public void onMapReady(GoogleMap googleMap){
        mMap=googleMap;
        LatLng origin=new LatLng(lat,lng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
        //get location when map is scroll
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.e("Camera postion change" + "", cameraPosition.target.latitude + " " + cameraPosition.target.longitude + "  " + cameraPosition);
                Double lat = cameraPosition.target.latitude;
                Double lng = cameraPosition.target.longitude;
                if (!lat.equals(0.0) && !lng.equals(0.0))
                    getAddress(lat,lng);
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    private String getAddress(Double mlat, Double mlng) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(mlat, mlng, 1);
            Log.e("Get_Address", "Get_Address" +mlat+" "+mlng+"  "+addresses.size());
            if (addresses != null&&addresses.size()>0) {
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                lat=addresses.get(0).getLatitude();
                lng=addresses.get(0).getLongitude();
                Log.e("State","State"+state+"  country"+country+lat+lng);
                if(state!=null){
                    ret=addresses.get(0).getAddressLine(0)+state+","+country;
                }else{
                    ret=country;
                }
                placeAutoComplete.setText(ret);
            } else {
                ret = "No Address returned!";
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ret = "Can't get Address!";
        }
        return ret;
    }

    //GEt Place Api Result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
                ret = place.getName().toString();
                placeAutoComplete.setText(ret);
                setMap(mMap, lat, lng);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void setMap(GoogleMap mMap, Double lat, Double lng) {
        if (mMap != null) {
            mMap = mMap;
            Log.e(TAG, " (map != null called");
            location = null;
            if (marker != null) {
                marker.remove();
            }
            location = new LatLng(lat, lng);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15));

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.set_location_button:
                saveLocation();
                finish();
                break;
        }
    }

    private void saveLocation() {
        Log.e("SaveLocation","SaveLocation"+ret+" "+getIntent().getStringExtra("place")+"  "+lat+"  "+lng);
        SharedPreferences preference=getSharedPreferences(GlobalVariables.USER_PREFS,MODE_PRIVATE);
        SharedPreferences.Editor editor= preference.edit();
        if(getIntent().getStringExtra("place").equals("Home")){
            editor.putString("home",ret+":"+lat+":"+lng);
            editor.commit();
        }
        else if(getIntent().getStringExtra("place").equals("Workplace")){
            editor.putString("workplace",ret+":"+lat+":"+lng);
            editor.commit();
        }
        else if(getIntent().getStringExtra("place").equals("Workplace1"))
        {
            editor.putString("workplace1",ret+":"+lat+":"+lng);
            editor.commit();
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Geocoder geocoder = new Geocoder(SetLocation.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude,latLng.longitude, 4);
            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i));//.append("\n");
                }
                ret = strReturnedAddress.toString()+":"+returnedAddress.getLatitude()+":"+returnedAddress.getLongitude();
            }
            else{
                ret = "No Address returned!";
            }
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ret = "Can't get Address!";
        }
        if (marker != null) {
            marker.remove();
        }
        MarkerOptions options = new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker))
                .position(new LatLng(latLng.latitude,
                        latLng.longitude));
        marker = mMap.addMarker(options);
        return;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

