package com.unoiatech.outofoptions.fragments.menufragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.activity.MainActivity;
import com.unoiatech.outofoptions.activity.MainActivityNew;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api .ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.notificationscreen.NotificationScreen;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.BlockedUserActivity;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 3/27/2017.
 */
public class SettingFragment extends Fragment implements View.OnClickListener {
    private static final String TAG="SettingFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_screen, container, false);
        Button logout_btn = (Button) view.findViewById(R.id.logout_btn);
        Button sendFeedback = (Button) view.findViewById(R.id.feedback_btn);
        Button add_location = (Button) view.findViewById(R.id.location);
        Button block_User_Btn=(Button)view.findViewById(R.id.block_user_btn);

        TextView title=(TextView)view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.setting_field_text));
        logout_btn.setOnClickListener(this);
        sendFeedback.setOnClickListener(this);
        block_User_Btn.setOnClickListener(this);
        add_location.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void logoutUser() {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        String fcmId=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).getFcmId();
        Call<Void> call= apiService.logoutUser(fcmId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i(TAG,"response"+response.toString());
                if(response.isSuccessful()){
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).removeUserPref();
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).removeIsEnable();
                    //getActivity().finish();
                    Intent intent= new Intent(getActivity(), MainActivityNew.class);
                    getActivity().finish();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                else {
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG + "Code", "" + response);
                    Snackbar.make(getView(),error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout_btn:
                logoutUser();
                break;
            case R.id.location:
                Intent intent = new Intent(getActivity(), AddLocation.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.feedback_btn:
                Intent email = new Intent(Intent.ACTION_SENDTO);
                String uriText = "mailto:" + Uri.encode("") +
                        "?subject=" + Uri.encode("Feedback") +
                        "&body=" + Uri.encode("the body of the message");
                Uri uri = Uri.parse(uriText);
                email.setData(uri);
                startActivity(Intent.createChooser(email, "Send mail..."));
                break;
            case R.id.block_user_btn:
                Intent intent1= new Intent(getActivity(), BlockedUserActivity.class);
                startActivity(intent1);
        }
    }
}
