package com.unoiatech.outofoptions.fragments.activejobs.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.JobProposalScreen;

/**
 * Created by Unoiatech on 5/8/2017.
 */

public class MarkJobCompleted extends DialogFragment implements View.OnClickListener
{
    Dialog dialog;
    public static DialogFragment newInstance() {
        MarkJobCompleted dialog= new MarkJobCompleted();
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.mark_job_completed,container,false);
        Button okay_btn=(Button)view.findViewById(R.id.okay_btn);

        okay_btn.setOnClickListener(this);
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog=super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.okay_btn:
                Intent intent = new Intent(getActivity(), JobProposalScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);
                break;
        }
    }
}
