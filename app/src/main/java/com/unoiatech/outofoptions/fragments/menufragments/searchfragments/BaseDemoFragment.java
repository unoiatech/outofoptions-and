package com.unoiatech.outofoptions.fragments.menufragments.searchfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.unoiatech.outofoptions.R;

/**
 * Created by Unoiatech on 6/30/2017.
 */

public abstract class BaseDemoFragment extends Fragment implements OnMapReadyCallback
{
    private GoogleMap mMap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.map,container,false);
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        //setUpMap();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        Log.e("OnMAp_REady","OnMap_ready");
        if (mMap != null) {
            return;
        }
        mMap = map;
        startDemo();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.e("OnActivity_Created","OnActivity_Created");
        super.onActivityCreated(savedInstanceState);
        setUpMap();
    }

    private void setUpMap() {
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    /**
     * Run the demo-specific code.
     */
    protected abstract void startDemo();

    protected GoogleMap getMap() {
        return mMap;
    }
}
