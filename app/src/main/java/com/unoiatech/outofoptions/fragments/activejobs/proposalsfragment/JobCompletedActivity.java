package com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.dialogs.MarkJobCompleted;
import com.unoiatech.outofoptions.model.Review;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 5/5/2017.
 */
public class JobCompletedActivity extends Activity implements View.OnClickListener,SimpleRatingBar.OnRatingBarChangeListener
{
    private static String TAG="JobCompletedActivity";
    private SimpleRatingBar communication_rating,punctuality_rating,quality_of_work_rating;
    private int communication,punctuality,quality_of_work;
    private String currentTime;
    private float average;
    private EditText review_description;
    private RelativeLayout punctualityLayout,qofLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_completed_screen);

        /*****Action bar components*********/
        TextView mTitle=(TextView)findViewById(R.id.title);
        mTitle.setText(getResources().getString(R.string.job_completed_title));

        Button submit=(Button)findViewById(R.id.submit_btn);
        punctualityLayout=(RelativeLayout)findViewById(R.id.punctuality_layout);
        qofLayout=(RelativeLayout)findViewById(R.id.qof_layout);
        communication_rating=(SimpleRatingBar)findViewById(R.id.communication_rating);
        punctuality_rating=(SimpleRatingBar)findViewById(R.id.punctuality_rating);
        quality_of_work_rating=(SimpleRatingBar)findViewById(R.id.qof_rating);
        review_description=(EditText)findViewById(R.id.description);
        submit.setOnClickListener(this);

        Log.e(TAG,"Data"+getIntent().getStringExtra("view_id"));

        if(getIntent().getStringExtra("view_id").equals("Job")){
            communication_rating.setOnRatingBarChangeListener(this);
            punctuality_rating.setOnRatingBarChangeListener(this);
            quality_of_work_rating.setOnRatingBarChangeListener(this);
        }
        else {
            punctualityLayout.setVisibility(View.GONE);
            qofLayout.setVisibility(View.GONE);
            punctuality_rating.setOnRatingBarChangeListener(this);
            quality_of_work_rating.setOnRatingBarChangeListener(this);
        }
        communication_rating.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                communication = (int) rating;
                Log.e("OnRatingChanged","OnRatingChanged"+communication+ " "+fromUser);
            }
        });
        punctuality_rating.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                punctuality = (int) rating;
            }
        });
        quality_of_work_rating.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                quality_of_work = (int) rating;
            }
        });
    }

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.ok_btn:
                Intent intent= new Intent(this, HomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.submit_btn:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                if(TextUtils.isEmpty(review_description.getText().toString())){
                    notifyUser();
                }
                else{
                    getCurrentTime();
                    Long jobId= getIntent().getLongExtra("job_id",0);
                    Long proposalId= getIntent().getLongExtra("proposal_id",0);

                    markJobAsCompleted(jobId,proposalId,getIntent().getLongExtra("user_id",0));}
                break;
        }
    }

    private void notifyUser(){
        Snackbar.make(getWindow().getDecorView().getRootView(), "Please add review description", Snackbar.LENGTH_LONG)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    private void markJobAsCompleted(Long jobId, Long proposalId, Long userId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call;
        long millis = System.currentTimeMillis();

        Log.e("MarkJob_Completed","MarkJob_completed"+getIntent().getStringExtra("view_id"));
        if(getIntent().getStringExtra("view_id").equals("Job")) {
            Log.e("Mark_Job","MArk_Job");
            call = apiService.markJobCompleted(jobId, proposalId, millis);
        }else{
            Log.e("Mark_Proposal","Mark_Proposal"+"  "+millis);
            call=apiService.markProposalCompleted(proposalId, (long) 508,millis);
        }
        Log.e(TAG,"MarkJobCompleted"+jobId+"  " +proposalId+"  "+currentTime);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+response.body()+"  "+response);
                if(response.isSuccessful()) {
                    addFeedBack();
                }
                else {
                    ApiError error= ErrorUtils.parseError(response);
                    Log.e("Error","Error"+error.getMessage());
                    Snackbar.make(getWindow().getDecorView().getRootView(),error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"Error"+t.toString());
            }
        });
    }

    private void addFeedBack(){
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        Review reviews= new Review();
        reviews.setReviewTime(currentTime);
        reviews.setDescriptin(review_description.getText().toString());
        reviews.setHireAgain(getIntent().getStringExtra("help"));
        reviews.setIsJobCompleted(getIntent().getStringExtra("job_status"));
        reviews.setRevieweeId(getIntent().getLongExtra("seller_id",0));
        reviews.setReviewerId(user.getId());
        //addReviews.setJobId(getIntent().getLongExtra("job_id",0));
        reviews.setCommunication(communication);
        reviews.setPunctuality(punctuality);
        reviews.setQualityOfWork(quality_of_work);
        reviews.setStars(Float.valueOf(average));

        Log.d("Data_to_Send","Data_to_send"+reviews.getQualityOfWork()+ reviews.getJobId()+ reviews.getCommunication()+"stars "
                +reviews.getStars()+"reviewId"+reviews.getRevieweeId()+" reviewerId"+reviews.getReviewerId()
                +" jobid"+reviews.getJobId()+" hireAgain"+reviews.getHireAgain()+" isJobCompleted"+" "+reviews.getReviewTime()
                +reviews.getIsJobCompleted());
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call;
        if(getIntent().getStringExtra("view_id").equals("Job")) {
            call = apiService.addBuyerReviews(getIntent().getLongExtra("job_id",0), reviews);
        }else {
            call = apiService.addReviews(getIntent().getLongExtra("job_id",0), reviews);
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+response+" "+response.message());
                if(response.isSuccessful()) {
                    showMarkJobCompletedDialog();
                }
                else {
                   ApiError error=ErrorUtils.parseError(response);
                   Log.d(TAG,"Error Body"+error.toString());
                    Snackbar.make(getWindow().getDecorView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"Error"+t.toString());
            }
        });

    }

    private void showMarkJobCompletedDialog() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        // Create and show the dialog.
        DialogFragment dialogFragment = MarkJobCompleted.newInstance();
        dialogFragment.show(ft,"dialog");
    }

    public void getCurrentTime() {
        Calendar now = Calendar.getInstance();
        try{
            DateFormat target = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
            currentTime= target.format(now.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(getIntent().getStringExtra("view_id").equals("Job")) {
            float total = (((float) communication + (float) punctuality + (float) quality_of_work) / 3);
            average = total;
        }
        else{
            average= communication;
        }
        Log.i("current_time","current_time"+currentTime+" "+average);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
        Log.e("OnRatingChanged","OnRatingChanged"+" "+simpleRatingBar+"  "+rating);
    }
}
