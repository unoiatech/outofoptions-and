package com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.RecievedProposalAdapter;
import com.unoiatech.outofoptions.fragments.activejobs.dialogs.JobCancellationDialog;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.SingleJobActivity;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.RequestJobData;
import com.unoiatech.outofoptions.model.JobProposalData;
import com.unoiatech.outofoptions.model.Proposal;
import com.unoiatech.outofoptions.model.PropsalResponse;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 4/10/2017.
 */
public class RecievedProposals extends Activity implements View.OnClickListener {
    RecievedProposalAdapter listAdapter;
    ArrayList<PropsalResponse> proposals;
    private static String TAG = "RecievedProposals";
    RecyclerView recyclerView;
    LinearLayout empty_view;
    Job job;
    Long jobId;
    TextView static_text;
    JobProposalData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.proposal_recieved);

        /*****ActionBarComponents**************/
        TextView actionBarTitle = (TextView) findViewById(R.id.title);

        TextView jobTitle = (TextView) findViewById(R.id.job_title);
        recyclerView = (RecyclerView) findViewById(R.id.my_jobs_recycler_view);
        empty_view = (LinearLayout) findViewById(R.id.empty_view);
        static_text=(TextView)findViewById(R.id.static_text);

        ImageView job_info=(ImageView)findViewById(R.id.job_info_image);
        Button cancel_job = (Button) findViewById(R.id.cancel_job);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        /*******DisplayData**********************/
        actionBarTitle.setText(R.string.received_proposal_toolbar_title);
        jobTitle.setText(getIntent().getStringExtra("jobTitle"));
        jobId = getIntent().getLongExtra("jobId",0);
        Log.e("Job_iD", "Job_Id" + jobId + "  " +"  "+System.currentTimeMillis());
        cancel_job.setOnClickListener(this);
        job_info.setOnClickListener(this);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Proposal proposal = proposals.get(position).getProposal();
                Intent intent = new Intent(RecievedProposals.this, ProposalDetails.class);
                data=new JobProposalData();
                data.setSellerName(proposals.get(position).getUser().getFirstName() + " " + proposals.get(position).getUser().getLastName());
                data.setSellerId(proposal.getUserID());
                data.setProposalId(proposal.getId());
                data.setProposalStartingTime(proposal.getStartingTime());
                data.setProposalDescription(proposal.getProposalDescription());
                data.setOfferedQuote(proposal.getOfferedQuote());
                data.setViewId(getIntent().getStringExtra("view_id"));
                data.setJobTitle(job.getTitle());
                data.setJobId(job.getId());
                data.setLat(job.getLat());
                data.setLng(job.getLng());
                data.setCategory(job.getCategory());
                data.setUserId(job.getUserId());
                intent.putExtra("job_data",data);
                //finish();
                startActivity(intent);
            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,"OnResume");
        getJobWithExtraData(jobId);
    }

    private void getJobWithExtraData(Long jobId) {
        proposals= new ArrayList<>();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<PropsalResponse> call = apiService.getExtraJobData(jobId);
        call.enqueue(new Callback<PropsalResponse>() {
            @Override
            public void onResponse(Call<PropsalResponse> call, Response<PropsalResponse> response) {
                Log.d(TAG, "Response" + " size" + response.body().getProposals().size());
                if (response.code() == 200) {
                    if (response.body().getProposals().size()> 0) {
                        for(int i=0;i<response.body().getProposals().size();i++){
                            PropsalResponse  proposalResponse= new PropsalResponse();
                            Log.e("For_Loop","For_loop"+response.body().getProposals().get(i).getProposal().getProposalStatus());
                            if(response.body().getProposals().get(i).getProposal().getProposalStatus().equals("pending")) {
                                proposalResponse.setProposal(response.body().getProposals().get(i).getProposal());
                                proposalResponse.setSeller_reviews(response.body().getProposals().get(i).getSeller_reviews());
                                proposalResponse.setAverage_review(response.body().getProposals().get(i).getAverage_review());
                                proposalResponse.setBuyer_reviews(response.body().getProposals().get(i).getBuyer_reviews());
                                proposalResponse.setUser(response.body().getProposals().get(i).getUser());
                                job=response.body().getJob();
                                proposals.add(proposalResponse);
                            }
                        }
                    }
                }
                displayData();
            }

            @Override
            public void onFailure(Call<PropsalResponse> call, Throwable t) {
                Log.d(TAG, "Error" + t.toString());
            }
        });
    }

    private void displayData() {
        if (proposals.size()> 0) {
            if(getIntent().getStringExtra("view_id").equals("pending")){
                listAdapter = new RecievedProposalAdapter(RecievedProposals.this, proposals);
                recyclerView.setAdapter(listAdapter);
            }
        }
        else {
            recyclerView.setVisibility(View.GONE);
            empty_view.setVisibility(View.VISIBLE);
            static_text.setText(R.string.proposal_received_static_text);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_job:
                showCancellationDialog();
                break;
            case R.id.job_info_image:
                User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
                Intent intent= new Intent(this,SingleJobActivity.class);
                intent.putExtra("status","Detail");
                intent.putExtra("jobId",jobId);
                intent.putExtra("userId",user.getId());
                startActivity(intent);
                break;
        }
    }

    private void showCancellationDialog() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment dialogFragment = JobCancellationDialog.newInstance();
        RequestJobData job = new RequestJobData();
        job.setJobId(jobId);
        job.setTime(System.currentTimeMillis());
        Bundle bundle= new Bundle();
        bundle.putParcelable("job_info",job);
        bundle.putString("view","Recieved_Proposal");
        dialogFragment.setArguments(bundle);
        dialogFragment.show(ft,"dialog");
    }
}
