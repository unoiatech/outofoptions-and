package com.unoiatech.outofoptions.fragments.activeproposals;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment.FeedbackActivity;
import com.unoiatech.outofoptions.fragments.activeproposals.dialogs.JobConfirmation;
import com.unoiatech.outofoptions.fragments.activeproposals.dialogs.WithdrawProposalDialog;
import com.unoiatech.outofoptions.fragments.instantjob.AsynResponse;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.SingleJobActivity;
import com.unoiatech.outofoptions.model.AcceptProposal;
import com.unoiatech.outofoptions.model.JobProposalData;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.GetDirectionPath;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 4/28/2017.
 */

public class ProposalInfoFragment extends Fragment implements OnMapReadyCallback,View.OnClickListener,AsynResponse {
    MapView mapView;
    GoogleMap map;
    TextView distance_text;
    String currentTime;
    JobProposalData proposalData;
    LinearLayout withdrawLayout;
    TextView proposalCompleted;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.proposal_info_layout,container,false);
        TextView job_title=(TextView)view.findViewById(R.id.job_title);
        TextView price_quote=(TextView)view.findViewById(R.id.budget);
        TextView time_to_start=(TextView)view.findViewById(R.id.time_to_start);
        TextView proposal_description=(TextView)view.findViewById(R.id.proposal_description);
        mapView=(MapView)view.findViewById(R.id.map);

        LinearLayout job_info=(LinearLayout) view.findViewById(R.id.info);
        LinearLayout proposal_view=(LinearLayout)view.findViewById(R.id.proposal_line_view);
        distance_text=(TextView)view.findViewById(R.id.distance);

        LinearLayout proposal_layout= (LinearLayout)view.findViewById(R.id.proposal_layout);
        TextView time_text=(TextView)view.findViewById(R.id.time_text_field);
        TextView budget_text=(TextView)view.findViewById(R.id.budget_text_field);
        ScrollView main_scrollview=(ScrollView)view.findViewById(R.id.main_scroll);
        ImageView transparentImage=(ImageView)view.findViewById(R.id.transparent_image);

        //Bottom Response layout
        Button withdraw_btn =(Button)view.findViewById(R.id.withdraw_btn);
        Button chat_btn=(Button)view.findViewById(R.id.chat_btn);
        withdrawLayout=(LinearLayout)view.findViewById(R.id.withdraw_layout);
        proposalCompleted=(TextView)view.findViewById(R.id.proposal_completed);

        /***GetIntent Data*******/
        proposalData= getArguments().getParcelable("proposal_info");

        job_title.setText(proposalData.getJobTitle());
        price_quote.setText(proposalData.getOfferedQuote()+ " SEK");
        try {
            time_to_start.setText(DateTimeConverter.getTimeToComplete(proposalData.getProposalCompletionTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        /****Show Job Confirmation Dialog**********/
        if(proposalData.getProposalStatus().equals("accepted")){
            Intent jobConfirmation= new Intent(getActivity(), JobConfirmation.class);
            Bundle bundle= new Bundle();
            bundle.putString("job_title", proposalData.getJobTitle());
            bundle.putLong("proposal_id",proposalData.getProposalId());
            jobConfirmation.putExtras(bundle);
            startActivity(jobConfirmation);
        }

        /*****OnClick Listener********/
        withdraw_btn.setOnClickListener(this);
        job_info.setOnClickListener(this);
        initialiseMap(savedInstanceState);

        /******Changes at Runtime************/
        checkViewId(proposal_layout,proposal_view,time_text,budget_text,withdraw_btn,chat_btn,proposal_description,proposalData);
        scrollMapView(main_scrollview,transparentImage);
        return view;
    }

    /*********Scroll MapView inside ScrollView************/
    private void scrollMapView(final ScrollView main_scrollview, ImageView transparentImage) {
        transparentImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        main_scrollview.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        main_scrollview.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        main_scrollview.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });
    }

    private void checkViewId(LinearLayout proposal_layout, LinearLayout view, TextView time_text, TextView budget_text, Button withdraw_btn, Button chat_btn, TextView proposal_description, JobProposalData proposal) {
        if(proposal.getViewId().equals("accepted")) {
            proposal_layout.setVisibility(View.VISIBLE);
            time_text.setText("Working Date");
            budget_text.setText("Agreed");

            proposal_description.setText(proposal.getProposalDescription());
            withdrawLayout.setVisibility(View.GONE);
            proposalCompleted.setVisibility(View.VISIBLE);
            proposalCompleted.setText("JOB COMPLETED");
            proposalCompleted.setOnClickListener(this);
        }
        else if(proposal.getViewId().equals("Completed")) {
            proposal_layout.setVisibility(View.GONE);
            time_text.setText("Completed on");
            budget_text.setText("Paid");
            //response_button.setVisibility(View.GONE);
        }
        else {
            proposal_layout.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            proposal_description.setText(proposal.getProposalDescription());
            time_text.setText(R.string.pending_proposal_time);
            budget_text.setText(R.string.pending_proposal_quoted);
            withdraw_btn.setText(R.string.withdraw_btn);
            chat_btn.setText(R.string.chat_btn);
        }
    }

    private void initialiseMap(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;

        /******Get Data************/
        String currentLocation=((JobDetails)getActivity()).getLocation();
        String[] location=currentLocation.split(":");
        String category=proposalData.getCategory();
        Log.e("DAta","Data"+proposalData.getCategory()+" "+proposalData);
        Double lat,lng;
        lat=proposalData.getLat();
        lng=proposalData.getLng();

        LatLng start = new LatLng(Double.parseDouble(location[0]), Double.parseDouble(location[1]));
        LatLng end = new LatLng(lat,lng);
        map.moveCamera(CameraUpdateFactory.newLatLng(start));
        map.animateCamera(CameraUpdateFactory.zoomTo(12));
        map.addMarker(new MarkerOptions().position(start).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
        if(category.equals("Animals sitting & Pet care"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.animal_passing)));
        }
        else if(category.equals("Computer & tech fixes"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.computer_and_networking)));
        }
        else if(category.equals("Ground & Construction"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.ground_and_construction_gray)));
        }
        else if(category.equals("Carrying & Assembling"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.carring_assemble)));
        }
        else if(category.equals("Health & Beauty")){
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.minor_tech)));
        }
        else if(category.equals("Photoshoot"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.photographer_green)));
        }
        else if(category.equals("Home & Gardening"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.house_and_gardning)));
        }
        else if(category.equals("Car & Vehicles"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.cleaning_green)));
        }
        else if(category.equals("All")){
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.cleaning_green)));
        }

        /*****Display Path********/
        if (lat!= null&lng!=null) {
            String sensor = "sensor=false";
            String source = "origin="+location[0]+ ","+ location[1];
            String destination = "destination="+lat +","+ lng;
            String parameters = source + "&" + destination + "&" + sensor;
            String output = "json";
            String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
            GetDirectionPath downloadtask = new GetDirectionPath();
            downloadtask.response=this;
            downloadtask.execute(url);
        }
    }

    @Override
    public void processFinish(List<List<HashMap<String, String>>> result) {
        ArrayList<LatLng> points = null;
        PolylineOptions lineOptions = null;
        // Traversing through all the routes
        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList<LatLng>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = result.get(i);
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);
                if(j==0){
                    String distance =point.get("distance");
                    //String[] dist=distance.split("km|m");
                    //String totalDistance=dist[0];
                    Log.e("Distance","Distance"+distance);
                    distance_text.setText(distance);
                   /* double distance_in_miles=Double.parseDouble(totalDistance)*0.621;
                    double roundOff = Math.round(distance_in_miles * 100.0) / 100.0;
                    distanceText.setText(String.valueOf(roundOff)+" miles away");*/
                    continue;}
                else if(j==1) {
                    /*duration = (String)point.get("duration");
                    timeText.setText(duration);*/
                    continue;
                }
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);}
            //Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            lineOptions.width(6);
            lineOptions.color(Color.rgb(65,199,88));
        }
        // Drawing polyline in the Google Map for the i-th route
        map.addPolyline(lineOptions);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.info:
                Intent intent= new Intent(getActivity(),SingleJobActivity.class);
                intent.putExtra("status","Detail");
                intent.putExtra("jobId",proposalData.getJobId());
                intent.putExtra("userId",proposalData.getSellerId());
                startActivity(intent);
                break;
            case R.id.withdraw_btn:
                CheckViewStatus();
                break;
            case R.id.proposal_completed:
                Intent intent1 = new Intent(getActivity(),FeedbackActivity.class);
                intent1.putExtra("job_id", proposalData.getJobId());
                intent1.putExtra("view_id", "Proposal");
                intent1.putExtra("proposal_id",proposalData.getProposalId());
                intent1.putExtra("seller_id",proposalData.getSellerId() );
                startActivity(intent1);
                Log.e("Proposal_info","Proposal_Id"+proposalData.getProposalId()+" "+proposalData.getSellerId());
                break;
        }
    }

   /* private void getCurrentTime() {
        Calendar now= Calendar.getInstance();
        DateFormat target = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
        currentTime= target.format(now.getTime());
        Log.i("current_time","current_time"+currentTime);
    }*/

    private void CheckViewStatus() {
        if(proposalData.getViewId().equals("accepted")) {
            Intent intent = new Intent(getActivity(), FeedbackActivity.class);
            intent.putExtra("job_id", proposalData.getJobId());
            intent.putExtra("view_id", "Active");
            intent.putExtra("user_id",proposalData.getSellerId() );
            startActivity(intent);
        }
        else {
            FragmentTransaction ft= getActivity().getFragmentManager().beginTransaction();
            ft.addToBackStack(null);
            // Create and show the dialog.
            DialogFragment withdrawDialog = WithdrawProposalDialog.newInstance();
            Bundle bundle= new Bundle();
            bundle.putLong("proposal_id",proposalData.getProposalId());
            withdrawDialog.setArguments(bundle);
            withdrawDialog.show(ft, "dialog");
        }
    }

    public void acceptProposal(String decison, Long proposal_id) {
        AcceptProposal proposal= new AcceptProposal();
        proposal.setProposalId(proposal_id);
        proposal.setResponse(decison);
        proposal.setTime(currentTime);
        Log.e("AcceptProposal","AcceptProposal"+proposal.getProposalId()+" "+proposal.getResponse());
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= apiService.acceptProposal(proposal);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("Accept_Proposal","Response"+"  "+response.body()+response);
                if(response.isSuccessful()) {
                    getActivity().finish();
                }else{
                    ApiError error= ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                    Log.e("Api_Integration","Api_Integration"+"  "+error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("OnFailure","OnFailure"+t.toString());
            }
        });
    }
}