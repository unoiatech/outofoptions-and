package com.unoiatech.outofoptions.fragments.main;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.common.view.AutoAddTextWatcher;

/**
 * Created by Unoiatech on 10/24/2017.
 */

public class AccountFragmentNew extends Fragment implements View.OnTouchListener {
    OnAccountSelectedListener mCallback;
    Button backButton;
    EditText emailEditText,telephoneEdit;
    ViewPager viewPager;
    public static final String EMAIL_PATTERN="[^\\s@]+@[^\\s@]+\\.[a-zA-z][a-zA-Z][a-zA-Z]*";
    OnRegisterViewListener mViewChangeListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.account_frag_new,container,false);

        //Get Widget Reference from Xml file
        emailEditText=(EditText)view.findViewById(R.id.email_edit_text);
        telephoneEdit=(EditText)view.findViewById(R.id.telephone_text);
        Button nextButton=(Button)view.findViewById(R.id.next_btn);
        viewPager= (ViewPager) getActivity().findViewById(R.id.viewpager);

        mViewChangeListener.accountTabSelected();
        backButton=(Button)view.findViewById(R.id.back_button);

        /*************Set OnClickListener**************/
        emailEditText.setOnTouchListener(this);

        //phone number validation check
        telephoneEdit.addTextChangedListener(new AutoAddTextWatcher(telephoneEdit,"+",0));
        telephoneEdit.addTextChangedListener(new AutoAddTextWatcher(telephoneEdit,"-",3));
        telephoneEdit.setOnTouchListener(this);

        /**********onEditorAction on EditText*****************/
        emailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(emailEditText,getString(R.string.empty_email_address),getString(R.string.invalid_email_address));
                }
                return false;
            }
        });

        telephoneEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(telephoneEdit,getString(R.string.empty_telephone_number),getString(R.string.invalid_number));
                }
                return false;
            }
        });

        // Move to User Info Screen
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if(!emailEditText.getText().toString().isEmpty()&&!telephoneEdit.getText().toString().isEmpty()) {
                    if (!emailEditText.getText().toString().isEmpty()) {
                        checkEnteredData(emailEditText,getString(R.string.empty_email_address),getString(R.string.invalid_email_address));
                    }
                    if (!telephoneEdit.getText().toString().isEmpty()) {
                        checkEnteredData(telephoneEdit,getString(R.string.empty_telephone_number),getString(R.string.invalid_number));
                    }
                    else {
                        mCallback.onAccountSelected(emailEditText.getText().toString(),telephoneEdit.getText().toString());

                        /********Custom SlidingTAbStripeView*********/
                        mViewChangeListener.userInfoSelected();
                        viewPager.setCurrentItem(1);
                    }
                }
                else {
                    if(emailEditText.getText().toString().isEmpty()) {
                        notifyUserForInvalidFields(getString(R.string.empty_email_address));
                    }
                    else if(telephoneEdit.getText().toString().isEmpty()){
                        notifyUserForInvalidFields(getString(R.string.empty_telephone_number));
                    }
                }
            }
        });
        //Back Button
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent= new Intent(getActivity(), MainActivity.class);
                getActivity().finish();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);*/
                getActivity().finish();
            }
        });

        return view;
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null : getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void checkEnteredData(EditText editText, String message, String msg) {
        if(!editText.getText().toString().isEmpty()) {
            if (editText.equals(emailEditText)) {
                if (editText.getText().toString().matches(EMAIL_PATTERN)==false) {
                    notifyUserForInvalidFields(msg);
                    setFocus(editText);
                }else{
                    telephoneEdit.setFocusableInTouchMode(true);
                    telephoneEdit.setFocusable(true);
                    telephoneEdit.setCursorVisible(true);
                }
            }

            else if (editText.equals(telephoneEdit)) {
                if(editText.getText().toString().isEmpty()) {
                    notifyUserForInvalidFields(msg);
                    setFocus(editText);
                }
                else if(editText.getText().toString().length()<13) {
                        notifyUserForInvalidFields(msg);
                    }
                else {
                    mCallback.onAccountSelected(emailEditText.getText().toString(),telephoneEdit.getText().toString());
                    mViewChangeListener.userInfoSelected();
                    viewPager.setCurrentItem(1);
                }
            }
        }
        else {
            notifyUserForInvalidFields(message);
            setFocus(editText);
        }
    }

    private void setFocus(EditText editText) {
        if(editText.equals(emailEditText)) {
            telephoneEdit.setFocusable(false);
        }
        else if(editText.equals(telephoneEdit)){
            emailEditText.setFocusable(false);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.email_edit_text:
                emailEditText.setFocusableInTouchMode(true);
                emailEditText.setCursorVisible(true);
                emailEditText.setFocusable(true);
                break;
            case R.id.telephone_text:
                checkEnteredData(emailEditText,getString(R.string.empty_email_address),getString(R.string.invalid_email_address));
                break;
        }
        return false;
    }

    //Container Activity must implement this interface
    public interface OnAccountSelectedListener {
        public void onAccountSelected(String email, String telNumber);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = context instanceof Activity ? (Activity) context : null;
        try {
            mCallback = (AccountFragmentNew.OnAccountSelectedListener) activity;

            /****Hide View*******/
            mViewChangeListener=(OnRegisterViewListener) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAccountSelectedListener");
        }
    }

    void notifyUserForInvalidFields(String message) {
        hideKeyboard();
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                })
                .setActionTextColor(getResources().getColor(R.color.app_color))
                .show();
    }
}
