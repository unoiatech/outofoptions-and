package com.unoiatech.outofoptions.fragments.activejobs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.fragments.activeproposals.ProposalFragment;
import com.unoiatech.outofoptions.jobdetail.chat.ChatFragment;
import com.unoiatech.outofoptions.model.Notification;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 4/2/2016.
 */
public class JobsFragment extends Fragment {
    private static final String TAG = "JobsFragment";
    private ArrayList<ActiveJobsResponse> data_list = new ArrayList<>();
    TextView tv_title;
    ArrayList<Notification> active;
    ArrayList<Notification> pending;
    ArrayList<Notification> completed;
    int[] unreadCount;
    String[] tabTitle = {"Active", "Pending Proposals", "Completed"};
    TabLayout mTabLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e(TAG, "OnCreate");
        View view = inflater.inflate(R.layout.seller_jobs_layout, container, false);
        final ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        active= new ArrayList<>();
        pending= new ArrayList<>();
        completed= new ArrayList<>();
        EventBus.getDefault().register(JobsFragment.this);
        mViewPager.setAdapter(new MyAdapter(getChildFragmentManager(),3));

        //Initializing viewPager
        mViewPager.setOffscreenPageLimit(3);
        //setupViewPager(mViewPager);

        //Initializing the tablayout
        mTabLayout.setupWithViewPager(mViewPager);

        try {
            setupTabIcons();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mViewPager.setCurrentItem(position, false);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return view;
    }

    private void setupTabIcons() {
        for (int i = 0; i < tabTitle.length; i++) {
            /*TabLayout.Tab tabitem = tabLayout.newTab();
            tabitem.setCustomView(prepareTabView(i));
            tabLayout.addTab(tabitem);*/

            mTabLayout.getTabAt(i).setCustomView(prepareTabView(i));
        }
    }

    private View prepareTabView(int pos) {
        View view1 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab, null);
        tv_title = (TextView) view1.findViewById(R.id.tv_title);
        TextView tv_count = (TextView) view1.findViewById(R.id.tv_count);
        tv_title.setText(tabTitle[pos]);
        if (unreadCount[pos] > 0) {
            tv_count.setVisibility(View.VISIBLE);
            tv_count.setText("" + unreadCount[pos]);
        } else
            tv_count.setVisibility(View.GONE);
        Log.e("PrepareTabView","PrepareTabView"+"  "+unreadCount.length+"  "+unreadCount[pos]);
        return view1;
    }

/*
        initTabLayout(mViewPager,mTabLayout);

        // Register Even Bus to Receive Notification Badge

        active= new ArrayList<>();
        pending= new ArrayList<>();
        completed= new ArrayList<>();
        return view;
    }

    private void initTabLayout(ViewPager mViewPager, TabLayout mTabLayout) {
        // Customise TabLayout To show Notification Badge
        View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.seller_jobs_layout, null, false);

        activeTab = (TextView) headerView.findViewById(R.id.tvtab1);
        pendingTab = (TextView) headerView.findViewById(R.id.tvtab2);
        completedTab = (TextView) headerView.findViewById(R.id.tvtab3);

        mTabLayout.addTab(mTabLayout.newTab().setCustomView(activeTab));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(pendingTab));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(completedTab));

        */

    /***
     * Show Badges
     ***//*
        BadgeView badge= GetBadgeView.getBadgeAtRight(getActivity());
        badge.setBadgeCount(0).bind(activeTab);

        BadgeView badge1= GetBadgeView.getBadgeAtRight(getActivity());
        badge1.setBadgeCount(0).bind(pendingTab);

        BadgeView badge2= GetBadgeView.getBadgeAtRight(getActivity());
        badge2.setBadgeCount(50).bind(completedTab);

        mViewPager.setAdapter(new MyAdapter(getChildFragmentManager(),3));
        mTabLayout.setupWithViewPager(mViewPager);

    }*/

    public class MyAdapter extends FragmentStatePagerAdapter {
        int tabcount;

        public MyAdapter(FragmentManager fm, int childCount) {
            super(fm);
            this.tabcount = childCount;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    MyJobsListFragment f = new MyJobsListFragment();
                    Bundle b = new Bundle();
                    b.putString("type", "active");
                    f.setArguments(b);
                    return f;

                case 1:
                    MyJobsListFragment f1 = new MyJobsListFragment();
                    Bundle b1 = new Bundle();
                    b1.putString("type", "pending");
                    f1.setArguments(b1);
                    return f1;

                case 2:
                    MyJobsListFragment f2 = new MyJobsListFragment();
                    Bundle b2 = new Bundle();
                    b2.putString("type", "completed");
                    f2.setArguments(b2);
                    return f2;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
          /* switch (position) {
                case 0:
                    return tabTitle[position];
                    //return getResources().getString(R.string.active_job);
                case 1:
                    return tabTitle[position];
                    //return getResources().getString(R.string.pending_proposal);
                case 2:
                    return tabTitle[position];
                    //return getResources().getString(R.string.completed_job);
            }*/
            return tabTitle[position];
        }


        @Override
        public int getCount() {
            return tabcount;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        getPostedJobList();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
        EventBus.getDefault().unregister(JobsFragment.this);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(TAG, "onActivityCreated");
    }

    public void getPostedJobList() {
        User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        data_list.clear();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<ActiveJobsResponse>> call = apiService.getPostedJobs(user.getId());
        call.enqueue(new Callback<ArrayList<ActiveJobsResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<ActiveJobsResponse>> call, Response<ArrayList<ActiveJobsResponse>> response) {
                Log.d(TAG, "RESPONSE" + response.body().size() + "  " + response);
                if (response.isSuccessful()) {
                    data_list.addAll(response.body());
                    EventBus.getDefault().post(new DataReceivedEvent("Hello EventBus!"));
                } else {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ActiveJobsResponse>> call, Throwable t) {
                Log.d(TAG, "Failure" + t.toString());
            }
        });
    }

    public ArrayList<ActiveJobsResponse> getData() {
        return data_list;
    }

    @Subscribe
    public void onEvent(DataReceivedEvent event) {
        Log.i("Event_Fired", "Event_Fired");
        getActiveJobs();
    }

    private void getActiveJobs() {
        JobProposalScreen activity = (JobProposalScreen) getActivity();
        ArrayList<Notification> data = activity.getData();
        ArrayList where = new ArrayList<>();
        Log.i("GetActive_Jobs", "GetActive_jobs" + data.size());

        for (Notification item : data) {
            Log.i("For", "For" + item.getActivity());
            if (item.getActivity().equals("ADDED_PROPOSAL") || item.getActivity().equals("REFUSED_PROPOSAL")) {
                Log.i("Mon", "Mon");
                pending.add(item);

            } else if (item.getActivity().equals("CONFIRMED_PROPOSAL")) {
                Log.i("MonM", "MonM");
                active.add(item);
            } else if (item.getActivity().equals("COMPLETED_PROPOSAL")) {
                Log.i("MonM", "MonM");
                completed.add(item);
            }
        }
        where.add(active.size());
        where.add(pending.size());
        where.add(completed.size());

       unreadCount = new int[where.size()];

        for (int i = 0; i < where.size(); i++) {
            if (where.get(i) != null) {
                unreadCount[i] = (int) where.get(i);
            }
        }
        Log.e("DATAAAA","DATAAA"+unreadCount.length);
    }
}
