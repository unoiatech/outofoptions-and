package com.unoiatech.outofoptions.fragments.menufragments.notificationscreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.SingleJobActivity;
import com.unoiatech.outofoptions.fragments.offerhelp.ChooseStaffMembers;
import com.unoiatech.outofoptions.model.CompanyProposal;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.DateTimeConverter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 8/16/2017.
 */

public class ReviewProposal extends AppCompatActivity implements View.OnClickListener{
    TextView completionTime, startingTime,offeredBid,service_Fee,budget,chooseStaffMembers,proposalDescrption;
    private static String TAG="ReviewProposal";
    ArrayList<User> staffMembers;
    Job job;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_proposal);

        //Action bar Components
        TextView mTitle=(TextView)findViewById(R.id.title);
        mTitle.setText(getString(R.string.review_proposal_title));

        TextView job_title=(TextView)findViewById(R.id.job_title);
        Button reviewResponse = (Button) findViewById(R.id.submit);
        ImageView jobInfo=(ImageView)findViewById(R.id.info);
        offeredBid= (TextView)findViewById(R.id.bid);
        service_Fee=(TextView)findViewById(R.id.service_fee);
        budget=(TextView)findViewById(R.id.budget);
        startingTime = (TextView) findViewById(R.id.starting_time);
        completionTime = (TextView) findViewById(R.id.completion_time);
        proposalDescrption = (TextView) findViewById(R.id.message);
        chooseStaffMembers=(TextView)findViewById(R.id.choose_staff_member);

        //Get Company proposal Detail
        job_title.setText(getIntent().getStringExtra("jobTitle"));
        getCompanyProposalDetail(getIntent().getLongExtra("companyProposalId",0));

        staffMembers= new ArrayList<>();
        job= new Job();

        //Set OnClickListener
        chooseStaffMembers.setOnClickListener(this);
        jobInfo.setOnClickListener(this);
    }

    private void getCompanyProposalDetail(long companyProposalId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<CompanyProposal> call=apiService.getCompanyProposalDetail(companyProposalId);
        call.enqueue(new Callback<CompanyProposal>() {
            @Override
            public void onResponse(Call<CompanyProposal> call, Response<CompanyProposal> response) {
                Log.e(TAG,"Response"+response.body());
                if(response.isSuccessful()){
                    staffMembers=response.body().getCompanyProposal().getCompanyProposalAssignees();
                    job= response.body().getCompanyProposal().getJob();
                    setResponseData(response.body());
                }
                Log.e("StaffMembers","StaffMembers"+staffMembers.size());
            }

            @Override
            public void onFailure(Call<CompanyProposal> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    private void setResponseData(CompanyProposal body) {
        ArrayList<String> members= new ArrayList<>();
        for(int i=0;i<body.getCompanyProposal().getCompanyProposalAssignees().size();i++) {
            members.add(body.getCompanyProposal().getCompanyProposalAssignees().get(i).getFirstName()+" "+body.getCompanyProposal().getCompanyProposalAssignees().get(i).getLastName());
        }
        offeredBid.setText(body.getCompanyProposal().getOfferedQuote()+" SEK");
        //final int serviceFee=(int)(body.getCompanyProposal().getOfferedQuote()*15/100);
        service_Fee.setText("5"+" SEK");
        budget.setText(String.valueOf(body.getCompanyProposal().getOfferedQuote()-5)+" SEK");
        proposalDescrption.setText(body.getCompanyProposal().getProposalDescription());
        startingTime.setText(DateTimeConverter.getDateTime(Long.valueOf(body.getCompanyProposal().getStartingTime())));
        completionTime.setText(body.getCompanyProposal().getCompletionHours()+" hours ");
        chooseStaffMembers.setText(android.text.TextUtils.join(" , ",members));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.choose_staff_member:
                Intent intent1= new Intent(ReviewProposal.this, ChooseStaffMembers.class);
                intent1.putExtra("status","ReviewProposal");
                intent1.putParcelableArrayListExtra("data",staffMembers);
                startActivity(intent1);
                break;
            case R.id.info:
                Intent intent= new Intent(ReviewProposal.this, SingleJobActivity.class);
                intent.putExtra("status","Review");
                intent.putExtra("jobId",job.getId());
                intent.putExtra("userId",job.getUserId());
                startActivity(intent);
                break;
        }
    }
}
