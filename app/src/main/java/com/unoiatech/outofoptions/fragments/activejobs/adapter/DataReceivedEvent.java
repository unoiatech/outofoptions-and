package com.unoiatech.outofoptions.fragments.activejobs.adapter;

/**
 * Created by unoiatech on 5/11/2016.
 */
public class DataReceivedEvent {
    private final String message;

    public DataReceivedEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
