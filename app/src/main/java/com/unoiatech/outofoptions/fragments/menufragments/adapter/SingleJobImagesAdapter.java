package com.unoiatech.outofoptions.fragments.menufragments.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.SingleJobActivity;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.util.S3Utils;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 4/5/2017.
 */
public class SingleJobImagesAdapter extends PagerAdapter
{
    LayoutInflater mLayoutInflater;
    ArrayList<Job.Images> images;
    Long jobId;

    public SingleJobImagesAdapter(SingleJobActivity activity, Long jobId, ArrayList<Job.Images> images) {
        this.images= images;
        this.jobId=jobId;
        mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.preview_viewpager_item, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        container.addView(itemView);
        String imageUrl= S3Utils.getDownloadJobImageURL(jobId,images.get(position).getName());
        Log.e("ImageUrl","ImageUrl"+S3Utils.BASE_URL+imageUrl);
        Glide.with(imageView.getContext())
                .load(imageUrl)
                // .fitCenter()
                .into(imageView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
