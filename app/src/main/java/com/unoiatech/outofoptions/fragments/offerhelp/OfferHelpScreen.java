package com.unoiatech.outofoptions.fragments.offerhelp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.AddProposal;
import com.unoiatech.outofoptions.model.CompanyProposal;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.ConnectionDetection;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by unoiatech on 10/6/2016.
 */
public class OfferHelpScreen extends Activity implements View.OnClickListener {
    TextView completionTime, startingTime, offeredBid, service_Fee, budget, chooseStaffMembers;
    private static String TAG = "OfferHelpScreen";
    boolean clickedTimeStatus;
    RelativeLayout parentView, staffLayout;
    View staffView;
    Job job;
    DatePicker datePicker;
    View dialogView;
    String userType,expiryDate;
    ArrayList<String> selectedItems;
    ArrayList<User> data;
    AVLoadingIndicatorView progressBar;
    int oldvalue=0;
    final String[] selectedValues = new String[1];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offer_help);
        parentView = (RelativeLayout) findViewById(R.id.parent_view);

        Button submitButton = (Button) findViewById(R.id.submit);
        offeredBid = (TextView) findViewById(R.id.bid);
        service_Fee = (TextView) findViewById(R.id.service_fee);
        budget = (TextView) findViewById(R.id.budget);
        startingTime = (TextView) findViewById(R.id.starting_time);
        completionTime = (TextView) findViewById(R.id.completion_time);
        final EditText proposalDescrption = (EditText) findViewById(R.id.message);
        final TextView actionBarTitle = (TextView) findViewById(R.id.title);
        TextView job_title = (TextView) findViewById(R.id.job_title);
        chooseStaffMembers = (TextView) findViewById(R.id.choose_staff_member);
        progressBar = (AVLoadingIndicatorView) findViewById(R.id.progress_bar);

        chooseStaffMembers.setHint(getString(R.string.choose_staff_member));
        clickedTimeStatus = false;

        staffLayout = (RelativeLayout) findViewById(R.id.staff_layout);
        staffView = (View) findViewById(R.id.staff_view);

        data = new ArrayList<User>();

        //Get Data From Intent
        Log.e(TAG, "Get_data" + getIntent().getStringExtra("user_type") + "  " + getIntent().getLongExtra("companyId", 0));

        userType = getIntent().getStringExtra("user_type");
        if (userType.equals("Individual")) {
            staffView.setVisibility(View.GONE);
            staffLayout.setVisibility(View.GONE);
        } else if (userType.equals("Staff")) {
            staffView.setVisibility(View.GONE);
            staffLayout.setVisibility(View.GONE);
        }
        job = (Job) getIntent().getExtras().getParcelable("job_info");
        job_title.setText(job.getTitle());
        offeredBid.setText(job.getBudget() + " SEK");
        final int serviceFee = (int) (job.getBudget() * 15 / 100);
        service_Fee.setText(String.valueOf(serviceFee) + " SEK");
        budget.setText(String.valueOf(job.getBudget() - serviceFee) + " SEK");
        actionBarTitle.setText(R.string.offer_help_title);

        //Click listeners
        if (staffLayout.getVisibility() == View.VISIBLE)
            staffLayout.setOnClickListener(this);
        startingTime.setOnClickListener(this);
        //completionTime.setOnClickListener(this);

        //Set On Touch Listener on Completion Time
        completionTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                try {
                    showTimeHours();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "click" + chooseStaffMembers.getText().toString());
                //dismiss keyboard
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                if (ConnectionDetection.isInternetAvailable(getApplicationContext())) {
                    if (startingTime.getText().toString().equals(null) || startingTime.getText().toString().equals("")) {
                        notifyUser(getString(R.string.job_starting_time));
                    } else if (completionTime.getText().toString().equals(null) || completionTime.getText().toString().equals("")) {
                        notifyUser(getString(R.string.job_completion_time));
                    } else if (proposalDescrption.getText().toString().equals(null) || proposalDescrption.getText().toString().equals("")) {
                        notifyUser(getString(R.string.proposal_description));
                    } else if (userType.equals("Staff")) {
                        if (chooseStaffMembers.getText().toString().equals(null) || chooseStaffMembers.getText().toString().equals("")) {
                            notifyUser(getString(R.string.select_member));
                        }
                    } else {
                        User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
                        Long userId = user.getId();
                        if (userType.equals("Individual")) {
                            AddProposal proposal = new AddProposal();
                            proposal.setCompletionHours("4");
                            //proposal.setGetCompletionMinutes("10");
                            proposal.setStartingTime(changeDateFormat(startingTime.getText().toString()));
                            //proposal.setCompletionTime(currentTime);
                            // proposal.setOfferedTimeline(changeDateFormat(completionTime.getText().toString()));
                            proposal.setOfferedQuote(job.getBudget());
                            proposal.setProposalDescription(proposalDescrption.getText().toString());
                            proposal.setPostingTime(DateTimeConverter.getTime());
                            proposal.setUserID(userId);
                            addProposalForJob(proposal);
                        } else {
                            CompanyProposal proposal = new CompanyProposal();
                            proposal.setCompanyId(getIntent().getLongExtra("companyId", 0));
                            proposal.setCompletionHours("4");
                            try {
                                if (selectedItems.size() > 0)
                                    if (selectedItems.contains(String.valueOf(userId))) {
                                        proposal.setIsSenderAssigned("1");
                                    } else {
                                        proposal.setIsSenderAssigned("0");
                                    }
                                if (selectedItems.size() > 0) {
                                    for (String items : selectedItems) {
                                        User assignees = new User();
                                        assignees.setAssignee(Long.valueOf(items));
                                        data.add(assignees);
                                        proposal.setCompanyProposalAssignees(data);
                                    }
                                }
                            } catch (NullPointerException ex) {
                                ex.printStackTrace();
                            }
                            if (selectedItems != null)
                                proposal.setNoOfAssignees(selectedItems.size());
                            proposal.setCompletionMinutes("10");
                            proposal.setStartingTime(changeDateFormat(startingTime.getText().toString()));
                            proposal.setOfferedTimeline(completionTime.getText().toString());
                            proposal.setOfferedQuote(job.getBudget());
                            proposal.setProposalDescription(proposalDescrption.getText().toString());
                            proposal.setPostingTime(DateTimeConverter.getTime());
                            proposal.setSentBy(userId);
                            addCompanyProposalForJob(proposal);
                        }
                    }
                } else {
                    notifyUser(getString(R.string.internet_connection_error));
                }
            }
        });

    }

    private void addCompanyProposalForJob(final CompanyProposal proposal) {
        Log.e("Add_company_Proposal", "Add_company_Proposal");
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.addCompanyProposal(job.getId(), proposal);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "Response" + response.body());
                if (response.isSuccessful()) {
                    showAlert();
                } else {
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG, "Error Body" + error.toString());
                    Snackbar.make(getWindow().getDecorView().getRootView(), error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "Failure" + t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void notifyUser(String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
                .setAction(getString(R.string.ok_btn), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    private void addProposalForJob(final AddProposal proposal) {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.addProposal(job.getId(), proposal);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "Response" + response.body());
                if (response.isSuccessful()) {
                    showAlert();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "Failure" + t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        selectedItems = (ArrayList<String>) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readMemberIds("member_ids");
        String items = (String) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readMemberIds("members_name");
        if (items != null) {
            chooseStaffMembers.setText(items);
        } else {
            chooseStaffMembers.setHint(getString(R.string.choose_staff_member));
        }
        Log.e("OnResume", "OnResume" + items);
    }

    public void showAlert() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("proposal_sent");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment dialogFragment = ProposalSentDialog.newInstance();
        dialogFragment.show(ft, "proposal_sent");
    }

    public String changeDateFormat(String s) {
        Log.e("ChangeDateFormat","ChangeDateDormat"+ "  "+s);
        DateFormat originalFormat = new SimpleDateFormat("dd-MMMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = null;
        try {
            date = originalFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.starting_time:
                clickedTimeStatus = false;
                InputMethodManager im = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(v.getWindowToken(), 0);
                showDateTimePicker();
                break;
           /* case R.id.completion_time:
                clickedTimeStatus = true;
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                //showDateTimePicker();
                try {
                    showTimeHours();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;*/
            case R.id.staff_layout:
                InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Intent intent = new Intent(this, ChooseStaffMembers.class);
                intent.putExtra("status", "OfferHelp");
                intent.putExtra("companyId", getIntent().getLongExtra("companyId", 0));
                startActivity(intent);
                break;
        }
    }

    private void showTimeHours() throws ParseException {
        Calendar cal= Calendar.getInstance();
        cal.setTimeInMillis(job.getTimeline());

        DateFormat startDateFormat = new SimpleDateFormat("dd-MMMM-yyyy hh:mm:ss a");
        SimpleDateFormat targetFormat= new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        Date strtdate = startDateFormat.parse(startingTime.getText().toString());

        //GEt Starting and Ending Date
        String start = targetFormat.format(strtdate);
        String stop = targetFormat.format(cal.getTime());
        Log.e("Show_Time", "Show_Time"+"  strt"+"  "+start+"  end"+"   "+stop);

        DateTime dt1, dt2;
        int days = 0, hours = 0, minutes=0;
        ArrayList<String> selectedTime = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(start);
            d2 = format.parse(stop);

            dt1 = new DateTime(d1);
            dt2 = new DateTime(d2);
            days = Days.daysBetween(dt1, dt2).getDays();
            hours= Hours.hoursBetween(dt1, dt2).getHours() % 24;
            minutes=Minutes.minutesBetween(dt1,dt2).getMinutes()%60;

            Log.e("Days ", "" + Days.daysBetween(dt1, dt2).getDays() + " days, ");
            Log.e(" Hours", " " + Hours.hoursBetween(dt1, dt2).getHours() % 24 + " hours, ");
            Log.e(" Minutes", " " + Minutes.minutesBetween(dt1, dt2).getMinutes() % 60 + " minutes, ");
            Log.e(" S", " " + Seconds.secondsBetween(dt1, dt2).getSeconds() % 60 + " seconds.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(days>0) {
            for(int i=0;i<=days;i++){
                if(i==0){
                    selectedTime.add(0,"-");
                }
                else{
                selectedTime.add("Select "+i + "  days");}
            }
        }
        else{
            if(hours>0) {
                for(int i=0;i<=hours;i++){
                    if(i==0) {
                        selectedTime.add(0,"-");
                    }
                    else{
                        selectedTime.add("Select "+i + "  hours");
                    }
                }
            }
            else {
                if(minutes>0){
                    for(int i=0;i<=minutes;i++){
                        if(i==0) {
                            selectedTime.add(0,"-");
                        }
                        else{
                            selectedTime.add("Select "+i + "  minutes");
                        }
                    }
                }
            }
        }
        showNumberPicker(selectedTime);
    }

    private void showNumberPicker(ArrayList<String> list) {
        View view= getLayoutInflater().inflate(R.layout.select_timeline,null);
        final Dialog timelineDialog = new Dialog(this, R.style.Theme_Dialog);
        timelineDialog.setContentView(view);
        timelineDialog.setCancelable(false);
        timelineDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        timelineDialog.getWindow().setGravity(Gravity.BOTTOM);
        timelineDialog.show();

        //Get Views from Timeline Dialog
        TextView cancel=(TextView) timelineDialog.findViewById(R.id.cancel);
        final NumberPicker timelineSelector=(NumberPicker)timelineDialog.findViewById(R.id.number_picker);
        TextView done=(TextView)timelineDialog.findViewById(R.id.done);

        // Customise Number Picker Divider
        changeDividerColor(timelineSelector,getResources().getColor(R.color.app_color));

        //Initializing a new string array with elements
        final String[] values= list.toArray(new String[list.size()]);

        timelineSelector.setMinValue(0); //from array first value
        Log.e("Data","Number_data"+"  "+timelineSelector.getValue()+" "+values.length);
        //Specify the maximum value/number of NumberPicker
        timelineSelector.setMaxValue(values.length-1); //to array last value

        timelineSelector.setValue(timelineSelector.getValue());
        //Specify the NumberPicker data source as array elements
        timelineSelector.setDisplayedValues(values);

        //Gets whether the selector wheel wraps when reaching the min/max value.
        timelineSelector.setWrapSelectorWheel(true);

        timelineSelector.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldval, int newVal) {
                selectedValues[0] =values[newVal];
                oldvalue=oldval;
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedValues[0].equals("-")) {
                    completionTime.setText("");
                    Snackbar.make(getWindow().getDecorView(),"Please choose valid time to complete Job",Snackbar.LENGTH_SHORT).show();
                }else {
                    completionTime.setText(selectedValues[0]);
                }
               // setStartingEndingTime();
                timelineDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timelineDialog.dismiss();
            }
        });
    }

    private void changeDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }


    public void showDateTimePicker() {
        dialogView = View.inflate(OfferHelpScreen.this, R.layout.date_time_picker, null);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(OfferHelpScreen.this);
        datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
        //final TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);
        //alertDialog.setTitle(getString(R.string.time_picker_title))
        alertDialog.setPositiveButton(getString(R.string.select_time_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                showTimePicker();
                        /*Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                                datePicker.getMonth(),
                                datePicker.getDayOfMonth(),
                                timePicker.getCurrentHour(),
                                timePicker.getCurrentMinute());
                        long time = calendar.getTimeInMillis();
                        compareDate(time);*/
            }
        });
        alertDialog.create();
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    private void showTimePicker() {
        final View dialogView = View.inflate(OfferHelpScreen.this, R.layout.time_picker, null);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(OfferHelpScreen.this);
        final TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);
        //alertDialog.setTitle(getString(R.string.time_picker_title))
        alertDialog.setPositiveButton(getString(R.string.ok_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());
                long time = calendar.getTimeInMillis();
                compareDate(time);
                //showDateTimePicker();
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });
        alertDialog.create();
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    public String compareDate(long selecteddate) {
        Calendar selectedTime = Calendar.getInstance();
        Calendar job_posted_time = Calendar.getInstance();
        Calendar job_expired_time = Calendar.getInstance();
        selectedTime.setTimeInMillis(selecteddate);
        job_posted_time.setTimeInMillis(job.getPostingTime());
        job_expired_time.setTimeInMillis(job.getTimeline());
        DateFormat targetFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        String selectedDate = targetFormat.format(selectedTime.getTime());
        String pstingDate = targetFormat.format(job_posted_time.getTime());
        expiryDate = targetFormat.format(job_expired_time.getTime());

        Log.e("Compare_Date", "Compare_Date" + "  " + job.getPostingTime() + "  " + job.getTimeline()+"  "+expiryDate);

        String format = "MM/dd/yyyy hh:mm:ss a";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date selected = sdf.parse(selectedDate);
            Date posted;
            if (clickedTimeStatus) {
                String format1 = "dd-MMMM-yyyy hh:mm:ss a";
                SimpleDateFormat sdf1 = new SimpleDateFormat(format1);
                Date post = sdf1.parse(startingTime.getText().toString());
                String postDate = targetFormat.format(post);
                posted = sdf.parse(postDate);
            } else {
                posted = sdf.parse(pstingDate);
            }
            Date expired = sdf.parse(expiryDate);
            if (selected.after(posted) && selected.before(expired)) {
                if (clickedTimeStatus) {
                    completionTime.setText(getDate(selecteddate, "dd-MMMM-yyyy hh:mm:ss a"));
                } else {
                    startingTime.setText(getDate(selecteddate, "dd-MMMM-yyyy hh:mm:ss a"));
                }
            } else {
                if (clickedTimeStatus) {
                    completionTime.setText(" ");
                } else {
                    startingTime.setText("");
                }
                //notifyUser(getString(R.string.choose_valid_date));
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OfferHelpScreen.this);
                alertDialogBuilder.setMessage(getString(R.string.choose_valid_date));
                alertDialogBuilder.setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showDateTimePicker();
                    }
                });
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialogBuilder.create();
                alertDialogBuilder.show();
            }
            Log.e("Date", "Date" + "  " + expiryDate + "  " + expired + "  selected Date" + "  " + selected);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String getDate(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}