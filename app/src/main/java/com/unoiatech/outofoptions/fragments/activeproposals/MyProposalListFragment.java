package com.unoiatech.outofoptions.fragments.activeproposals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.model.ActiveProposalResponse;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.JobProposalData;
import com.unoiatech.outofoptions.model.Proposal;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.S3Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by unoiatech on 10/10/2016.
 */
public class MyProposalListFragment extends Fragment {
    private static String TAG = "MyProposalListFragment";
    RecyclerView recyclerView;
    String type;
    ArrayList<ActiveProposalResponse> datalist;
    ArrayList<ActiveProposalResponse> proposalData;
    MyAdapter listAdapter;
    LinearLayout emptyView;
    TextView staticText;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_proposal_list_fragment, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_jobs_recycler_view);
        emptyView=(LinearLayout)view.findViewById(R.id.empty_view);
        staticText=(TextView)view.findViewById(R.id.static_text);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getParentFragment().getActivity());
        recyclerView.setLayoutManager(layoutManager);

        type = getArguments().getString("type");
        datalist = new ArrayList<>();
        ProposalFragment parentFragment =(ProposalFragment) getParentFragment();
        datalist = parentFragment.getProposalData();
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
             if (type.equals("pending")) {
                 Intent intent= new Intent(getActivity(),JobDetails.class);
                 Proposal proposal= proposalData.get(position).getProposal();
                 Job job=proposalData.get(position).getJob();

                 JobProposalData data=new JobProposalData();
                 data.setSellerName(proposalData.get(position).getUser().getFirstName() + " " + proposalData.get(position).getUser().getLastName());
                 data.setSellerId(proposalData.get(position).getUser().getId());
                 data.setProposalId(proposal.getId());
                 data.setProposalStartingTime(proposal.getStartingTime());
                 data.setProposalDescription(proposal.getProposalDescription());
                 data.setOfferedQuote(proposal.getOfferedQuote());
                 data.setProposalCompletionTime(proposal.getCompletionTime());
                 data.setProposalStatus(proposal.getProposalStatus());
                 data.setViewId("pending");
                 data.setJobTitle(job.getTitle());
                 data.setJobId(job.getId());
                 data.setLat(job.getLat());
                 data.setLng(job.getLng());
                 data.setCategory(job.getCategory());
                 data.setUserId(proposal.getUserID());
                 intent.putExtra("proposal_data",data);
                 startActivity(intent);
             }
             else if (type.equals("confirmed")) {
                 Intent intent= new Intent(getActivity(),JobDetails.class);
                 Proposal proposal= proposalData.get(position).getProposal();
                 Job job=proposalData.get(position).getJob();

                 JobProposalData data=new JobProposalData();
                 data.setSellerName(proposalData.get(position).getUser().getFirstName() + " " + proposalData.get(position).getUser().getLastName());
                 data.setSellerId(proposalData.get(position).getUser().getId());
                 data.setProposalId(proposal.getId());
                 data.setProposalStartingTime(proposal.getStartingTime());
                 data.setProposalDescription(proposal.getProposalDescription());
                 data.setOfferedQuote(proposal.getOfferedQuote());
                 data.setProposalCompletionTime(proposal.getCompletionTime());
                 data.setProposalStatus(proposal.getProposalStatus());
                 data.setViewId("accepted");
                 data.setJobTitle(job.getTitle());
                 data.setJobId(job.getId());
                 data.setLat(job.getLat());
                 data.setLng(job.getLng());
                 data.setCategory(job.getCategory());
                 data.setUserId(proposal.getUserID());
                 intent.putExtra("proposal_data",data);
                 startActivity(intent);
               }
               else if (type.equals("completed")) {
                    Intent intent = new Intent(getActivity(), JobDetails.class);
                    Proposal proposal= proposalData.get(position).getProposal();
                    Job job=proposalData.get(position).getJob();
                    //intent.putExtra("proposal_info",proposal);
                    //intent.putExtra("job_info",job);
                    intent.putExtra("name",proposalData.get(position).getUser().getFirstName()+" "+proposalData.get(position).getUser().getLastName());
                    intent.putExtra("view_id", "Completed");
                    startActivity(intent);
                }
            }
        }));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.e(TAG,"OnActivityCreated");
        super.onActivityCreated(savedInstanceState);
        setDataOnList();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(MyProposalListFragment.this);
    }
    @Subscribe
    public void onEvent(DataReceivedEvent event){
        setDataOnList();
    }

    private void setDataOnList() {
        proposalData= new ArrayList<>();
        for(int i=0;i<datalist.size();i++) {
            if (type.equals("pending")) {
                if (datalist.get(i).getProposal().getProposalStatus().equals(type) || datalist.get(i).getProposal().getProposalStatus().equals("accepted"))
                    proposalData.add(datalist.get(i));
            } else if (datalist.get(i).getProposal().getProposalStatus().equals(type)) {
                proposalData.add(datalist.get(i));
                Log.e(TAG, "OnEvent" + proposalData.size());
            }
        }
        if(type.equals("confirmed")&&proposalData.size()==0){
            emptyView.setVisibility(View.VISIBLE);
            staticText.setText(R.string.active_proposal_empty_text);
        }
        else if(type.equals("pending")&&proposalData.size()==0){
            emptyView.setVisibility(View.VISIBLE);
            staticText.setText(R.string.pending_proposal_empty_text);
        }
        else if(type.equals("completed")&&proposalData.size()==0) {
            emptyView.setVisibility(View.VISIBLE);
            staticText.setText(R.string.completed_proposal_empty_text);
        }
        else {
            emptyView.setVisibility(View.GONE);
            listAdapter = new MyAdapter(getActivity(),proposalData,type);
            recyclerView.setAdapter(listAdapter);
        }
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        ArrayList<ActiveProposalResponse> data;
        Activity activity;

        public MyAdapter(FragmentActivity activity, ArrayList<ActiveProposalResponse> proposalData, String type) {
            data=proposalData;
            this.activity=activity;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.applied_jobs_recycler_view_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (type.equals("confirmed")) {
                holder.profileLayout.setVisibility(View.VISIBLE);
                holder.dateToComplete.setVisibility(View.VISIBLE);
                holder.time.setVisibility(View.VISIBLE);
                holder.time.setText("Escrow submitted 0 SEK");
                holder.time_image.setBackgroundResource(R.drawable.sek);
                holder.title.setText(data.get(position).getJob().getTitle());
                holder.budget.setText(data.get(position).getJob().getBudget() + " SEK");
                holder.proposals.setText("You offered " + data.get(position).getProposal().getOfferedQuote()+ " SEK");
                try {
                    holder.dateToComplete.setText("Takes " + DateTimeConverter.getTimeToComplete(data.get(position).getProposal().getCompletionTime()));
                    holder.dueDate.setText(DateTimeConverter.getDateTime(data.get(position).getProposal().getStartingTime()));
                }
                catch (ParseException e) {
                    Log.e("ParseException", e.toString());
                }
                holder.status_image.setBackgroundResource(R.drawable.flag);
            }
            else if (type.equals("pending")) {
                holder.profileLayout.setVisibility(View.VISIBLE);
                holder.name_text.setVisibility(View.VISIBLE);
                holder.profileImage.setVisibility(View.VISIBLE);
                holder.dateToComplete.setVisibility(View.VISIBLE);
                holder.time.setVisibility(View.VISIBLE);
                holder.title.setText(data.get(position).getJob().getTitle());
                holder.budget.setText(data.get(position).getJob().getBudget() + " SEK");
                holder.proposals.setText("Quoted " + data.get(position).getProposal().getOfferedQuote() + " SEK");
                try {
                    holder.dateToComplete.setText(DateTimeConverter.getTimeToComplete(data.get(position).getProposal().getCompletionTime()));
                    holder.dueDate.setText("Starts on "+ DateTimeConverter.getDateTime(data.get(position).getProposal().getStartingTime()));
                    holder.time.setText("Expired on "+ DateTimeConverter.getDateTime(data.get(position).getJob().getTimeline()));
                } catch (ParseException e) {
                    Log.e("ParseException", e.toString());
                }
                String imageURL = S3Utils.getDownloadUserImageURL(data.get(position).getUser().getId(), data.get(position).getUser().getImagePath());
                Glide.with(holder.profileImage.getContext())
                        .load(imageURL)
                        .asBitmap()
                        .centerCrop()
                        .into(holder.profileImage);
                holder.name_text.setText(data.get(position).getUser().getFirstName()+" "+data.get(position).getUser().getLastName());
                holder.time_image.setBackgroundResource(R.drawable.about);
                holder.time.setTag(holder);
                holder.status_image.setBackgroundResource(R.drawable.flag);
            }
            else {
                holder.profileLayout.setVisibility(View.VISIBLE);
                holder.name_text.setVisibility(View.VISIBLE);
                holder.profileImage.setVisibility(View.VISIBLE);
                holder.title.setText(data.get(position).getJob().getTitle());
                holder.budget.setText(data.get(position).getJob().getBudget() + " SEK");
                holder.rating_bar.setVisibility(View.VISIBLE);
                holder.proposals.setText("Paid " + data.get(position).getProposal().getOfferedQuote() + " SEK");
                /*try {
                    holder.dueDate.setText("Completed on " + formatToYesterdayOrToday(data.get(position).getProposal().getCompletionTime()));
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }*/
                //holder.rating_bar.setRating(Float.parseFloat(jobs.getStars()));
                holder.time_image.setBackgroundResource(R.drawable.about);
                holder.status_image.setBackgroundResource(R.drawable.flag);
            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            public TextView title, budget, proposals, dateToComplete, dueDate, time, name_text;
            ImageView status_image, time_image;
            CircleImageView profileImage;
            SimpleRatingBar rating_bar;
            RelativeLayout profileLayout;

            public ViewHolder(View v) {
                super(v);
                title = (TextView) v.findViewById(R.id.title);
                budget = (TextView) v.findViewById(R.id.budget);
                proposals = (TextView) v.findViewById(R.id.proposals);
                dateToComplete = (TextView) v.findViewById(R.id.date_to_complete);
                dueDate = (TextView) v.findViewById(R.id.due_date);
                time = (TextView) v.findViewById(R.id.time);
                status_image = (ImageView) v.findViewById(R.id.status_image);
                time_image = (ImageView) v.findViewById(R.id.time_image);
                rating_bar = (SimpleRatingBar) v.findViewById(R.id.rating);
                profileLayout = (RelativeLayout) v.findViewById(R.id.profileLayout);
                name_text = (TextView) v.findViewById(R.id.name);
                profileImage = (CircleImageView) v.findViewById(R.id.profile_image);
            }
        }
    }
/*
    private Job getJobInfo(Object s){
        Gson gson = new Gson();
        Object obj = s;
        // Java object to JSON, and save into a file
        gson.toJson(obj);
        // Java object to JSON, and assign to a String
        String jsonInString = gson.toJson(s);
        Log.e("Json_data","Json_ddata"+ jsonInString);
        try {
            job = gson.fromJson(jsonInString, Job.class);
        }
        catch (JsonSyntaxException ex){
            ex.printStackTrace();
        }
        return  job;
    }*/

   /* private String compareDate(Long dateTimeline) {
        Log.e("CompareDate", "" + dateTimeline);
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTimeInMillis(dateTimeline);
       *//* DateFormat targetFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
        String date1 = targetFormat.format(cal1.getTime());
        String date2 = targetFormat.format(cal2.getTime());
        Log.e("date","date"+date1+" "+date2);*//*
        String format = "MM/dd/yyyy hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date dateObj1 = sdf.parse(String.valueOf(cal1.getTime()));
            Date dateObj2 = sdf.parse(String.valueOf(cal2.getTime()));
            long diff = dateObj1.getTime() - dateObj2.getTime();
            int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
            int diffhours = (int) (diff / (60 * 60 * 1000));
            int diffmin = (int) (diff / (60 * 1000) % 60);
            System.out.println("difference between days: " + diffDays);
            if (diffDays < 0) {
                return "Expired";
            } else if (diffDays == 0) {
                if (diffhours < 0) {
                    return "Expired";
                }
                if (diffmin < 0) {
                    return "Expired";
                } else {
                    return "Accepting proposals";
                }
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }*/
}

