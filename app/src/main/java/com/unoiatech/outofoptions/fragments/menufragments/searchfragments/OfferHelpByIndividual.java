package com.unoiatech.outofoptions.fragments.menufragments.searchfragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.AddProposal;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ConnectionDetection;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Unoiatech on 6/19/2017.
 */

public class OfferHelpByIndividual extends Fragment implements View.OnClickListener{
    TextView completionTime, startingTime,offeredBid,service_Fee,budget;
    private static String TAG="OfferHelpByIndividual";
    boolean clickedTimeStatus;
    RelativeLayout parentView;
    Job job;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.offer_help,container,false);
        parentView=(RelativeLayout)view.findViewById(R.id.parent_view);
        Button submitButton = (Button)view.findViewById(R.id.submit);
        offeredBid= (TextView)view.findViewById(R.id.bid);
        service_Fee=(TextView)view.findViewById(R.id.service_fee);
        budget=(TextView)view.findViewById(R.id.budget);
        startingTime = (TextView)view.findViewById(R.id.starting_time);
        completionTime = (TextView)view.findViewById(R.id.completion_time);
        final EditText proposalDescrption = (EditText)view.findViewById(R.id.description);
        TextView actionBarTitle=(TextView)view.findViewById(R.id.title);
        TextView description=(TextView)view.findViewById(R.id.job_description);

        //Get Data from Intent
        job=(Job)getArguments().getParcelable("job_info");
        description.setText(job.getDescription());
        offeredBid.setText(job.getBudget()+" SEK");
        double serviceFee=(job.getBudget()*.15);
        service_Fee.setText(String.valueOf(serviceFee)+" SEK");
        budget.setText(String.valueOf(job.getBudget()-serviceFee)+" SEK");
        actionBarTitle.setText(R.string.offer_help_title);

        //Clicklisteners
        startingTime.setOnClickListener(OfferHelpByIndividual.this);
        completionTime.setOnClickListener(OfferHelpByIndividual.this);
        clickedTimeStatus=false;

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConnectionDetection.isInternetAvailable(getActivity())){
                    if (startingTime.getText().toString().equals(null) || startingTime.getText().toString().equals("")) {
                        notifyUser("Enter Starting Time");
                    }
                    else if (completionTime.getText().toString().equals(null) || completionTime.getText().toString().equals("")) {
                        notifyUser("Enter Completion Time");
                    }
                    else if (proposalDescrption.getText().toString().equals(null) || proposalDescrption.getText().toString().equals("")) {
                        notifyUser("Enter Message");
                    }
                    else {
                        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
                        long userId=user.getId();
                        AddProposal proposal= new AddProposal();
                        proposal.setCompletionHours("2");
                        //proposal.setGetCompletionMinutes("10");
                        proposal.setStartingTime(changeDateFormat(startingTime.getText().toString()));
                        //proposal.setCompletionTime(currentTime);
                        // proposal.setOfferedTimeline(changeDateFormat(completionTime.getText().toString()));
                        proposal.setOfferedQuote(Integer.parseInt(offeredBid.getText().toString()));
                        proposal.setProposalDescription(proposalDescrption.getText().toString());
                        proposal.setPostingTime(DateTimeConverter.getTime());
                        proposal.setUserID(userId);
                        Log.e(TAG,"OfferHelpData"+"userId  "+userId+" "+proposal.getPostingTime()+"startingTime "+proposal.getStartingTime()+" "+proposal.getOfferedQuote()+" "+proposal.getProposalDescription()+" "+proposal.getUserID());
                        addProposalForJob(proposal);
                    }
                }
                else {
                    notifyUser(getString(R.string.internet_connection_error));
                }
            }
        });
        return view;
    }

    private void notifyUser(String message) {
        Snackbar.make(getView(),message, Snackbar.LENGTH_SHORT)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    private void addProposalForJob(AddProposal proposal) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= apiService.addProposal(job.getId(),proposal);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+response.message()+" "+response.body());
                if(response.isSuccessful()) {
                    showAlert();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG,"Failure"+t.toString());
            }
        });
    }
    public void showAlert() {
       /* FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("proposal_sent");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment dialogFragment = ProposalSentDialog.newInstance();
        dialogFragment.show(ft,"proposal_sent");*/
    }

    public String changeDateFormat(String s) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("dd-MMMM-yyyy hh:mm:ss a", Locale.ENGLISH);
        SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = null;
        try {
            date = originalFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.starting_time:
                clickedTimeStatus=false;
                InputMethodManager im= (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(v.getWindowToken(), 0);
                showDateTimePicker();
                break;
            case R.id.completion_time:
                clickedTimeStatus=true;
                InputMethodManager imm= (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                showDateTimePicker();
                break;}
    }

    public void showDateTimePicker() {
        final View dialogView = View.inflate(getActivity(), R.layout.date_time_picker, null);
        final AlertDialog.Builder  alertDialog = new AlertDialog.Builder(getActivity());
        final DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
        final TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);
        alertDialog.setTitle("Choose time")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                                datePicker.getMonth(),
                                datePicker.getDayOfMonth(),
                                timePicker.getCurrentHour(),
                                timePicker.getCurrentMinute());
                        long time = calendar.getTimeInMillis();
                        compareDate(time);
                    }
                });
        alertDialog.create();
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    public String compareDate(long selecteddate) {
        Calendar selectedTime = Calendar.getInstance();
        Calendar job_posted_time = Calendar.getInstance();
        Calendar job_expired_time= Calendar.getInstance();
        selectedTime.setTimeInMillis(selecteddate);
        job_posted_time.setTimeInMillis(job.getPostingTime());
        job_expired_time.setTimeInMillis(job.getCompletedTime());
        SimpleDateFormat targetFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
        String selectedDate = targetFormat.format(selectedTime.getTime());
        String pstingDate = targetFormat.format(job_posted_time.getTime());
        String expiryDate= targetFormat.format(job_expired_time.getTime());
        String format = "MM/dd/yyyy hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date selected = sdf.parse(selectedDate);
            Date posted;
            if(clickedTimeStatus)
            {
                String format1 = "dd-MMMM-yyyy hh:mm:ss a";
                SimpleDateFormat sdf1 = new SimpleDateFormat(format1);
                Date post = sdf1.parse(startingTime.getText().toString());
                String postDate= targetFormat.format(post);
                posted=sdf.parse(postDate);
            }
            else {
                posted = sdf.parse(pstingDate);
            }
            Date expired=sdf.parse(expiryDate);
            if(selected.after(posted)&&selected.before(expired)) {
                if(clickedTimeStatus) {
                    completionTime.setText(getDate(selecteddate, "dd-MMMM-yyyy hh:mm:ss a"));
                }
                else{
                    startingTime.setText(getDate(selecteddate, "dd-MMMM-yyyy hh:mm:ss a"));
                }
            }
            else{
                if(clickedTimeStatus){
                    completionTime.setText(" ");
                }
                else{
                    startingTime.setText("");
                }
                notifyUser("Choose valid Date");
            }
        }
        catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    public String getDate(long milliSeconds, String dateFormat)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
