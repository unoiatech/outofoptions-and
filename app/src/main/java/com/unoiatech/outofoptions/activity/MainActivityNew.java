package com.unoiatech.outofoptions.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.VerifyBankIdResponse;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.BankId.ExceptionHelper;
import com.unoiatech.outofoptions.util.ConnectionDetection;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 10/26/2017.
 */

public class MainActivityNew extends AppCompatActivity implements View.OnClickListener
{
    private AVLoadingIndicatorView progressBar;
    private boolean isBankIdVerification= false;
    private static final String TAG="MainActivity";
    private static String orderRef;
    private static String verifiedId;
    private TextView progressText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        final LinearLayout loginByBankId=(LinearLayout)findViewById(R.id.bank_id_login);
        progressBar=(AVLoadingIndicatorView)findViewById(R.id.progress_bar);
        progressText=(TextView)findViewById(R.id.progress_text);

        //check if user already login
        User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        if(user!=null) {
            Log.e("Check_Saved_data","Check_saved_data"+user.getId()+"  "+user.getLastName()+"  "+user.getFirstName());
            Intent intent = new Intent(MainActivityNew.this, HomeScreenActivity.class);
            finish();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        //SetOnClickListeners
        loginByBankId.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isBankIdVerification){
            collectUserStatus(orderRef);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bank_id_login:
                if(ConnectionDetection.isInternetAvailable(this)){
                    signInWithBankId();
                }else{
                    Toast.makeText(this,getString(R.string.internet_connection_error),Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void signInWithBankId() {
        progressBar.setVisibility(View.VISIBLE);
        progressText.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<VerifyBankIdResponse> call = apiService.bankIdSignIn();
        call.enqueue(new Callback<VerifyBankIdResponse>() {
            @Override
            public void onResponse(Call<VerifyBankIdResponse>call, Response<VerifyBankIdResponse> response) {
                progressBar.setVisibility(View.GONE);
                if(response.code()==200) {
                    //Hide Static Progress Text
                    progressText.setVisibility(View.GONE);

                    isBankIdVerification=true;
                    String autoStartToken = response.body().getAutoStartToken();
                    orderRef = response.body().getOrderRef();
                    verifiedId=response.body().getVerifiedId();
                    Log.e(TAG,"orderRef"+orderRef);
                    Log.e(TAG,"verifyId"+verifiedId);

                    //Open BankId App
                    Intent intent = new Intent();
                    intent.setPackage("com.bankid.bus");
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("bankid:///?autostarttoken=" + autoStartToken + "&redirect=null "));
                    startActivity(intent);
                }
                else {
                    progressText.setVisibility(View.GONE);
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG,"Error Body"+error.toString());
                    Snackbar.make(getWindow().getDecorView(), error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VerifyBankIdResponse>call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                progressText.setVisibility(View.GONE);
                Log.e(TAG, t.toString());
            }
        });
    }

    private void collectUserStatus(String orderRef) {
        Log.e("Collect_Status","Collect_Status"+"  " +orderRef);
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<User> call = apiService.collectStatus(orderRef);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User>call, Response<User> response) {
                progressBar.setVisibility(View.GONE);
                Log.e(TAG,"Response"+" "+response.body()+"  "+response.message()+response.code()+" "+response.isSuccessful()+"  ");
                isBankIdVerification=false;
                if(response.code()==200) {
                    //Save user Data in SharedPreference
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).writeUserPref(response.body());

                    //Method To register Device Token
                    registerDeviceToken(response.body().getId());
                   /* Intent intent= new Intent(MainActivityNew.this,HomeScreenActivity.class);
                    finish();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);*/
                }
                else {
                    Log.d(TAG,"Message"+response.message());
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG,"Error Body"+error.toString());
                    Log.d(TAG+"Code",""+response.code());
                    Snackbar.make(getWindow().getDecorView(),
                            new ExceptionHelper(getApplicationContext()).getErrorMessage(error.getMessage()), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User>call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Snackbar.make(getWindow().getDecorView(),
                        "Server Error", Snackbar.LENGTH_SHORT).show();
                Log.e(TAG, t.toString());
            }
        });
    }

    private void registerDeviceToken(Long userId) {
            final String fcmId = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).getFcmId();
            User user = new User();
            user.setDeviceId(fcmId);
            user.setDeviceType(getString(R.string.device_type));
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Void> call = apiInterface.registerDeviceId(userId, user);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Log.d(TAG, "Registered" + response+"  "+fcmId);
                    if (response.isSuccessful()) {
                        Intent intent = new Intent(MainActivityNew.this, HomeScreenActivity.class);
                        finish();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Log.d(TAG, "error" + "" + response.message());
                        ApiError error = ErrorUtils.parseError(response);
                        Snackbar.make(getWindow().getDecorView(), error.getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    // Log error here since request failed
                    Log.d(TAG, t.toString());
                }
            });
        }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
