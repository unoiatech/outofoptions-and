package com.unoiatech.outofoptions.activity;

/**
 * Created by unoiaAndroid on 3/15/2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.main.AccountFragment;
import com.unoiatech.outofoptions.fragments.main.AccountFragmentNew;
import com.unoiatech.outofoptions.fragments.main.OnRegisterViewListener;
import com.unoiatech.outofoptions.fragments.main.UserInfoFragment;
import com.unoiatech.outofoptions.fragments.main.VerifyIdsFragment;
import com.unoiatech.outofoptions.model.RepostData;
import com.unoiatech.outofoptions.util.common.view.NonSwipeableViewPager;
import com.unoiatech.outofoptions.util.common.view.SlidingTabLayout;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 4/8/2016.
 */
public class RegisterActivity extends FragmentActivity implements AccountFragmentNew.OnAccountSelectedListener,OnRegisterViewListener {
    String email;
    String password;
    Long userId;
    ImageView firstCheckedImage,secondCheckedImage,thirdCheckedImage;
    TextView firstView,secondView,thirdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewpager);
        firstCheckedImage=(ImageView)findViewById(R.id.first_tick);
        secondCheckedImage=(ImageView)findViewById(R.id.second_tick);
        thirdCheckedImage=(ImageView)findViewById(R.id.third_tick);
        firstView=(TextView)findViewById(R.id.firstview);
        secondView=(TextView)findViewById(R.id.second_view);
        thirdView=(TextView)findViewById(R.id.third_view);

        mViewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        //getWindow().setBackgroundDrawableResource(R.drawable.background_image) ;
        // END_INCLUDE (setup_viewpager)
        // BEGIN_INCLUDE (setup_slidingtablayout)
        // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
        // it's PagerAdapter set.

        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        //Sliding TabLayout Textview
        TextView text_view=(TextView)findViewById(R.id.custom_tabstrip_tv);
        mSlidingTabLayout.setCustomTabView(R.layout.custom_tabstrip_layout, R.id.custom_tabstrip_tv);

        /******Hide TabStrip*******/
        mSlidingTabLayout.setSelectedIndicatorColors(android.R.color.transparent);
        mSlidingTabLayout.setViewPager(mViewPager, "#ffffff");
    }

   @Override
    public void onAccountSelected(String email,String telNumber) {
        setEmail(email);
        setTelephoneNo(telNumber );
    }

    public void hideFirstView() {
        firstCheckedImage.setVisibility(View.VISIBLE);
        secondView.setVisibility(View.VISIBLE);
        firstView.setVisibility(View.GONE);
    }

    public void hideSecondView() {
        secondView.setVisibility(View.GONE);
        thirdView.setVisibility(View.VISIBLE);
        secondCheckedImage.setVisibility(View.VISIBLE);
    }

    public void hideThirdView() {
        thirdCheckedImage.setVisibility(View.VISIBLE);
        thirdView.setVisibility(View.GONE);
    }

    @Override
    public void accountTabSelected() {
        firstView.setVisibility(View.VISIBLE);

    }

    @Override
    public void userInfoSelected() {
        secondView.setVisibility(View.VISIBLE);
        firstCheckedImage.setVisibility(View.VISIBLE);
        firstView.setVisibility(View.GONE);

    }

    @Override
    public void verifiedIdSelected() {
       thirdView.setVisibility(View.VISIBLE);
       secondCheckedImage.setVisibility(View.VISIBLE);
       secondView.setVisibility(View.GONE);
    }

    @Override
    public void accountUnselected() {
        firstCheckedImage.setVisibility(View.GONE);
        firstView.setVisibility(View.VISIBLE);
        secondView.setVisibility(View.GONE);
    }

    @Override
    public void verifyIdsSelected() {
        thirdView.setVisibility(View.GONE);
        thirdCheckedImage.setVisibility(View.VISIBLE);
    }

    public class MyAdapter extends FragmentPagerAdapter {
        private static final int FRAGMENT_1 = 0;
        private static final int FRAGMENT_2 = 1;
        private static final int FRAGMENT_3 = 2;

        public MyAdapter (FragmentManager fm)
        {
            super(fm);
        }
        @Override
        public Fragment getItem(int i) {
            switch (i){
                case FRAGMENT_1 :return new AccountFragmentNew();
                case FRAGMENT_2 : return new UserInfoFragment();
                case FRAGMENT_3 : return new VerifyIdsFragment();
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case FRAGMENT_1 : return "Account";
                case FRAGMENT_2 : return "User Info";
                case FRAGMENT_3 : return "Verify Ids";
            }
            return super.getPageTitle(position);
        }
        @Override
        public int getCount() {
            return 3;
        }
    }

    public String getTelephoneNo() {
        return password;
    }

    public void setTelephoneNo(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}



