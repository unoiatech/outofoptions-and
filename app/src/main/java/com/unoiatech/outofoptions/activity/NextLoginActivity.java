package com.unoiatech.outofoptions.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.home.HomeScreenActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 4/1/2016.
 */
public class NextLoginActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_login);
    }

    public void LayoutTapped(View view) {
        if (view.getId() == R.id.offer_help_clicked) {
            Intent intent = new Intent(NextLoginActivity.this, HomeScreenActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        else if (view.getId() == R.id.take_help_clicked) {
            Intent intent = new Intent(NextLoginActivity.this, HomeScreenActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
