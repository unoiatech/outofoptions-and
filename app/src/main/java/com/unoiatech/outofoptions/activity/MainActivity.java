package com.unoiatech.outofoptions.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.MapView;
import com.unoiatech.outofoptions.Manifest;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.CollectStatusResponse;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.VerifyBankIdResponse;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.BankId.ExceptionHelper;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static org.jivesoftware.smack.packet.XMPPError.Condition.redirect;


public class MainActivity extends Activity implements View.OnClickListener {
    private AVLoadingIndicatorView progressBar;
    private boolean isBankIdVerification= false;
    private static final String TAG="MainActivity";
    private static String orderRef;
    private static String verifiedId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        //final Button signInButton=(Button) findViewById(R.id.sign_in_button);
        LinearLayout loginByBankId=(LinearLayout)findViewById(R.id.bank_id_login);
        progressBar=(AVLoadingIndicatorView)findViewById(R.id.progress_bar);

        //check if user already login
        User user =SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        if(user !=null) {
            Intent intent = new Intent(MainActivity.this, HomeScreenActivity.class);
            startActivity(intent);
        }

        //Set OnClick Listeners
        loginByBankId.setOnClickListener(this);

        /*signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
                //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        // Fixing Later Map loading Delay
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MapView mv = new MapView(getApplicationContext());
                    mv.onCreate(null);
                    mv.onPause();
                    mv.onDestroy();
                }catch (Exception ignored){
                }
            }
        }).start();*/
    }

    @Override
    protected void onResume() {


        super.onResume();
        Log.e(TAG,"On_Resume");
        collectStatus(orderRef);
    }

    private void signInWithBankId() {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<VerifyBankIdResponse> call = apiService.bankIdSignIn();
        call.enqueue(new Callback<VerifyBankIdResponse>() {
            @Override
            public void onResponse(Call<VerifyBankIdResponse>call, Response<VerifyBankIdResponse> response) {
                progressBar.setVisibility(View.GONE);
                if(response.code()==200) {
                    isBankIdVerification=true;
                    String autoStartToken = response.body().getAutoStartToken();
                    orderRef = response.body().getOrderRef();
                    verifiedId=response.body().getVerifiedId();
                    Log.d(TAG,"orderRef"+orderRef);
                    Log.d(TAG,"verifyId"+verifiedId);

                    openBankId(autoStartToken);
                }
                else {
                    Log.d(TAG,"Message"+response.message());
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG,"Error Body"+error.toString());
                    Log.d(TAG+"Code",""+response.code());
                    Snackbar.make(getWindow().getDecorView(), error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VerifyBankIdResponse>call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, t.toString());
            }
        });
    }

    private void openBankId(String autoStartToken) {
        Intent intent = new Intent();
        intent.setPackage("com.bankid.bus");
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("bankid:///?autostarttoken=" + autoStartToken + "&redirect=null "));
        intent.putExtra("exit_on_sent", true);
        startActivity(intent);
    }

    private void collectStatus(String orderRef) {
        Log.e("Collect_Status","Collect_Status"+"  " +orderRef);
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<User> call = apiService.collectStatus(orderRef);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User>call, Response<User> response) {
                progressBar.setVisibility(View.GONE);
                Log.e(TAG,"REsponse"+" "+response.body()+"  "+response.message()+response.code()+" "+response.isSuccessful()+"  ");
                isBankIdVerification=false;
                if(response.code()==200) {
                    String ocspResponse = response.body().getFirstName();
                    String progressStatus = response.body().getLastName();
                    Log.e("data","Data"+ocspResponse+"  "+progressStatus);
                    Intent intent= new Intent(MainActivity.this,HomeScreenActivity.class);
                    startActivity(intent);

                }
                else {
                    Log.d(TAG,"Message"+response.message());
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG,"Error Body"+error.toString());
                    Log.d(TAG+"Code",""+response.code());
                    Snackbar.make(getWindow().getDecorView(),
                            new ExceptionHelper(getApplicationContext()).getErrorMessage(error.getMessage()), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User>call, Throwable t) {
                // Log error here since request failed
                progressBar.setVisibility(View.GONE);
                Snackbar.make(getWindow().getDecorView(),
                        "Server Error", Snackbar.LENGTH_SHORT).show();
                Log.e(TAG, t.toString());
            }
        });
    }

    private void setVerifiedImage(String bankid) {
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG,"OnACtivityResult"+"  "+requestCode+"  "+data);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bank_id_login:
                signInWithBankId();
                break;
        }
    }
}





