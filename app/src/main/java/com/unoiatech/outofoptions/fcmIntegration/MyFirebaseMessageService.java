package com.unoiatech.outofoptions.fcmIntegration;

import android.app.ActivityManager;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.fragments.activeproposals.dialogs.JobConfirmation;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.util.Foreground;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.greenrobot.eventbus.EventBus;

public class MyFirebaseMessageService extends FirebaseMessagingService {
    public static String TAG="FirebaseMsgService";
    final Handler handler= new Handler();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        Log.e(TAG, "From: " + remoteMessage.getFrom()+" "+remoteMessage.getData()+ " "+remoteMessage.getTtl());
        Log.e(TAG, "NotificationResponse Message Body: " + remoteMessage.getNotification()+" NotificationResponse");
                if (Foreground.get().isForeground()) {
                    EventBus.getDefault().post(new DataReceivedEvent("Notification"));
                    if (remoteMessage.getData().get("activity").equals("accepted proposal")) {
                        String data[] = remoteMessage.getData().get("message").split(":");
                        Log.e("JobInfo", "JobInfo" + data[0] + "  " + data[1] + "  " + remoteMessage.getData().get("proposalId"));
                        Intent intent = new Intent(this, JobConfirmation.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle bundle = new Bundle();
                        bundle.putString("job_title", data[1]);
                        bundle.putString("proposal_id", remoteMessage.getData().get("proposalId"));
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                    else if (remoteMessage.getData().get("activity").equals("INVITE_USER")) {
                        Intent intent = new Intent(this, CompanyInviteResponse.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle bundle = new Bundle();
                        bundle.putLong("notification_id", Long.parseLong(remoteMessage.getData().get("notificationId")));
                        bundle.putLong("company_id", Long.parseLong(remoteMessage.getData().get("companyId")));
                        bundle.putString("company_name", remoteMessage.getData().get("companyName"));
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                } else {
                /*if(remoteMessage.getData().get("activity").equals("accepted proposal")) {
                    NotificationPref notification=new NotificationPref();
                    notification.setJobTitle(data[1]);
                    notification.setProposalId(remoteMessage.getData().get("proposalId"));
                    SharedPrefsHelper.getInstance().savePushPref(remoteMessage.getData().get("proposalId"),notification);
                }*/
                }
                if (SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).isNotificationEnable()) {
                    if(remoteMessage.getData().get("activity").equals("COMPLETED_EMAIL_VERIFICATION")){
                        EventBus.getDefault().post(new DataReceivedEvent("Email Verified"));
                        sendNotification(remoteMessage.getData().get("activity"),"","","");
                    }
                    else {
                        String data[] = remoteMessage.getData().get("message").split(":");
                        sendNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("activity"), remoteMessage.getData().get("proposalId"), data[1]);
                    }
                }
    }

    private void sendNotification(String messageBody, String activity, String proposalId, String title) {
        Log.e("Send_Notification"," "+messageBody);
        Intent intent = new Intent(this,HomeScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if(activity.equals("accepted proposal")){
            Bundle bundle= new Bundle();
            bundle.putString("notification_title",activity);
            bundle.putString("job_title",title);
            bundle.putString("proposal_id",proposalId);
            intent.putExtras(bundle);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("outofoptions")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
