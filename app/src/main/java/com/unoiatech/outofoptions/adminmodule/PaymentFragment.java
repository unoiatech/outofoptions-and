package com.unoiatech.outofoptions.adminmodule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.adminmodule.payment.PaymentRecordFragment;
import com.unoiatech.outofoptions.adminmodule.payment.PaymentWithdrawal;
import com.unoiatech.outofoptions.adminmodule.payment.RefundPaymentFragment;
import com.unoiatech.outofoptions.fragments.menufragments.SettingFragment;
import com.unoiatech.outofoptions.util.common.view.SlidingTabLayout;

/**
 * Created by Unoiatech on 7/4/2017.
 */
public class PaymentFragment extends Fragment
{
    ViewPager mViewPager;
    SlidingTabLayout mSlidingTabLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.seller_job_demo,container,false);
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyAdapter(getChildFragmentManager(),3));
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.custom_tabstrip_for_activejobs, R.id.custom_tabstrip_tv);
        mSlidingTabLayout.setDistributeEvenly(false);
        mSlidingTabLayout.setViewPager(mViewPager);
        return view;
    }
    public class MyAdapter extends FragmentStatePagerAdapter {
        int tabcount;

        public MyAdapter(FragmentManager fm, int childCount) {
            super(fm);
            this.tabcount=childCount;
        }

        @Override
        public Fragment getItem(int i){
            switch (i) {
                case 0:
                    PaymentWithdrawal f=new PaymentWithdrawal();
                    Bundle b = new Bundle();
                    b.putString("type", "confirmed");
                    f.setArguments(b);
                    return f;

                case 1:
                    RefundPaymentFragment f1= new RefundPaymentFragment();
                    Bundle b1= new Bundle();
                    b1.putString("type","pending");
                    f1.setArguments(b1);
                    return f1;

                case 2:
                    PaymentRecordFragment f2=new PaymentRecordFragment();
                    Bundle b2 = new Bundle();
                    b2.putString("type","completed");
                    f2.setArguments(b2);
                    return f2;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getActivity().getString(R.string.withdrawal_tab_text);
                case 1:
                    return getActivity().getString(R.string.refund_tab_text);
                case 2:
                    return getActivity().getString(R.string.record_tab_text);
            }
            return super.getPageTitle(position);
        }

        @Override
        public int getCount() {
            return tabcount;
        }
    }
}
