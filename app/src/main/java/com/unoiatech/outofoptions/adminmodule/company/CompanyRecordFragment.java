package com.unoiatech.outofoptions.adminmodule.company;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.URL;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 7/4/2017.
 */
public class CompanyRecordFragment extends Fragment
{
    RecyclerView mRecyclerView;
    ArrayList<Company> companyRecords;
    MyAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.my_proposal_list_fragment,container,false);
        mRecyclerView=(RecyclerView)view.findViewById(R.id.my_jobs_recycler_view);
        companyRecords= new ArrayList<>();

        setRecyclerView(companyRecords);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //Get Company Records
        getCompanyRecords();
    }

    private void getCompanyRecords() {
        companyRecords.clear();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<CompanyData>> call=apiService.getCompanyRecords();
        call.enqueue(new Callback<ArrayList<CompanyData>>() {
            @Override
            public void onResponse(Call<ArrayList<CompanyData>> call, Response<ArrayList<CompanyData>> response) {
                Log.d("Response","Response"+response.isSuccessful());
                if(response.isSuccessful()){
                    for(int i=0;i<response.body().size();i++){
                        if(!response.body().get(i).getCompany().getCompanyStatus().equals("3"))
                        companyRecords.add(response.body().get(i).getCompany());
                    }
                    setRecyclerView(companyRecords);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CompanyData>> call, Throwable t) {
                Log.e("Failure","Failure"+t.toString());
            }
        });
    }

    private void setRecyclerView(ArrayList<Company> companyRecords) {
        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        Log.e("SetRecyclerView","Size"+companyRecords.size());

        //initialize Adapter
        mAdapter= new MyAdapter(getActivity(), this.companyRecords);
        mRecyclerView.setAdapter(mAdapter);
    }

    private class  MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
        Activity activity;
        ArrayList<Company> data;
        public MyAdapter(FragmentActivity activity, ArrayList<Company> companyRecords) {
            this.activity=activity;
            data=companyRecords;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.company_record_layout, parent, false);
            MyAdapter.ViewHolder vh=new MyAdapter.ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
            holder.userName.setText(data.get(position).getUser().getFirstName()+" "+data.get(position).getUser().getLastName());
            holder.companyName.setText(data.get(position).getBusinessName());
            if(data.get(position).getLogo()!=null)
            Glide.with(activity).load(S3Utils.getCompanylogo(data.get(position).getId(), data.get(position).getLogo())).into(holder.logo);

            setUserImage(holder.userImage,data.get(position).getUser().getImagePath(),data.get(position).getUser().getId());

            //set Company Response Status Image And Text
            setResponseStatusImage(data.get(position).getCompanyStatus(),holder.companyStatusImage,holder.companystatus);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView companyName,userName,companystatus;
            ImageView logo,userImage,companyStatusImage;

            public ViewHolder(View itemView) {
                super(itemView);
                companyName=(TextView)itemView.findViewById(R.id.company_name);
                logo=(ImageView)itemView.findViewById(R.id.logo);

                userName=(TextView) itemView.findViewById(R.id.user_name);
                userImage=(ImageView)itemView.findViewById(R.id.user_image);

                companystatus=(TextView)itemView.findViewById(R.id.company_status);
                companyStatusImage=(ImageView)itemView.findViewById(R.id.status_image);
            }
        }
        private void setUserImage(ImageView memberImage, String imagePath, Long id) {
            if (imagePath != null) {
                if (imagePath.contains("\\")) {
                    String lastIndex = imagePath.replace("\\", " ");
                    String[] Stamp = lastIndex.split("\\s");
                    String timeStamp = Stamp[3];
                    String userImageUrl = URL.BASE_URL + "/users/download/" + id + "/image/" + timeStamp;
                    Glide.with(activity).load(userImageUrl).into(memberImage);
                } else {
                    Glide.with(activity).load(S3Utils.getDownloadUserImageURL(id, imagePath)).into(memberImage);
                }
            }
        }
    }

    private void setResponseStatusImage(String companyStatus, ImageView companyStatusImage, TextView companystatus) {
        if(companyStatus.equals("1")){
            companystatus.setTextColor(getResources().getColor(R.color.app_color));
            companystatus.setText(getString(R.string.completed_job));
            companyStatusImage.setImageResource(R.drawable.record_completed);
        }
        else if(companyStatus.equals("2")){
            companystatus.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            companystatus.setText(getString(R.string.archive_cancel_job));
            companyStatusImage.setImageResource(R.drawable.record_cancelled);
        }
    }
}
