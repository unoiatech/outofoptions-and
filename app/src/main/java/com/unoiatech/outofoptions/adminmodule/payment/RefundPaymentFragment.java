package com.unoiatech.outofoptions.adminmodule.payment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unoiatech.outofoptions.R;

/**
 * Created by Unoiatech on 8/31/2017.
 */

public class RefundPaymentFragment extends Fragment
{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.refund_screen_layout,container,false);
        return view;
    }
}
