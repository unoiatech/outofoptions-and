package com.unoiatech.outofoptions.adminmodule.payment;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.model.TransactionRequest;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 7/4/2017.
 */

public class PaymentWithdrawal extends Fragment {
    RecyclerView mRecyclerView;
    ArrayList<CompanyData> withdrawalRequest;
    private static String TAG="PaymentWithdrawal";
    private MyAdapter mAdapter;
    private LinearLayout emptyView;
    private TextView staticText;
    private AlertDialog alertDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.my_proposal_list_fragment,container,false);
        mRecyclerView=(RecyclerView)view.findViewById(R.id.my_jobs_recycler_view);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        emptyView=(LinearLayout)view.findViewById(R.id.empty_view);
        staticText=(TextView)view.findViewById(R.id.static_text);

        withdrawalRequest= new ArrayList<>();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //Api Call to ge Withdrawal Payment list
        getWithdrawalPaymentList();
    }

    private void getWithdrawalPaymentList(){
        withdrawalRequest.clear();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<CompanyData>> call=apiService.getWithdrawalPaymentList();
        call.enqueue(new Callback<ArrayList<CompanyData>>() {
            @Override
            public void onResponse(Call<ArrayList<CompanyData>> call, Response<ArrayList<CompanyData>> response) {
                Log.d(TAG,"Response"+"   "+response.body());
                for(int i=0;i<response.body().size();i++){
                    withdrawalRequest.add(response.body().get(i));
                }
                setDataOnRecyclerView(withdrawalRequest);
            }

            @Override
            public void onFailure(Call<ArrayList<CompanyData>> call, Throwable t) {
                Log.e(TAG,"failure"+t.toString());
            }
        });
    }

    private void setDataOnRecyclerView(ArrayList<CompanyData> withdrawalRequest) {
        if(withdrawalRequest.size()>0){
            emptyView.setVisibility(View.GONE);
            staticText.setVisibility(View.GONE);
            mAdapter= new MyAdapter(getActivity(),withdrawalRequest);
            mRecyclerView.setAdapter(mAdapter);
        }
        else{
            emptyView.setVisibility(View.VISIBLE);
            staticText.setVisibility(View.VISIBLE);
            staticText.setText(R.string.static_txt_on_company_req);
        }
    }

    private void showAlert(final Long transactionId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.cancel_reg_dialog_layout, null);
        dialogBuilder.setView(view);

        Button no = (Button) view.findViewById(R.id.no);
        final TextView yes=(TextView) view.findViewById(R.id.yes);
        EditText desEdit=(EditText)view.findViewById(R.id.des_edit);
        final AVLoadingIndicatorView progressBar=(AVLoadingIndicatorView)view.findViewById(R.id.progress_bar1);
        final TextView desc=(TextView)view.findViewById(R.id.des_edit);
        TextView mTitle=(TextView)view.findViewById(R.id.title);

        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        //Set Text
        desEdit.setHint(getString(R.string.cancel_withdrawal_hint));
        mTitle.setText(getString(R.string.cancelled_withdrawal_title));

        /****Negative response Button****/
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        /***Positive Response Button*****/
        yes.setText(R.string.yes_text);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!desc.getText().toString().isEmpty()) {
                    InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow((null ==getActivity().getCurrentFocus()) ? null :
                            getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    //To Cancel company Registration Request
                    withdrawRequestResponse(transactionId, "No", desc.getText().toString());
                }
                else{
                    Toast.makeText(getActivity(),getString(R.string.cancel_reg_message),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void withdrawRequestResponse(Long id, String response, String reason) {
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);
        TransactionRequest obj= new TransactionRequest();
        obj.setDisapprovalReason(reason);

        Call<ResponseBody> call= apiService.withdrawalRequestResponse(response,id,obj);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+"  "+response.body());
                if(response.isSuccessful()){
                   if(alertDialog!=null){
                       alertDialog.dismiss();
                   }
                    getWithdrawalPaymentList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
        Activity activity;
        ArrayList<CompanyData> withdrawalData;

        public MyAdapter(FragmentActivity activity, ArrayList<CompanyData> withdrawalRequest) {
            this.activity=activity;
            withdrawalData=withdrawalRequest;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view= getActivity().getLayoutInflater().inflate(R.layout.payment_withdrawal_layout,parent,false);
            MyAdapter.ViewHolder vh= new MyAdapter.ViewHolder(view);
            return vh;
        }

        @Override
        public void onBindViewHolder(final MyAdapter.ViewHolder holder, final int position) {
            holder.userName.setText(withdrawalData.get(position).getUser().getFirstName()+" "+withdrawalData.get(position).getUser().getLastName());
            holder.swishNumber.setText(withdrawalData.get(position).getTransactionRequest().getSwishNumber());
            holder.amountRequested.setText(withdrawalData.get(position).getTransactionRequest().getAmountRequested()+"  SEK");

            //Copy Text to Clip Board
            holder.swishNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copyText(holder.swishNumber);
                }
            });

            holder.amountRequested.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copyText(holder.amountRequested);
                }
            });

            //To Complete Withdrawal Request
            holder.completed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    withdrawRequestResponse(withdrawalData.get(position).getTransactionRequest().getId(),"yes",null);
                }
            });

            //Cancel Withdrawal Request
            holder.cancelled.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlert(withdrawalData.get(position).getTransactionRequest().getId());
                }
            });
        }

        @Override
        public int getItemCount() {
            return withdrawalData.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            private TextView swishNumber,amountRequested,userName;
            private Button completed,cancelled;
            public ViewHolder(View view) {
                super(view);
                swishNumber=(TextView)view.findViewById(R.id.swish_number);
                userName=(TextView)view.findViewById(R.id.name);
                amountRequested=(TextView)view.findViewById(R.id.budget);
                completed=(Button)view.findViewById(R.id.completed_btn);
                cancelled=(Button)view.findViewById(R.id.cancel_btn);
            }
        }

        //Method to Copy Swish number And Amount To ClipBoard
        private void copyText(TextView textView) {
            ClipboardManager clipboard = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("",textView.getText().toString());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(activity,"Text Copied",Toast.LENGTH_SHORT).show();
        }
    }
}
