package com.unoiatech.outofoptions.sharedui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.registershop.ShopRegistrationScreen;
import com.unoiatech.outofoptions.util.PlaceUtil;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class LocationFragment extends DialogFragment {
    private SupportMapFragment fragment;
    private GoogleMap googlemap;
    private static String TAG = "LocationFragment";
    private static View view;
    private Double latitude;
    private Double longitude;
    private String locationText;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static com.unoiatech.outofoptions.home.fragment.filter.LocationFragment locationFragment;
    private LatLng location;
    private TextView placesAutocomplete;
    private Marker marker;

    //create single instance of Location Fragment
    public static com.unoiatech.outofoptions.home.fragment.filter.LocationFragment getInstance() {

        if (locationFragment == null) {
            locationFragment = new com.unoiatech.outofoptions.home.fragment.filter.LocationFragment();
        }
        return locationFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // View view = inflater.inflate(R.layout.location_fragment_layout, container, false);
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.location_fragment_layout, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        //set location button
        Button setLocation = (Button) view.findViewById(R.id.set_location_button);

        // set previously selected location
        placesAutocomplete = (TextView) view.findViewById(R.id.places_autocomplete);

        // start google's autocomplete widget
        placesAutocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    Log.e(TAG, "GooglePlayServicesRepairableException" + e.toString());
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                    Log.e(TAG, "GooglePlayServicesNotAvailableException" + e.toString());
                }
            }
        });

        //save new location
        setLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationText = placesAutocomplete.getText().toString();
                dismissKeyboard();
                saveMapData(latitude, longitude, locationText);
                Log.e(TAG, "selected lat lng" + latitude + " " + longitude);

                // pop up to the previous fragment
                FragmentManager manager = getActivity().getSupportFragmentManager();
//                manager.beginTransaction()
//                        .setCustomAnimations(R.anim.slide_down, 0)
//                        .remove(LocationFragment.this)
//                        .commit();

                Intent intent = new Intent();
                intent.putExtra("location", "Ok");
                // getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);

               ((ShopRegistrationScreen)getActivity()).setLocation(locationText);
                manager.popBackStack();


            }
        });


       /* googlemap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){
            @Override
            public boolean onMyLocationButtonClick()
            {
                latitude=GPSPlotter.getLocation().getLatitude();
                longitude=GPSPlotter.getLocation().getLongitude();

                Log.e(TAG,"latitude: "+latitude+" longitude : "+longitude);
                setMap(googlemap,latitude,longitude);
                placesAutocomplete.setText(PlaceUtil.getCompleteAddressString(latitude,longitude,getContext()));


                //TODO: Any custom actions
                return false;
            }
        });*/

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, fragment).commit();
        }

        // Positiong my location button
       /* View locationButton = fragment.getView().findViewById(2);

        // and next place it, for exemple, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams)     locationButton.getLayoutParams();
       // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);*/


    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, " onResume called");

        //read location from pref
        if (latitude == null)
            latitude = readLatitudeFromPref();
        if (longitude == null)
            longitude = readLongitudeFromPref();
        if (locationText == null)
            locationText = readLocationTextFromPref();
        // set marker and location
        setMap(googlemap, latitude, longitude);
        placesAutocomplete.setText(locationText);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, " onStart called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, " onStop called");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, " onDestroy called");

    }

    private void setMap(GoogleMap map, final Double mlatitude, final Double mlongitude) {
        if (map == null) {
            fragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    Log.e(TAG, " (map == null called");
                    if (marker != null) {
                        marker.remove();
                    }
                    location = null;
                    location = new LatLng(mlatitude, mlongitude);
                    // try{

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    map.setMyLocationEnabled(true);
                    //}
                  /*  catch (SecurityException e)
                    {
                     *//*   Intent gpsOptionsIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);*//*
                    }*/
                   /* CameraUpdate center=
                            CameraUpdateFactory.newLatLng(location);
                    CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);

                    map.moveCamera(center);
                    map.animateCamera(zoom);*/


                    //map.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 9.5f));
                    marker= map.addMarker(new MarkerOptions()
                            // .title("Sydney")
                            // .snippet("The most populous city in Australia.")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker))
                            .position(location));

                    Log.i(TAG,"Location was not null");
                    googlemap=map;

                    googlemap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){
                        @Override
                        public boolean onMyLocationButtonClick()
                        {
                            //if(GPSPlotter.getLocation()!=null) {
                               /* latitude = GPSPlotter.getLocation().getLatitude();
                                longitude = GPSPlotter.getLocation().getLongitude();
*/
                            Log.e(TAG, "latitude: " + latitude + " longitude : " + longitude);
                            setMap(googlemap, latitude, longitude);
                            placesAutocomplete.setText(PlaceUtil.getCompleteAddressString(latitude, longitude, getContext()));
                            //   }

                            //TODO: Any custom actions
                            return true;
                        }
                    });
                }
            });

        }

        else {
            googlemap=map;
            Log.e(TAG," (map != null called");
            location=null;
            if (marker != null) {
                marker.remove();
            }
            location = new LatLng(mlatitude,mlongitude);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15));

            googlemap.setMyLocationEnabled(true);
           /* CameraUpdate center=
                    CameraUpdateFactory.newLatLng(location);
            CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);

            map.moveCamera(center);
            map.animateCamera(zoom);*/

            marker= googlemap.addMarker(new MarkerOptions()
                    // .title("Sydney")
                    // .snippet("The most populous city in Australia.")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker))
                    .position(location));
        }
    }



  /*  @Override
    public boolean onMyLocationButtonClick() {
        latitude=(Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("latitude");
        longitude=(Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("longitude");

        Log.e(TAG,"latitude: "+latitude+" longitude : "+longitude);
        locationText=PlaceUtil.getCompleteAddressString(latitude,longitude,getContext());
        setMap(googlemap,latitude,longitude);
      //  placesAutocomplete.setText(PlaceUtil.getCompleteAddressString(latitude,longitude,getContext()));

        return true;
    }*/

    private void saveMapData(Double latitude,Double longitude,String locationText)
    {
        //save latitude
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                writeSearchRequest("latitude",latitude);

        //save longitude
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                writeSearchRequest("longitude",longitude);

        //save location text
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                writeSearchRequest("location_text", locationText);
    }

    private Double readLatitudeFromPref(){
        return (Double)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("latitude");
    }


    private Double readLongitudeFromPref(){
        return (Double)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("longitude");
    }


    private String readLocationTextFromPref(){
        return  (String)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("location_text");
    }

    private void dismissKeyboard()
    {


    }

    @Override
    public void onPause() {
        super.onPause();
        googlemap=null;
        Log.e(TAG," onPause called");

    }


    // Get data from PlaceAutoCompleteActivity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                Log.i(TAG, "Place: " + place.getName());


                //save data to shared preference
               /*   saveMapData(place.getLatLng().latitude,
                       place.getLatLng().longitude,
                        placesAutocomplete.getText().toString());*/
                latitude=place.getLatLng().latitude;
                longitude= place.getLatLng().longitude;
                locationText=place.getName().toString();

                setMap(googlemap,latitude,longitude);
                // placesAutocomplete.setText(place.getName());

                dismissKeyboard();


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
