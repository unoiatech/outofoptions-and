package com.unoiatech.outofoptions.dealsmodule.createdeals;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.Deals;
import com.unoiatech.outofoptions.registershop.TabSelectorListener;

/**
 * Created by Unoiatech on 11/28/2017.
 */

public class DealDetailsFragment extends Fragment implements View.OnClickListener {
    private static final int KEYWORDS_REQUEST_CODE = 111;
    private static final int DES_REQUEST_CODE= 222;
    private EditText deal_title, original_price, deal_price, keywords, description;
    private FrameLayout nextButton;
    private TabSelectorListener mCallback;
    private RecyclerView packList;
    private static String TAG = "DealDetailsScreen";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (TabSelectorListener) getActivity();
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_deal_detail, container, false);

        //Initialise View
        initView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "OnResume");
    }

    private void initView(View view) {
        deal_title = (EditText) view.findViewById(R.id.deal_title);
        original_price = (EditText) view.findViewById(R.id.original_price);
        deal_price = (EditText) view.findViewById(R.id.deal_price);
        keywords = (EditText) view.findViewById(R.id.keywords);
        description = (EditText) view.findViewById(R.id.deal_des);
        nextButton = (FrameLayout) view.findViewById(R.id.next_button);

        //OnClick Event
        nextButton.setOnClickListener(this);
        keywords.setOnClickListener(this);
        description.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_button:
                checkDealDetailsData();
                break;
            case R.id.keywords:
                addKeyWords("keywords");
                break;
            case R.id.deal_des:
                addKeyWords("description");
                break;
        }
    }

    private void addKeyWords(String type) {
        Log.e("Add_Keywords","Add_Keywords"+"  "+type);
        DialogFragment newFragment;
        if(type.equals("keywords")) {
            newFragment = AddKeywords.newInstance();
            newFragment.setTargetFragment(this, KEYWORDS_REQUEST_CODE);
        }
        else{
            newFragment= AddDescription.newInstance();
            newFragment.setTargetFragment(this, DES_REQUEST_CODE);
        }
        // newFragment.show(getSupportFragmentManager(), "dialog");

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (newFragment != null) {
            transaction.remove(newFragment);
        }
        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        //newFragment.setTargetFragment(this, REQUEST_CODE);


        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        transaction.add(android.R.id.content, newFragment, "keywords")
                .addToBackStack(null).commit();
    }

    private void checkDealDetailsData() {
        //&&!TextUtils.isEmpty(keywords.getText())&&!TextUtils.isEmpty(description.getText())
        if (!TextUtils.isEmpty(deal_title.getText()) && !TextUtils.isEmpty(original_price.getText()) && !TextUtils.isEmpty(deal_price.getText())&&!TextUtils.isEmpty(keywords.getText())&&!TextUtils.isEmpty(description.getText())) {
            //Hide Keyboard
            InputMethodManager inputmanager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputmanager.hideSoftInputFromWindow(getView().getWindowToken(), 0);

            // Set Data
            Deals data= Deals.getInstance();
            data.setTitle(deal_title.getText().toString());
            data.setDealPrice(Integer.parseInt(deal_price.getText().toString()));
            data.setOriginalPrice(Integer.parseInt(original_price.getText().toString()));
            data.setKeyword(keywords.getText().toString());
            data.setDescription(description.getText().toString());

            FragmentTabHost mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
            mhost.setCurrentTab(1);
            mCallback.selectDetailsTab();
        } else {
            notifyUser(getString(R.string.validation_check));
        }
    }

    private void notifyUser(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case KEYWORDS_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Log.e(TAG,"AcivityResult"+" "+data.getExtras());
                    String str= data.getExtras().getString("data").replace(" ",",");
                    String deal_keywords = str.substring(0, str.length() - 1);
                    keywords.setText(deal_keywords);
                }
                break;
            case DES_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Log.e(TAG,"AcivityResult"+" "+data.getExtras());
                    String str= data.getExtras().getString("data").trim();
                    description.setText(str);
                }
                break;
        }
    }
}
