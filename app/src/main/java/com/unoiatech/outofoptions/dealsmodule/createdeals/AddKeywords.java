package com.unoiatech.outofoptions.dealsmodule.createdeals;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.unoiatech.outofoptions.R;


/**
 * Created by Unoiatech on 11/30/2017.
 */

public class AddKeywords extends DialogFragment implements View.OnClickListener {
    private EditText keywords_text;

    public static DialogFragment newInstance() {
        return new AddKeywords();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.add_deal_keyword,container,false);
        Button doneButton=(Button)view.findViewById(R.id.done_btn);
        keywords_text=(EditText)view.findViewById(R.id.tagsEditText);

        //Set Focus on Keywords_text
        keywords_text.requestFocus();
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(keywords_text, InputMethodManager.SHOW_IMPLICIT);

        doneButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.done_btn:
                //Hide Keyboard
                InputMethodManager inputmanager= (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputmanager.hideSoftInputFromWindow(getView().getWindowToken(),0);

                //Pass EditText data to OnActivityResult of DealDetails Fragment
                Intent intent= getActivity().getIntent();
                intent.putExtra("data",keywords_text.getText().toString());
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,intent);
                getActivity().getSupportFragmentManager().popBackStack();
                dismiss();
                break;
        }
    }
}
