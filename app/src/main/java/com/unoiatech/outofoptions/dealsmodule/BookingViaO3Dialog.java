package com.unoiatech.outofoptions.dealsmodule;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.common.logger.Log;

import java.util.Calendar;

/**
 * Created by Unoiatech on 12/12/2017.
 */

public class BookingViaO3Dialog extends DialogFragment implements View.OnClickListener{
    private MaterialCalendarView widget;
    private ImageView cancelImage;
    private RadioButton currentTime;
    private RadioGroup hourButtonLayout;

    public static DialogFragment newInstance() {
        return new BookingViaO3Dialog();
        /*if(instance==null)
            instance= new BookingViaO3Dialog();
        return instance;*/
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deal_booking_dialog, container, false);
        widget = (MaterialCalendarView) view.findViewById(R.id.calendar_view);
        cancelImage=(ImageView)view.findViewById(R.id.cancel);
        //currentTime=(RadioButton)view.findViewById(R.id.time0);
        hourButtonLayout=(RadioGroup)view.findViewById(R.id.shop_time);
        //currentTime.setText(DateTimeConverter.getDealBookingTime());
        cancelImage.setOnClickListener(this);

        initCalendarView();
        setTime();
        return view;
    }

    private void setTime() {
        for (int i = 0; i < 10; i++) {
            RadioButton button = new RadioButton(getActivity());
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
            LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(width,height);
            params.setMargins(0,0,15,0);
            button.setLayoutParams(params);

           /* button.setLayoutParams(new LinearLayout.LayoutParams(width, height));
            // Get the TextView current LayoutParams
           LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);

            // Set TextView layout margin 25 pixels to all side
            // Left Top Right Bottom Margin
            lp.setMargins(15,20,10,50);

            // Apply the updated layout parameters to TextView
            button.setLayoutParams(lp);*/
           /* // To set Margin for the child Views
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            params.setMargins(0,0,5,0);*/
            button.setId(i);
            button.setText(DateTimeConverter.getDealBookingTime());
            button.setChecked(i == 1);
            button.setGravity(Gravity.CENTER);
            button.setButtonDrawable(R.drawable.null_selector);
            // Only select button with same index as currently selected number of hours
            button.setBackgroundResource(R.drawable.booking_selector); // This is a custom button drawable, defined in XML
            hourButtonLayout.addView(button);
        }
    }

    private void initCalendarView() {
        CalendarDay today = CalendarDay.from(Calendar.getInstance().getTime());

        //TO set MaximumDate on MaterialCalendarView
        Calendar maxCalen = Calendar.getInstance();
        maxCalen.set(Calendar.YEAR, today.getYear());
        maxCalen.set(Calendar.MONTH, today.getMonth() + 1);

        widget.state().edit()
                .setMinimumDate(Calendar.getInstance().getTime())
                .setMaximumDate(maxCalen)
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();
        widget.setTopbarVisible(false);
        widget.setCurrentDate(today);
        widget.setSelectedDate(today);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancel:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
