package com.unoiatech.outofoptions.dealsmodule.createdeals;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.Deals;
import com.unoiatech.outofoptions.registershop.TabSelectorListener;

/**
 * Created by Unoiatech on 11/28/2017.
 */
public class DealPlanFragment extends Fragment implements View.OnClickListener{
    private TabSelectorListener mCallback;
    private LinearLayout firstPack,secondPack,thirdPack,fourthPack;
    private FrameLayout nextButton,backButton;
    private static String selectedPlan;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mCallback=(TabSelectorListener)getActivity();
        }catch (ClassCastException ex){
            ex.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_deal_plan,container,false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        mCallback.unselectContactTab();
        selectedPlan= null;
        firstPack=(LinearLayout)view.findViewById(R.id.one_day_pack);
        secondPack=(LinearLayout)view.findViewById(R.id.seven_day_pack);
        thirdPack=(LinearLayout)view.findViewById(R.id.fourteen_day_pack);
        fourthPack=(LinearLayout)view.findViewById(R.id.thirty_day_pack);

        nextButton=(FrameLayout) view.findViewById(R.id.next_button);
        backButton=(FrameLayout)view.findViewById(R.id.back_button);

        //setOnClickListeners
        firstPack.setOnClickListener(this);
        secondPack.setOnClickListener(this);
        thirdPack.setOnClickListener(this);
        fourthPack.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.one_day_pack:
                 selectPlan(firstPack);
                break;
            case R.id.seven_day_pack:
                selectPlan(secondPack);
                break;
            case R.id.fourteen_day_pack:
                selectPlan(thirdPack);
                break;
            case R.id.thirty_day_pack:
                selectPlan(fourthPack);
                break;
            case R.id.next_button:
                checkSelectedPlan();
                break;
            case R.id.back_button:
                FragmentTabHost mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
                mhost.setCurrentTab(0);
                mCallback.unselectContactTab();
                break;
        }
    }

    private void checkSelectedPlan() {
        Log.e("selectedPLan","SelectedPlan"+" "+selectedPlan);
        if(selectedPlan!=null){
            //Redirect to Next booking Fragment
            Deals data= Deals.getInstance();
            data.setPlan(selectedPlan);

            FragmentTabHost mhost = (FragmentTabHost)getActivity().findViewById(R.id.fragment_tabhost);
            mhost.setCurrentTab(2);
            mCallback.selectContactTab();
        }else{
            Snackbar.make(getView(),getString(R.string.choose_plan),Snackbar.LENGTH_LONG).show();
        }
    }

    private void selectPlan(LinearLayout layout) {
        if(layout.equals(firstPack)){
            selectedPlan="PLAN_39";
            firstPack.setBackgroundResource(R.drawable.selected_plan);
            secondPack.setBackgroundResource(R.drawable.rounded_rectangle);
            thirdPack.setBackgroundResource(R.drawable.rounded_rectangle);
            fourthPack.setBackgroundResource(R.drawable.rounded_rectangle);
        }
        else if(layout.equals(secondPack)) {
            selectedPlan="PLAN_199";
            secondPack.setBackgroundResource(R.drawable.selected_plan);
            firstPack.setBackgroundResource(R.drawable.rounded_rectangle);
            thirdPack.setBackgroundResource(R.drawable.rounded_rectangle);
            fourthPack.setBackgroundResource(R.drawable.rounded_rectangle);
        }
        else if(layout.equals(thirdPack)) {
            selectedPlan="PLAN_299";
            thirdPack.setBackgroundResource(R.drawable.selected_plan);
            firstPack.setBackgroundResource(R.drawable.rounded_rectangle);
            secondPack.setBackgroundResource(R.drawable.rounded_rectangle);
            fourthPack.setBackgroundResource(R.drawable.rounded_rectangle);
        }
        else if(layout.equals(fourthPack)) {
            selectedPlan="PLAN_499";
            fourthPack.setBackgroundResource(R.drawable.selected_plan);
            firstPack.setBackgroundResource(R.drawable.rounded_rectangle);
            secondPack.setBackgroundResource(R.drawable.rounded_rectangle);
            thirdPack.setBackgroundResource(R.drawable.rounded_rectangle);
        }
    }
}
