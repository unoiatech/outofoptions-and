package com.unoiatech.outofoptions.dealsmodule.createdeals;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Deals;
import com.unoiatech.outofoptions.registershop.TabSelectorListener;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 11/28/2017.
 */
public class DealBookingFragment extends Fragment implements View.OnClickListener{
    private TabSelectorListener mCallBack;
    private RadioButton booking_by_o3,booking_by_call;
    private Button submit_button;
    private ImageView static_image;
    private FrameLayout back_button;
    private TextView static_text;
    private String bookingType;
    private static String TAG="DealBookingFragment";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallBack=(TabSelectorListener)getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_deal_booking,container,false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        mCallBack.unselectImagesTab();
        bookingType="O3";
        static_text=(TextView)view.findViewById(R.id.static_text);
        static_image=(ImageView)view.findViewById(R.id.static_image);
        booking_by_o3=(RadioButton) view.findViewById(R.id.booking_btn);
        booking_by_call=(RadioButton) view.findViewById(R.id.call_btn);
        submit_button=(Button)view.findViewById(R.id.submit_btn);
        back_button=(FrameLayout)view.findViewById(R.id.back_btn);

        //OnClickListeners
        booking_by_o3.setOnClickListener(this);
        booking_by_call.setOnClickListener(this);
        submit_button.setOnClickListener(this);
        back_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.call_btn:
                bookingType="call";
                static_text.setText(getString(R.string.booking_by_call_text));
                break;
            case R.id.booking_btn:
                bookingType="O3";
                static_text.setText(getString(R.string.o3_booking_text));
                break;
            case R.id.submit_btn:
                createDeal();
                break;
            case R.id.back_btn:
                FragmentTabHost mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
                mhost.setCurrentTab(1);
                mCallBack.unselectImagesTab();
                break;
        }
    }

    private void createDeal() {
        Deals object= Deals.getInstance();
        object.setBookingType(bookingType);
        //static data just for testing the add Deal url
        object.setShopId((long) 52);
        object.setShopName("My Shop");
        Log.e(" data","DEAl_DATa"+"  "+object.getBookingType()+"  "+object.getDescription()+"  "+object.getKeyword()+"  "
        +object.getTitle()+"  "+object.getDealPrice()+" "+object.getOriginalPrice()+" "+object.getPlan());

        ApiInterface services= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= services.createDeal(object);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Deal Response"+"  "+response.isSuccessful()+"  "+response.body());
                if(response.isSuccessful()){
                    getActivity().finish();
                }
                else{
                    ApiError error= ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"OnFailure"+"  "+t.toString());
            }
        });
    }

   /* private void bookingOptions(Button btn) {
        if(btn.equals(booking_by_call)){
            static_image.setImageResource(R.drawable.booking_by_call);
            bookingType="call";
            static_text.setText(getString(R.string.booking_by_call_text));
            booking_by_o3.setBackgroundResource(R.drawable.booking_button_deselect);
            booking_by_o3.setTextColor(Color.parseColor("#aaaaaa"));
            booking_by_call.setBackgroundResource(R.drawable.booking_button_selected);
            booking_by_call.setTextColor(Color.WHITE);
        }
        else if(btn.equals(booking_by_o3)){
            static_image.setImageResource(R.drawable.mobile);
            bookingType="O3";
            static_text.setText(getString(R.string.o3_booking_text));
            booking_by_call.setBackgroundResource(R.drawable.booking_button_deselect);
            booking_by_call.setTextColor(Color.parseColor("#aaaaaa"));
            booking_by_o3.setBackgroundResource(R.drawable.booking_button_selected);
            booking_by_o3.setTextColor(Color.WHITE);
        }
    }*/
}
