package com.unoiatech.outofoptions.home.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.dealsmodule.DealDetailsScreen;
import com.unoiatech.outofoptions.model.Deals;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 12/6/2017.
 */

public class DealListFragment extends Fragment implements OnMapReadyCallback {
    private ListView listView;
    private MapView mapView;
    private LatLng origin;
    private GoogleMap map;
    private int lastTopValue = 0;
    View listHeader;
    private static String TAG="DealsListFragment";
    private ArrayList<Deals> dealsList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deal_list_layout, container, false);

        initView(view);

        //Initialise Map
        mapView=(MapView)listHeader.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(DealListFragment.this);

        //Get Deals List
        getDealsList();
       /* if(ConnectionDetection.isInternetAvailable(getActivity())){
            getDealsList();
        }else{
            notifyUser(getString(R.string.internet_connection_error));
        }*/
        return view;
    }

    private void initView(View view){
        //Using Listview coz We can add header easily than recyclerview
        listView = (ListView)view.findViewById(R.id.listView);
        final LayoutInflater inflater1 = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listHeader = inflater1.inflate(R.layout.deals_header_on_home_screen, null);

        //Add Header on listview
        listView.addHeaderView(listHeader);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Long dealId= (Long) view.getTag(R.id.deal_title);
                Intent intent= new Intent(getActivity(), DealDetailsScreen.class);
                intent.putExtra("deal_id",dealId);
                getActivity().startActivity(intent);
            }
        });

        /* Handle list View scroll events */
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //Check if the first item is already reached to top.
                 Rect rect = new Rect();
                    if (mapView != null)
                        mapView.getLocalVisibleRect(rect);
                    if (lastTopValue != rect.top) {
                        lastTopValue = rect.top;
                        if (mapView != null)
                            mapView.setY((float) (rect.top / 2.0));
                    }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);;

        //map.getUiSettings().setZoomControlsEnabled(false);
        origin = new LatLng(30.6514,76.7355);

        //to disable map zooming from world map.
        map.moveCamera(CameraUpdateFactory.newLatLng(origin));
        map.animateCamera(CameraUpdateFactory.zoomTo(10));
    }
    private void getDealsList() {
        Log.e("Get_Deal_List","get_deal_list");
        ApiInterface service= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<Deals>> call=service.getDealsList();
        call.enqueue(new Callback<ArrayList<Deals>>() {
            @Override
            public void onResponse(Call<ArrayList<Deals>> call, Response<ArrayList<Deals>> response) {
                Log.d(TAG,"Deals_Response"+"  "+response.isSuccessful());
                dealsList= new ArrayList<Deals>();
                if(response.isSuccessful()){
                    dealsList=response.body();
                    setAdapter(dealsList);
                }else{
                    ApiError error= ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Deals>> call, Throwable t) {
                Log.e(TAG,"Failures"+"  "+t.toString());
            }
        });
    }

    private void setAdapter(ArrayList<Deals> dealsList) {
        //Set Adapter
        DealsAdapter adapter = new DealsAdapter(getActivity(),android.R.layout.simple_list_item_1,dealsList);
        listView.setAdapter(adapter);
    }

    private void notifyUser(String message) {
        Snackbar.make(getView(),message,Snackbar.LENGTH_LONG).show();
    }

    private class DealsAdapter extends ArrayAdapter<Deals> {
        ArrayList<Deals> data;
        LayoutInflater inflater;
        ViewHolder view_holder;

        public DealsAdapter(FragmentActivity activity, int simple_list_item_1, ArrayList<Deals> modelList) {
            super(activity,simple_list_item_1);
            data= modelList;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            return data.size() ;
        }

        @Nullable
        @Override
        public Deals getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            Deals deal=data.get(position);
            View view1 = convertView;
            if (convertView == null) {
                view1 = inflater.inflate(R.layout.deal_list_row_item, null);
                view_holder = new ViewHolder();
                view_holder.deal_title = (TextView) view1.findViewById(R.id.deal_title);
                view_holder.deal_price=(TextView)view1.findViewById(R.id.deal_price);
                view_holder.original_price=(TextView)view1.findViewById(R.id.original_price);
                view_holder.keywords=(TextView)view1.findViewById(R.id.keywords);
                view_holder.expiryTime=(TextView)view1.findViewById(R.id.expiry_time);
                view_holder.view=(View)view1.findViewById(R.id.view);
                view1.setTag(view_holder);
            }

            view_holder = (ViewHolder) view1.getTag();
            view_holder.deal_title.setText(deal.getTitle());
            view_holder.deal_price.setText("SEK "+deal.getDealPrice());
            view_holder.original_price.setText("SEK "+deal.getOriginalPrice());

            //To get Original Price Textview width and set custom width to the view accordingly
            view_holder.original_price.measure(0,0);       //must call measure!
            view_holder.view.getLayoutParams().width = view_holder.original_price.getMeasuredWidth();
            view_holder.view.requestLayout();

            view_holder.keywords.setText(deal.getKeyword());
            view_holder.expiryTime.setText(DateTimeConverter.getDealExpiryTime(deal.getExpiryDate()));
           try{
                view1.setTag(R.id.deal_title,deal.getId());
            }catch (NullPointerException ex){
                ex.printStackTrace();
            }
            return view1;
        }
        class ViewHolder {
            public TextView deal_title,deal_price,original_price,keywords,expiryTime;
            View view;
        }
    }
}
