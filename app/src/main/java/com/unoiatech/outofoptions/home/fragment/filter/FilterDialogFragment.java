package com.unoiatech.outofoptions.home.fragment.filter;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.MapScreen;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.ListFragment;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.SearchListFragment;
import com.unoiatech.outofoptions.home.JobDealScreen;
import com.unoiatech.outofoptions.home.fragment.JobsListFragment;
import com.unoiatech.outofoptions.model.SearchListResponse;
import com.unoiatech.outofoptions.util.Constants;
import com.unoiatech.outofoptions.util.RangeSeekBarNew;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.SingleThumbSeekBar;

/**
 * Created by unoiaAndroid on 3/23/2017.
 */

public class FilterDialogFragment extends DialogFragment {
    private TextView selectedLocation;
    private static String TAG="FilterDialogFragment";
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private TextView categoryTextView;
    private TextView distanceTextView,budgetTextView;
    private SingleThumbSeekBar distanceSeekBar;
    private RangeSeekBarNew budgetSeekbar;

    public static FilterDialogFragment newInstance() {
        return new FilterDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.filter_dialog_fragment_layout,container,false);
        Constants.returnFromFilterFragment=true;

        // click on current location
        LinearLayout currentLocationTextView = (LinearLayout) view.findViewById(R.id.current_location_textview);

       // apply button at the tool bar
        Button applyButton = (Button) view.findViewById(R.id.apply);
        // click listener for category layout
        RelativeLayout categoryLayout = (RelativeLayout) view.findViewById(R.id.category_layout);
        //show distance
        distanceSeekBar =(SingleThumbSeekBar) view.findViewById(R.id.distance_seekBar);
        // show distance text
        distanceTextView =(TextView) view.findViewById(R.id.distance) ;
        //show budget
        budgetSeekbar =(RangeSeekBarNew)view.findViewById(R.id.budget_seek_bar);
        // show budget text
        budgetTextView =(TextView) view.findViewById(R.id.budget_selected) ;
        // user's selected location with address
        selectedLocation=(TextView) view.findViewById(R.id.selected_location);
        // show selected categories
        categoryTextView =(TextView)view.findViewById(R.id.category_text_view);

        //Static View With customise Height
        View staticView =(View)view.findViewById(R.id.top_view);

        if(getArguments().getString("view").equals("SearchJobs")){
            staticView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,45));}
        else {
            staticView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,200));}
        //staticView.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, theSizeIWant));

        // set category text
        categoryTextView.setText((String)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("category_text"));
        Log.e("category","Category"+(String)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("category_text"));

        // radio group for handling radio buttons
        radioGroup= (RadioGroup) view.findViewById(R.id.rbg1);
        // all radio button
        RadioButton all =(RadioButton) view.findViewById(R.id.radio_all);
        //4 and above radio button
        RadioButton fourAndAbove =(RadioButton) view.findViewById(R.id.radio_4_and_above);

        // get current rating from preferences
        Double rating=(Double)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("rating");

        // mark the radio buttton checked according to the rating value
        if(rating==0.0)
        all.setChecked(true);
        else
        fourAndAbove.setChecked(true);

        // click listener for location
      currentLocationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               //open Location Fragemnt
               LocationFragment locationFragment=LocationFragment.getInstance();
               FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                // For a little polish, specify a transition animation
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                // To make it fullscreen, use the 'content' root view as the container
                // for the fragment, which is always the root view for the activity

                //check to avoid adding fragments multiple times
                if(!locationFragment.isAdded())
                transaction.add(android.R.id.content, locationFragment)
                        .addToBackStack(null).commit();
            }
        });


        //apply button for location
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remove this fragment from the backstack
              //  getActivity().getSupportFragmentManager().popBackStack();

                /***       save the selected rating  into prefs    ********/
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) getView().findViewById(selectedId);

                if(radioButton.getText().toString().equalsIgnoreCase("all"))
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                            writeSearchRequest("rating",0.0);
                else
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                            writeSearchRequest("rating",4.0);

                /***   save max budget ,min budget , distance into the prefs   ************/
                String string = budgetTextView.getText().toString();
                String[] parts = string.split("-");
                String part1 = parts[0];
                String part2 = parts[1];

                saveMinBudgetToPref(Integer.parseInt(part1));
                saveMaxBudgetToPref(Integer.parseInt(part2));
                saveDistanceToPref(Double.parseDouble(distanceTextView.getText().toString()));

                //Dismiss DialogFragment
                getActivity().getSupportFragmentManager().popBackStack();

                if(getArguments().getString("view").equals("SearchJobs")){
                   if(getArguments().getString("selected_tab").equals("Map")){
                       MapScreen frag=(MapScreen)getActivity().getSupportFragmentManager().findFragmentByTag("Tab1");
                       frag.onResume();
                   }else{
                       SearchListFragment frag=(SearchListFragment)getActivity().getSupportFragmentManager().findFragmentByTag("Tab2");
                       frag.onResume();
                   }
                }
                else{
                    JobDealScreen jobsListFragment=(JobDealScreen) getActivity().getSupportFragmentManager().findFragmentByTag("jobs_list");
                    jobsListFragment.onResume();
                }
        }
        });

        // listener for catgeory layout
        categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            // open the catgeory layout
            DialogFragment categoryFragment = CategoryFragment.getInstance();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                // For a little polish, specify a transition animation
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                // To make it fullscreen, use the 'content' root view as the container
                // for the fragment, which is always the root view for the activity
                //check to avoid adding fragments multiple times
                if( !categoryFragment.isAdded())
                transaction.add(android.R.id.content, categoryFragment)
                        .addToBackStack("category").commit();
            }
        });

      // read distance from pref
        distanceSeekBar.setSelectedMaxValue(readDistanceFromPref());
        // set distance text view
        distanceTextView.setText(distanceSeekBar.getSelectedMaxValue().toString());
        // listener for distance seek bar
        distanceSeekBar.setOnRangeSeekBarChangeListener(new SingleThumbSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(SingleThumbSeekBar bar, Object minValue, Object maxValue) {

                // change distance text based on user action
                distanceTextView.setText(distanceSeekBar.getSelectedMaxValue().toString());
          }
        });

        // read max budget from pref
        budgetSeekbar.setSelectedMaxValue(readMaxBudgetFromPref());
        // read min budget from pref
        budgetSeekbar.setSelectedMinValue(readMinBudgetFromPref());
        // set budget text view
        budgetTextView.setText(budgetSeekbar.getSelectedMinValue().toString()+"-"+ budgetSeekbar.getSelectedMaxValue().toString());
        // listener for budget seek bar
        budgetSeekbar.setOnRangeSeekBarChangeListener(new RangeSeekBarNew.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBarNew bar, Object minValue, Object maxValue) {

                // change budget text based on user action
                budgetTextView.setText(budgetSeekbar.getSelectedMinValue().toString()+"-"+
                        budgetSeekbar.getSelectedMaxValue().toString());
            }
        });
      return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG,"onResume"+(String)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("category_text"));

        /*******
         *  set the category value from the pref , which was set in the category fragemnt 's on back press .
         */
        categoryTextView.setText((String)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("category_text"));
        /*******
         *  set the location value from the pref ,which was set in the location fragemnt 's on back press .
         */

        selectedLocation.setText((String)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("location_text"));
    }


    private Double readDistanceFromPref() {
        return (Double)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("distance");
    }


    private void saveDistanceToPref(Double distance) {
         SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                writeSearchRequest("distance",distance);
    }

    private Integer readMaxBudgetFromPref(){
        return (Integer)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("max_budget");
    }


    private void saveMaxBudgetToPref(Integer maxBudget){
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                writeSearchRequest("max_budget",maxBudget);
    }

    private Integer readMinBudgetFromPref(){
        return (Integer)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("min_budget");
    }

    private void saveMinBudgetToPref(Integer minBudget) {
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                writeSearchRequest("min_budget",minBudget);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG,"onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG,"onStop");
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.e(TAG,"onCancel");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e(TAG,"onDestroyView");
    }
}
