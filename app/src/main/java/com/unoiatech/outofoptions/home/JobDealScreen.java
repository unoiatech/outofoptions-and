package com.unoiatech.outofoptions.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.allenliu.badgeview.BadgeView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.JobProposalScreen;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.home.fragment.DealListFragment;
import com.unoiatech.outofoptions.home.fragment.JobsListFragment;
import com.unoiatech.outofoptions.home.fragment.filter.FilterDialogFragment;
import com.unoiatech.outofoptions.model.Notification;
import com.unoiatech.outofoptions.model.NotificationResponse;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.GetBadgeView;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.view.SlidingTabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 12/7/2017.
 */

public class JobDealScreen extends Fragment implements View.OnClickListener
{
    private Button filterButton;
    private RelativeLayout active_jobs;
    private ArrayList<Notification> activeJobs;
    private ArrayList<Notification> activeProposals;
    private ViewPager viewPager;
    SlidingTabLayout mSlidingTabLayout;
    BadgeView badgeView;
    private static String TAG="JobDEalScreen";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.job_deal_layout,container,false);
        
        //Initialise Views
        initViews(view);
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        //To Show Badges on Active Jobs Button
        getNotificationCount(user.getId());
        return view;
    }

    private void initViews(View view) {
        //filterButton=(Button) view.findViewById(R.id.filter_button);
        active_jobs=(RelativeLayout) view.findViewById(R.id.active_jobs);
        viewPager = (ViewPager)view.findViewById(R.id.pager);
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager(),2));

        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.custom_tabstrip_for_activejobs, R.id.custom_tabstrip_tv);
        mSlidingTabLayout.setDistributeEvenly(false);
        mSlidingTabLayout.setViewPager(viewPager);


        //OnClickListeners
        //filterButton.setOnClickListener(this);
        active_jobs.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
        HomeScreenActivity.showTabHost();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.active_jobs:
                Intent intent= new Intent(getActivity(), JobProposalScreen.class);
                getActivity().startActivity(intent);
                break;
            case R.id.filter_button:
                //showDialog();
                break;
        }
    }

    private void showDialog() {
        DialogFragment newFragment = FilterDialogFragment.newInstance();
        // newFragment.show(getSupportFragmentManager(), "dialog");

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        //Add Data To sent
        Bundle bundle = new Bundle();
        bundle.putString("view", "JobList");
        newFragment.setArguments(bundle);

        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        transaction.add(android.R.id.content, newFragment, "filter")
                .addToBackStack(null).commit();
    }
    public class MyAdapter extends FragmentStatePagerAdapter {
        int tabcount;
        public MyAdapter(FragmentManager fm, int childCount) {
            super(fm);
            this.tabcount=childCount;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    JobsListFragment f = new JobsListFragment();
                    Bundle b = new Bundle();
                    b.putString("type", "confirmed");
                    f.setArguments(b);
                    return f;

                case 1:
                    DealListFragment f1 = new DealListFragment();
                    Bundle b1 = new Bundle();
                    b1.putString("type", "pending");
                    f1.setArguments(b1);
                    return f1;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Jobs";

                case 1:
                    return "Deals";
            }
            return super.getPageTitle(position);
        }

        @Override
        public int getCount() {
            return tabcount;
        }
    }
    private void getNotificationCount(Long userId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationResponse> call= apiService.getCategorizedNotification(userId);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
            //    Log.e(TAG,"NotificationResponse"+" "+response.body().getJobNotifications().size()+" "+response.isSuccessful());
                if(response.isSuccessful()){
                    activeJobs= new ArrayList<Notification>();
                    activeProposals= new ArrayList<Notification>();
                    if(response.body().getJobNotifications().size()>=0){
                        for(int i=0;i<response.body().getJobNotifications().size();i++){
                            Log.e(TAG,"For");
                            if(response.body().getJobNotifications().get(i).getActivity().equals("CONFIRMED_PROPOSAL")||response.body().getJobNotifications().get(i).getActivity().equals("REFUSED_PROPOSAL")||response.body().getJobNotifications().get(i).getActivity().equals("COMPLETED_PROPOSAL")||response.body().getJobNotifications().get(i).getActivity().equals("ADDED_PROPOSAL")){
                                activeJobs.add(response.body().getJobNotifications().get(i));
                                Log.e(TAG,"IF"+activeJobs.size());
                            }
                            else if(response.body().getJobNotifications().get(i).getActivity().equals("CANCELLED_JOB")||response.body().getJobNotifications().get(i).getActivity().equals("ACCEPTED_PROPOSAL")||response.body().getJobNotifications().get(i).getActivity().equals("CONFIRMED_PROPOSAL")){
                                activeProposals.add(response.body().getJobNotifications().get(i));
                                Log.e("Else","Else"+" "+activeProposals.size());
                            }
                        }
                    }
                    setBadge(response.body().getJobUnreadCount());
                }else{
                    ApiError error= ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e(TAG,"OnFailure"+"  "+t.toString());
            }
        });
    }

    private void setBadge(String jobUnreadCount) {
        Log.e("Set_Badge","Set_Badge"+"  "+jobUnreadCount);
        if(badgeView!=null)
            badgeView.unbind();
        final Animation animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_bounce);
        badgeView= GetBadgeView.getBadgeAtLeft(getContext());

        //SetBadge Value
        badgeView.setBadgeCount(jobUnreadCount)
                .bind(active_jobs)
                .setAnimation(animBounce);
    }

    @Subscribe
    public void onEvent(DataReceivedEvent event){
        Log.e(TAG,"OnEvent"+"  ");
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        getNotificationCount(user.getId());
    }
}
