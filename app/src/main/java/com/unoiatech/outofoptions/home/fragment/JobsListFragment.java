package com.unoiatech.outofoptions.home.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.JobProposalScreen;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.home.JobDealScreen;
import com.unoiatech.outofoptions.home.fragment.adapter.JobAdapter;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.home.fragment.adapter.JobListResponse;
import com.unoiatech.outofoptions.home.fragment.filter.FilterDialogFragment;
import com.unoiatech.outofoptions.model.Notification;
import com.unoiatech.outofoptions.model.NotificationResponse;
import com.unoiatech.outofoptions.model.PageDetail;
import com.unoiatech.outofoptions.model.SearchRequest;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.ConnectionDetection;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.PlaceUtil;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by unoiaAndroid on 3/20/2017.
 */

public class JobsListFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRecyclerView;
    private JobAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final String TAG="JobsListFragment";
    private List<JobListResponse> jobListResponse ;
    private PageDetail pageDetail;
    private int currentPage=0;
    private GoogleApiClient mGoogleApiClient;
    private TextView noJobsTextView,badgeCount;
    private Location mLastLocation;
    private AVLoadingIndicatorView progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    User user;
    private RelativeLayout active_jobs;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG,"onCreateView");
        View view=inflater.inflate(R.layout.jobs_list_layout,container,false);

        // initialize views
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        noJobsTextView=(TextView) view.findViewById(R.id.no_jobs_text_view);
        progressBar=(AVLoadingIndicatorView)view.findViewById(R.id.progress);

        JobDealScreen parentFrag = (JobDealScreen)getParentFragment();
        Button filterButton=(Button)parentFrag.getView().findViewById(R.id.filter_button);


        //toolbar = (Toolbar) view.findViewById(R.id.toolbar);
       /* filterButton=(Button) view.findViewById(R.id.filter_button);
        active_jobs=(RelativeLayout) view.findViewById(R.id.active_jobs);*/
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        /*** Hide no job available layout***/
        noJobsTextView.setVisibility(View.INVISIBLE);

        /***** open Filter Fragment *****/
       filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mAdapter.setLoadMoreListener(null);
                // show  filter dialog
                 showDialog();
            }
        });

        /***   set up recycler View   ******/
        setmRecyclerView();

        /* Google Play Services Check       */
        if(isGooglePlayServicesAvailable(getContext())) {
        }

        /**** initialize GoogleAPI Client ***/
        if (mGoogleApiClient == null) {
            Log.e("GoogleApiClient","GoogleApiClient"+mGoogleApiClient);
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();

        // set parameters for the search job request from preferences
        setDefaultValues();

        swipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        SearchRequest searchRequest = SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                                                readSearchRequest();


                                        // fetch userId from preferences
                                        Log.e("onRefresh by SlidingUp", jobListResponse.size() + "");
                                        /*User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
                                        listJobs(searchRequest, user.getId());*/
                                    }
                                }
        );
        return view;
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        SearchRequest searchRequest = SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequest();

        // fetch userId from preferences
        Log.e("size of jobs list", jobListResponse.size() + "");
        user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        listJobs(searchRequest, user.getId());
    }

    void setmRecyclerView(){
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // mRecyclerView.setHasFixedSize(true);
        jobListResponse = new ArrayList<>();
        mAdapter = new JobAdapter(getActivity(),mRecyclerView,jobListResponse);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume"+"  "+mLastLocation);

        //reset pages
        currentPage=0;
        if (mLastLocation != null) {
           SearchRequest searchRequest = SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                    readSearchRequest();
            Log.e("size:jobs list onResume",jobListResponse.size()+"");
            // fetch userId from preferences
           user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            if (user != null&& ConnectionDetection.isInternetAvailable(getActivity())) {
                listJobs(searchRequest, user.getId());
            }else{
                Snackbar.make(getView(),getString(R.string.internet_connection_error),Snackbar.LENGTH_SHORT).show();
            }
        }

        mAdapter.setMoreDataAvailable(true);
        // add load more listener
        mAdapter.setLoadMoreListener(new JobAdapter.OnLoadMoreListener() {

            @Override
            public void onLoadMore() {
                Log.e(TAG, "onLoadMore");
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(getContext(), "runnable here", Toast.LENGTH_LONG).show();
                        Log.e(TAG, "outside if current Page " + currentPage);
                        Log.e(TAG, "outside if  total pages" + pageDetail.getTotalPages());
                        currentPage = currentPage + 1;
                        if (currentPage <= pageDetail.getTotalPages()) {
                            SearchRequest searchRequest = SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                                            readSearchRequest();
                            //searchRequest.setPageNo(jobListResponse.size() - chats);
                            searchRequest.setPageNo(currentPage);

                            // fetch userId from preferences
                            User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
                            loadMore(searchRequest, user.getId());// a method which requests remote data
                            Log.e(TAG, "current Page " + currentPage+searchRequest.getCategory()+" "+searchRequest.getRating()+" "+searchRequest.getLatitude()
                            +" "+searchRequest.getDistance()+" "+searchRequest.getLongitude()+"  "+searchRequest.getPageNo()+"  "+searchRequest.getMaxBudget()+"  "+searchRequest.getMinBudget());
                            Log.e(TAG, "total pages" + pageDetail.getTotalPages());
                        }
                    }
                });
            }
        });
    }

    // get jobs from server
    public void listJobs(SearchRequest searchRequest, final Long userId) {
        Log.e("searchRequest",searchRequest.getCategory().toArray().toString()+"\n"+
                searchRequest.getLatitude()+"\n"+
                searchRequest.getLongitude()+"\n"+
                searchRequest.getMaxBudget()+"\n"+
                searchRequest.getMinBudget()+"\n"+
                searchRequest.getRating()+"\n"+
                searchRequest.getPageNo()+"\n"+
                searchRequest.getDistance()+"\n");

        // clear previous response
        jobListResponse.clear();
        mAdapter.notifyDataChanged();

        // start animating progress bar
        progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<PageDetail> call = apiService.searchJobForUserId(searchRequest,userId);
        call.enqueue(new Callback<PageDetail>() {
            @Override
            public void onResponse(Call<PageDetail>call, Response<PageDetail> response) {
                Log.e(TAG,"Response "+response.body()+"");
                swipeRefreshLayout.setRefreshing(false);

                //server response
                if(response!=null) {
                    pageDetail =response.body();
                List<JobListResponse> result=response.body().getContent();
                    Log.e(TAG,result.size()+"");
                   /* for(JobListResponse jlr:result) {
                        jlr.setType("job");
                    }*/

                    // update response list
                    jobListResponse.addAll(result);
                    // notify adapter about the list update
                    mAdapter.notifyDataChanged();

                    // stop animating progress bar
                    progressBar.setVisibility(View.INVISIBLE);

                    /***  Show "no jobs layout " if no jobs is found matching the user's criteria  ***/
                    if(result.size()==0) {
                        noJobsTextView.setVisibility(View.VISIBLE);
                    }
                    else{
                        noJobsTextView.setVisibility(View.INVISIBLE);
                    }
                 }
                else {
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getView(), error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PageDetail>call, Throwable t) {
                // stop animating progress bar if some error occurs
                Snackbar.make(getView(),"Server error",Snackbar.LENGTH_SHORT);
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG+"failure",t.toString());
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /***  Function to handle pagination *****/
    private void loadMore(SearchRequest searchRequest, Long userId){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        jobListResponse.add(new JobListResponse("load"));
        mAdapter.notifyItemInserted(jobListResponse.size()-1);
        //mAdapter.notifyDataChanged();

        Call<PageDetail> call = apiService.searchJobForUserId(searchRequest,userId);
        call.enqueue(new Callback<PageDetail>() {
            @Override
            public void onResponse(Call<PageDetail> call, Response<PageDetail> response) {
                if(response.isSuccessful()){
                    Log.e(TAG,"Response"+response.body());
                    //remove loading view
                    jobListResponse.remove( jobListResponse.size()-1);
                    pageDetail=response.body();
                    List<JobListResponse> result = response.body().getContent();
                    if(result.size()>0){
                        Log.e("Response_Successful","Response_Successful"+result.size());
                       /* //add loaded data
                        for(JobListResponse jlr:result){
                            jlr.setType("job");
                        }*/
                        jobListResponse.addAll(result);
                    }
                    else{
                        Log.e(TAG,"Response_Else");
                        //result size 0 means there is no more data available at server
                        mAdapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
                        //Toast.makeText(getContext(),"No More Data Available", Toast.LENGTH_LONG).show();
                    }
                    mAdapter.notifyDataChanged();
                }
                else{

                }
            }

            @Override
            public void onFailure(Call<PageDetail> call, Throwable t) {
                Log.e(TAG," Load More Response Error "+t.getMessage());
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.e(TAG, "onViewCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        Log.e(TAG, "onStart");
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach");
    }

    public void setDefaultValues() {
        // set distance here as default not works for , doubel to long conversion
        if(((Double)(SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("distance")))==0.0){

           /* Log.d("distance sent ",""+((Double)(SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                    readSearchRequestByKey("distance"))));*/
            SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("distance", 200.0);
        }

        //if category empty in preference , pass new array list with size 0
        if(((ArrayList<String>)(SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
            readSearchRequestByKey("category")))==null)
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("category",
                new ArrayList<String>());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("OnConnected","Onconnected"+"  "+mLastLocation);
        try {
            if(mLastLocation==null) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    // update location values in the shared preferences
                    updateLocation();
                    SearchRequest searchRequest = SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                            readSearchRequest();

                    // fetch userId from preferences
                    Log.e("size of jobs list", jobListResponse.size() + "");
                    User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
                    if (user != null&&ConnectionDetection.isInternetAvailable(getActivity())) {
                        listJobs(searchRequest, user.getId());
                    }else{
                        Snackbar.make(getView(),getString(R.string.internet_connection_error),Snackbar.LENGTH_SHORT).show();
                    }
                }
                else {
                    showLocationSettingsAlert();
                }
            }else{
                Log.e("Else","eLse");
            }
        }
        catch (SecurityException e) {
           showLocationSettingsAlert();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG,"onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG,"onConnectionFailed");
    }

    public boolean isGooglePlayServicesAvailable(Context context){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);
        return resultCode == ConnectionResult.SUCCESS;
    }


    public void showLocationSettingsAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder
                .setMessage("GPS is disabled in your device. Enable it?")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                getActivity().startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    void showDialog() {
        DialogFragment newFragment = FilterDialogFragment.newInstance();
        // newFragment.show(getSupportFragmentManager(), "dialog");

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        //Add Data To sent
        Bundle bundle= new Bundle();
        bundle.putString("view","JobList");
        newFragment.setArguments(bundle);

        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        transaction.add(android.R.id.content, newFragment,"filter")
                .addToBackStack(null).commit();
    }

    void updateLocation() {
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("latitude",
                mLastLocation.getLatitude());

        //  set longitude
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("longitude",
                mLastLocation.getLongitude());

        //set Location text
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).writeSearchRequest("location_text",
                PlaceUtil.getCompleteAddressString(mLastLocation.getLatitude(),mLastLocation.getLongitude(),getContext()));
    }
}

