package com.unoiatech.outofoptions.home.fragment.adapter;

import android.support.v4.app.FragmentActivity;

import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.User;

import java.io.Serializable;
import java.util.List;

/**
 * Created by unoiaAndroid on 3/21/2017.
 */

public class JobListResponse implements Serializable {
    String image;
    String userName;
    Job job;
    User user;
    private String average_review;
    private List seller_reviews;
    private List buyer_reviews;
    public String type;

    //Getter Setter
    public String getAverage_review() {
        return average_review;
    }

    public void setAverage_review(String average_review) {
        this.average_review = average_review;
    }

    public List getSeller_reviews() {
        return seller_reviews;
    }

    public void setSeller_reviews(List seller_reviews) {
        this.seller_reviews = seller_reviews;
    }

    public List getBuyer_reviews() {
        return buyer_reviews;
    }

    public void setBuyer_reviews(List buyer_reviews) {
        this.buyer_reviews = buyer_reviews;
    }

    public JobListResponse(String type)
    {
        this.type=type;
    }

    public String getImageName() {
            return image;
        }

        public void setImageName(String imageName) {
            this.image = imageName;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public Job getJob() {
            return job;
        }

        public void setJob(Job job) {
            this.job = job; }

        public User getUser() {
            return user;
        }


    public void setUser(User user) {
            this.user = user;
        }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

