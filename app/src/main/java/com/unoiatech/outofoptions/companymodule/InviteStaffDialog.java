package com.unoiatech.outofoptions.companymodule;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 7/26/2017.
 */

public class InviteStaffDialog extends DialogFragment implements View.OnClickListener {
    private static String TAG = "InviteStaffDialog";
    Dialog dialog;
    EditText emailEdit;
    TextView sendInvite;
    OnDialogFinishListener listener;
    public static final String EMAIL_PATTERN = "[^\\s@]+@[^\\s@]+\\.[a-zA-z][a-zA-Z][a-zA-Z]*";
    AVLoadingIndicatorView progressBar;

    public static DialogFragment newInstance() {
        InviteStaffDialog frag = new InviteStaffDialog();
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invite_staff_member_dialog, container, false);
        sendInvite = (TextView) view.findViewById(R.id.invite);
        TextView staticText=(TextView)view.findViewById(R.id.static_text);
        ImageView cancelImage=(ImageView)view.findViewById(R.id.cancel_image);
        emailEdit=(EditText)view.findViewById(R.id.email_address);
        progressBar=(AVLoadingIndicatorView)view.findViewById(R.id.progress_bar);

        //set Text on other terms
        Spannable wordtoSpan=new SpannableString(getString(R.string.invite_staff_static_text));
        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), 0,5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        staticText.setText(wordtoSpan);

        //Communication with Activity using Interface
        listener = (OnDialogFinishListener) getActivity();

        sendInvite.setOnClickListener(this);
        cancelImage.setOnClickListener(this);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.invite:
                if(!emailEdit.getText().toString().isEmpty()&& emailEdit.getText().toString().matches(EMAIL_PATTERN)) {
                    dismissKeyboard();
                    sendInviteToStaff(emailEdit.getText().toString());
                }
                else {
                    if (emailEdit.getText().toString().isEmpty()){
                        dismissKeyboard();
                        listener.onFinishEditDialog(getActivity().getString(R.string.empty_email_address));
                    }
                    else{
                        dismissKeyboard();
                        listener.onFinishEditDialog(getActivity().getString(R.string.invalid_email_address));
                    }
                }
                break;

            case R.id.cancel_image:
                dialog.dismiss();
                break;
        }
    }

    private void dismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void sendInviteToStaff(String emailAddress) {
        progressBar.setVisibility(View.VISIBLE);
        sendInvite.setText("");
        Company company= new Company();
        company.setEmail(emailAddress);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        User user=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        Call<Void> call = apiService.inviteStaffMember(user.getId(),company);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                progressBar.setVisibility(View.GONE);
                Log.d(TAG, "Response" + response + " " + response.body()+ " "+response.isSuccessful());
                    if (response.isSuccessful()) {
                        dialog.dismiss();
                        listener.onFinishEditDialog(getActivity().getString(R.string.success_text));
                    }
                    else{
                        dialog.dismiss();
                        final ApiError error = ErrorUtils.parseError(response);
                        listener.onFinishEditDialog(error.getMessage());
                    }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "Error" + t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
