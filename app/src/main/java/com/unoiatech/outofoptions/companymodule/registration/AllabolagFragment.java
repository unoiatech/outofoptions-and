package com.unoiatech.outofoptions.companymodule.registration;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.companymodule.SelectedViewCallback;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 7/5/2017.
 */

public class AllabolagFragment extends Fragment implements View.OnClickListener {
    private static String TAG="AllabolagFragment";
    private SelectedViewCallback mCallback;
    private FragmentTabHost mTabhost;
    private TextView fetchInfoButton;
    private EditText org_number,companyName;
    AVLoadingIndicatorView progressBar;
    private ImageView orgImage,companyLogo;
    private String orgNumber;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = context instanceof Activity ? (Activity) context : null;
        try {
            mCallback=(SelectedViewCallback)context;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAccountSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.allabolag_layout,container,false);
        mTabhost=(FragmentTabHost)getActivity().findViewById(R.id.fragment_tabhost);
        LinearLayout cancelImage=(LinearLayout)getActivity().findViewById(R.id.cancel_image);

        org_number=(EditText)view.findViewById(R.id.org_num_edit_text);
        orgImage=(ImageView)view.findViewById(R.id.org_img);
        fetchInfoButton=(TextView) view.findViewById(R.id.fetch_info_button);
        companyLogo=(ImageView)view.findViewById(R.id.company_logo);
        companyName=(EditText) view.findViewById(R.id.company_name);
        progressBar=(AVLoadingIndicatorView)view.findViewById(R.id.progress_bar);

        //set Text, Drawable and Backgrounds
        orgImage.setImageResource(R.drawable.org_no_grey_img);
        companyLogo.setImageResource(R.drawable.company_logo_grey);
        fetchInfoButton.setText(getString(R.string.allabolag_btn_text));
        fetchInfoButton.setBackgroundResource(R.drawable.grey_ripple_effect);
        setOrganisationNumber(org_number,orgImage,fetchInfoButton);

        cancelImage.setOnClickListener(this);
        return view;
    }

    //TextWatcher to Add limit of 10 digits on Organisation Number EditText
    private void setOrganisationNumber(EditText org_number, final ImageView orgImage, final TextView fetchInfoButton) {
        org_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==10){
                    orgImage.setImageResource(R.drawable.org_no_green_img);
                    fetchInfoButton.setBackgroundResource(R.drawable.ripple_effect);
                    fetchInfoButton.setOnClickListener(AllabolagFragment.this);
                }
                else if(editable.length()<10){
                    orgImage.setImageResource(R.drawable.org_no_grey_img);
                    fetchInfoButton.setBackgroundResource(R.drawable.grey_ripple_effect);
                    fetchInfoButton.setOnClickListener(null);
                    fetchInfoButton.setText(getString(R.string.allabolag_btn_text));
                    if(!companyName.getText().toString().isEmpty()){
                    companyName.setText("");}
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("OnActivity_Created","OnActivity_Created");
    }

    @Override
    public void onResume() {
        super.onResume();
        String orgNumber = ((CompanyRegistration)getActivity()).getOrgNumber();
        String businessName=((CompanyRegistration)getActivity()).getBusinessName();
        try{
        if(!orgNumber.isEmpty()&&orgNumber!=null) {
            org_number.setText(orgNumber);
            fetchInfoButton.setText(getString(R.string.allabolag_next));
        }
        if(!businessName.isEmpty()&&businessName!=null){
            companyName.setText(businessName);
        }
        Log.e("OnResume","OnResume"+orgNumber+"  "+businessName);}
        catch (NullPointerException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fetch_info_button:
                fetchInfoOnclick(fetchInfoButton);
                break;
            case R.id.cancel_image:
                getActivity().finish();
                break;
        }
    }

    private void fetchInfoOnclick(TextView fetchInfoButton) {
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        if(fetchInfoButton.getText().toString().equals(getString(R.string.allabolag_btn_text))){

            //Hide Soft Keyboard
            InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null :
                    getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            //Fetch Data from Allabolag
            fetchInfoFromAllabolag(user.getId(),org_number.getText().toString());
        }
        else if(fetchInfoButton.getText().toString().equals(getString(R.string.allabolag_next))){
            //Set Company Details
            Company companyData= Company.getInstance();
            companyData.setBusinessName(companyName.getText().toString());
            companyData.setOrgNumber(orgNumber);

            //Open next fragment on ViewPager
            mTabhost.setCurrentTab(1);
            mCallback.unselectedTabView();
            mCallback.allabolagData(orgNumber,companyName.getText().toString());
        }
    }

    private void fetchInfoFromAllabolag(Long userId, final String organisationNumber) {
        progressBar.setVisibility(View.VISIBLE);
        fetchInfoButton.setText("");
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Company> call=apiService.fetchInfoFromAllabolag(organisationNumber,userId);
        call.enqueue(new Callback<Company>() {
            @Override
            public void onResponse(Call<Company> call, Response<Company> response) {
                Log.d(TAG,"Response"+response.errorBody()+" message "+response.message()+" response "+response);
                if(response.isSuccessful()) {
                    companyName.setText(response.body().getBusinessName());
                    orgNumber=response.body().getOrgNumber();
                    progressBar.setVisibility(View.GONE);
                    fetchInfoButton.setText(getString(R.string.allabolag_next));
                    companyLogo.setImageResource(R.drawable.company_logo_green);
                }
                else {
                    //Handle Response Error
                    ApiError error = ErrorUtils.parseError(response);
                    progressBar.setVisibility(View.GONE);
                    fetchInfoButton.setText(getString(R.string.allabolag_btn_text));
                    Log.d(TAG, "Error Body" + error.toString()+"  message"+error.getMessage());
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT)
                            .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                        }
                                    }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
                }
            }

            @Override
            public void onFailure(Call<Company> call, Throwable t) {
                Log.e(TAG,"REsponse"+t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
