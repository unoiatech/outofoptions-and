package com.unoiatech.outofoptions.companymodule;

/**
 * Created by Unoiatech on 7/28/2017.
 */

public interface OnDialogFinishListener {
    void onFinishEditDialog(String inputText);
}
