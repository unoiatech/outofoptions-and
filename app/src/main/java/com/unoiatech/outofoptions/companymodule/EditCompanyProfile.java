package com.unoiatech.outofoptions.companymodule;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.EditAboutScreen;
import com.unoiatech.outofoptions.fragments.menufragments.EditProfileScreen;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.activities.MyFileContentProvider;
import com.wang.avi.AVLoadingIndicatorView;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 7/25/2017.
 */
public class EditCompanyProfile extends AppCompatActivity implements View.OnClickListener {
    public static final int GET_FROM_GALLERY = 1;
    public static final int GET_FROM_CAMERA = 2;
    private static final String TAG = "EditCompanyProfile";
    Uri selectedImage;
    ImageView profile_Image;
    private TextView tagline, about;
    private Bundle bundle;
    File avatar;
    AVLoadingIndicatorView progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_company_profile);

        //Action Bar Components
        TextView mTitle = (TextView) findViewById(R.id.title);
        TextView save = (TextView) findViewById(R.id.edit);
        save.setVisibility(View.VISIBLE);

        profile_Image = (ImageView) findViewById(R.id.company_image);
        TextView editImage = (TextView) findViewById(R.id.edit_image);
        tagline = (TextView) findViewById(R.id.tagline);
        about = (TextView) findViewById(R.id.about);
        progressBar = (AVLoadingIndicatorView) findViewById(R.id.progress_bar);

        mTitle.setText(R.string.edit_profile_text);
        save.setText(R.string.edit_profile_save);

        editImage.setOnClickListener(this);
        save.setOnClickListener(this);
        about.setOnClickListener(this);
        setData(profile_Image, tagline, about);
    }

    private void setData(ImageView profile_image, TextView tagline, TextView about) {
        bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.getString("tagline").equals("-")) {
                tagline.setHint(R.string.edit_tagline_hint);
            } else {
                tagline.setText(bundle.getString("tagline"));
            }
            if (bundle.getString("about").equals("-")) {
                about.setHint(R.string.add_about_text);
            } else {
                about.setText(bundle.getString("about"));
            }
            Glide.with(this).load(S3Utils.getCompanylogo(bundle.getLong("company_id"), bundle.getString("logo"))).into(profile_image);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "OnResume");
        String aboutText = (String) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("about_data");
        if (!aboutText.isEmpty()) {
            about.setText(aboutText);
        } else {
            about.setText(about.getText().toString());
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit_image:
                InputMethodManager inputmanager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputmanager.hideSoftInputFromInputMethod(view.getApplicationWindowToken(), 0);
                showDialog();
                break;
            case R.id.edit:
                dismissKeyboard();
                saveCompanyProfileData();
                break;
            case R.id.about:
                Intent intent = new Intent(EditCompanyProfile.this, EditAboutScreen.class);
                intent.putExtra("about", about.getText().toString());
                startActivity(intent);
                break;
        }
    }

    private void saveCompanyProfileData() {
        progressBar.setVisibility(View.VISIBLE);
        Company data = new Company();
        data.setTagline(tagline.getText().toString());
        data.setAbout(about.getText().toString());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.editCompanyProfile(bundle.getLong("company_id"), data);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, "Response" + response.isSuccessful());
                if (selectedImage != null) {
                    uploadUserImage(getRealPathFromURI(selectedImage), bundle.getLong("company_id"));
                } else {
                    progressBar.setVisibility(View.GONE);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "FAilure" + t.toString());
            }
        });
    }

    private void uploadUserImage(String imagePath, Long companyId) {
        //Create Upload Server Client
        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        avatar = new File(imagePath);


        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatar);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", avatar.getName(), requestFile);

        Log.e("Data", "Data" + body + "  " + body + " " + imagePath);
        Call<Void> resultCall = service.addCompanyLogo(body, companyId);
        resultCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG, "Error" + t.toString());
            }
        });
    }

    private void dismissKeyboard() {
        //dismiss keyboard
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void showDialog() {
        View view = getLayoutInflater().inflate(R.layout.add_images_dialog_layout, null);
        TextView fromGallery = (TextView) view.findViewById(R.id.gallery);
        TextView fromCamera = (TextView) view.findViewById(R.id.camera);
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        final Dialog mBottomSheetDialog = new Dialog(EditCompanyProfile.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        fromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                        GET_FROM_GALLERY);
            }
        });
        fromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getApplicationContext().getPackageManager();
                mBottomSheetDialog.dismiss();
                if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                    startActivityForResult(i, GET_FROM_CAMERA);
                } else {
                    Toast.makeText(EditCompanyProfile.this, "Camera is not available", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Detects request codes
        if (requestCode == GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            selectedImage = data.getData();
            Bitmap bitmap;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(EditCompanyProfile.this.getContentResolver(), data.getData());
                profile_Image.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (requestCode == GET_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            Log.i("DATA", "Receive the camera result");
            File out = new File(getApplicationContext().getFilesDir(), "newImage.jpg");
            if (!out.exists()) {
                Toast.makeText(EditCompanyProfile.this,
                        "Error while capturing image", Toast.LENGTH_LONG)
                        .show();
                return;
            }
            Bitmap mBitmap = BitmapFactory.decodeFile(out.getAbsolutePath());
            selectedImage = getImageUri(getApplicationContext(), mBitmap);
            profile_Image.setImageBitmap(mBitmap);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        Log.e("getImageUri", "getImageUri");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).removePostedJobLocationData();
    }
}

