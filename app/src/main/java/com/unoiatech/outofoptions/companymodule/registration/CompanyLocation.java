package com.unoiatech.outofoptions.companymodule.registration;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 7/14/2017.
 */

public class CompanyLocation extends AppCompatActivity implements OnMapReadyCallback,View.OnClickListener{
    private MapView mapView;
    private GoogleMap mMap;
    private String ret,locationText;
    private Double latitude, longitude;
    private TextView placeAutoComplete;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static String TAG = "CompanyLocation";
    private LatLng location;
    private Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_location_layout);
        mapView = (MapView) findViewById(R.id.map);
        placeAutoComplete = (TextView) findViewById(R.id.places_autocomplete);
        Button setLocation=(Button)findViewById(R.id.set_location_button);
        setLocation.setOnClickListener(this);

        //Initialize Map View
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(CompanyLocation.this.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(this);
        // start google's autocomplete widget
        placeAutoComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(CompanyLocation.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    Log.e(TAG, "GooglePlayServicesRepairableException" + e.toString());
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                    Log.e(TAG, "GooglePlayServicesNotAvailableException" + e.toString());
                }
            }
        });
    }

    //GEt Place Api Result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                locationText = place.getName().toString();
                placeAutoComplete.setText(locationText);
                setMap(mMap, latitude, longitude);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e("OnMap_ready", "OnMap_Ready" + googleMap);
        mMap = googleMap;

        //move camera to current location
        Bundle bundle=getIntent().getExtras();
        LatLng origin=new LatLng(bundle.getDouble("lat"),bundle.getDouble("lng"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));

        //get location when map is scroll
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.e("Camera postion change" + "", cameraPosition.target.latitude + " " + cameraPosition.target.longitude + "  " + cameraPosition);
                Double lat = cameraPosition.target.latitude;
                Double lng = cameraPosition.target.longitude;
                if (!lat.equals(0.0) && !lng.equals(0.0))
                    getAddress(lat,lng);
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    /*******Get Address from Latitude n Longitude
     * @param mlat***************/
    private String getAddress(Double mlat, Double mlng) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(mlat, mlng, 1);
            Log.e("Get_Address", "Get_Address" +mlat+" "+mlng+"  "+addresses.size());
            if (addresses != null&&addresses.size()>0) {
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
               /* String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i));//.append("\n");
                }*/

                latitude=addresses.get(0).getLatitude();
                longitude=addresses.get(0).getLongitude();
                Log.e("State","State"+state+"  country"+country+latitude+longitude);
                if(state!=null){
                    locationText=state+","+country;
                }else{
                    locationText=country;
                }
                placeAutoComplete.setText(locationText);
            } else {
                ret = "No Address returned!";
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ret = "Can't get Address!";
        }
        return ret;
    }

    private void setMap(GoogleMap map, final Double mlatitude, final Double mlongitude) {
        if (map != null) {
            mMap = map;
            Log.e(TAG, " (map != null called");
            location = null;
            if (marker != null) {
                marker.remove();
            }
            location = new LatLng(mlatitude, mlongitude);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15));

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    private void saveMapData(Double latitude, Double longitude, String locationText) {
        //save latitude
        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveJobLocationData("lat",latitude);

        //save Longitude
        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveJobLocationData("lng",longitude);

        //save Place
        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveJobLocationData("place",locationText);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.set_location_button:
                saveMapData(latitude,longitude,locationText);
                finish();
        }
    }
}
