package com.unoiatech.outofoptions.companymodule.registration;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.companymodule.SelectedViewCallback;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 7/5/2017.
 */

public class CompanyRegistration extends FragmentActivity implements SelectedViewCallback {
    private FragmentTabHost mTabHost;
    private ImageView firstTickImage, secondTickImage, thirdTickImage;
    private TextView firstView, secondView, thirdView;
    private Button secondButton, thirdButton;
    private String orgNumber,businessName;
    private Double latitude,longitude;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_registration);
        mTabHost = (FragmentTabHost) findViewById(R.id.fragment_tabhost);
        firstTickImage = (ImageView) findViewById(R.id.first_tick);
        secondTickImage = (ImageView) findViewById(R.id.second_tick);
        thirdTickImage = (ImageView) findViewById(R.id.third_tick);

        secondButton = (Button) findViewById(R.id.secondButton);
        thirdButton = (Button) findViewById(R.id.thirdButton);

        firstView = (TextView) findViewById(R.id.firstview);
        secondView = (TextView) findViewById(R.id.second_line);
        thirdView = (TextView) findViewById(R.id.third_line);
        getLocation();

        /*****Adding Tabs*******/
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.addTab(mTabHost.newTabSpec("allabolag").setIndicator(""), AllabolagFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("company_details").setIndicator(""), CompanyDetailsFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("submit").setIndicator(""), DetailSubmitFragment.class, null);
        mTabHost.setCurrentTab(0);
        firstView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void selectedTabView() {
        int index = mTabHost.getCurrentTab();
        Log.e("Index", "Index" + index);
        if (index == 0) {
            firstTickImage.setVisibility(View.GONE);
            firstTickImage.setBackgroundResource(R.drawable.round_tick);
            firstView.setVisibility(View.VISIBLE);

            secondButton.setTextColor(getResources().getColor(R.color.unselected_menu));
            secondView.setVisibility(View.GONE);
        }
        else if (index == 1) {
            secondTickImage.setVisibility(View.GONE);
            secondTickImage.setBackgroundResource(R.drawable.round_tick);
            secondView.setVisibility(View.VISIBLE);

            thirdButton.setTextColor(getResources().getColor(R.color.unselected_menu));
            thirdView.setVisibility(View.GONE);
        }
    }

    //Get Current Location of User
    private void getLocation() {
        GPSTracker gpsTracker= new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            latitude= gpsTracker.getLatitude();
            longitude= gpsTracker.getLongitude();
        }
        else {
            gpsTracker.showSettingsAlert();
        }
    }

    @Override
    public void unselectedTabView() {
        int index = mTabHost.getCurrentTab();
        Log.e("Unselected", "unselected" + index);
        if (index == 1) {
            firstTickImage.setVisibility(View.VISIBLE);
            firstTickImage.setBackgroundResource(R.drawable.round_tick);
            firstView.setVisibility(View.GONE);

            secondButton.setTextColor(getResources().getColor(android.R.color.white));
            secondView.setVisibility(View.VISIBLE);
        }
        else if (index == 2) {
            secondTickImage.setVisibility(View.VISIBLE);
            secondTickImage.setBackgroundResource(R.drawable.round_tick);
            secondView.setVisibility(View.GONE);

            thirdButton.setTextColor(getResources().getColor(android.R.color.white));
            thirdView.setVisibility(View.VISIBLE);
        }
        else if (index == 3) {
            thirdTickImage.setVisibility(View.VISIBLE);
            thirdTickImage.setBackgroundResource(R.drawable.round_tick);
            thirdView.setVisibility(View.GONE);

            thirdButton.setTextColor(getResources().getColor(android.R.color.white));
            thirdView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void allabolagData(String orgNumber, String businessName) {
        setOrgNumber(orgNumber);
        setBusinessName(businessName);
    }

    //Getter Setter for OrgNumber and BusinessName and Pass value of Latitude n longitude to fragments
    public String getOrgNumber(){
        return orgNumber;
    }

    public void setOrgNumber(String orgNumber) {
        this.orgNumber = orgNumber;
    }

    public String getBusinessName(){
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Double getLatitude(){
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).removePostedJobLocationData();
    }
}
