package com.unoiatech.outofoptions.companymodule.registration;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.companymodule.SelectedViewCallback;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.activities.MyFileContentProvider;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 7/14/2017.
 */

public class DetailSubmitFragment extends Fragment implements View.OnClickListener {
    SelectedViewCallback mCallback;
    FragmentTabHost mTabHost;
    CheckBox confirmCheck;
    private LinearLayout upload_logo;
    private static final int GET_FROM_GALLERY = 3;
    private static  final  int GET_FROM_CAMERA=2;
    private Uri selectedImage;
    private Bitmap bitmap;
    private CircleImageView companyLogo;
    private File avatar;
    private AVLoadingIndicatorView progressBar;
    private static String TAG="DetailSubmitFragment";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback=(SelectedViewCallback)context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.submit_cmpny_detail_layout,container,false);

        mTabHost=(FragmentTabHost)getActivity().findViewById(R.id.fragment_tabhost);
        LinearLayout cancelImage=(LinearLayout)getActivity().findViewById(R.id.cancel_image);
        confirmCheck=(CheckBox)view.findViewById(R.id.confirm);
        upload_logo=(LinearLayout)view.findViewById(R.id.upload_logo);
        companyLogo=(CircleImageView)view.findViewById(R.id.company_logo);
        TextView companyName=(TextView)view.findViewById(R.id.company_name);
        progressBar=(AVLoadingIndicatorView)view.findViewById(R.id.progress_bar);

        companyName.setText(((CompanyRegistration)getActivity()).getBusinessName());

        //Back and Next Button
        RelativeLayout backButton=(RelativeLayout)view.findViewById(R.id.back_button);
        Button submitButton=(Button)view.findViewById(R.id.submit_btn);

        backButton.setOnClickListener(this);
        submitButton.setOnClickListener(this);
        cancelImage.setOnClickListener(this);
        upload_logo.setOnClickListener(this);
        if(bitmap!=null){
            companyLogo.setImageBitmap(bitmap);
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_btn:
                if (confirmCheck.isChecked()){
                    registerCompanyDetails();
                }
                else {
                   notifyUser(getString(R.string.confirm_detail_text));
                }
                mCallback.unselectedTabView();
                break;
            case R.id.back_button:
                mTabHost.setCurrentTab(1);
                mCallback.selectedTabView();
                break;
            case R.id.cancel_image:
                getActivity().finish();
                break;
            case R.id.upload_logo:
                showDialog();
                break;
        }
    }

    private void showDialog() {
        View view = getActivity().getLayoutInflater().inflate (R.layout.add_images_dialog_layout, null);
        TextView fromGallery = (TextView)view.findViewById( R.id.gallery);
        TextView fromCamera = (TextView)view.findViewById( R.id.camera);
        TextView cancel=(TextView)view.findViewById(R.id.cancel);
        final Dialog addImageDialog = new Dialog(getActivity(), R.style.Theme_Dialog);
        addImageDialog.setContentView (view);
        addImageDialog.setCancelable(true);
        addImageDialog.show();

        /************** Listeners on Dialog**********/
        cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addImageDialog.dismiss();
                }
            });
        fromGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {addImageDialog.dismiss();
                    startActivityForResult(new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                            GET_FROM_GALLERY);
                }
        });
        fromCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {PackageManager pm = getActivity().getPackageManager();
                    addImageDialog.dismiss();
                    if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                        startActivityForResult(i, GET_FROM_CAMERA);
                    }
                    else {
                        notifyUser("Camera is not Available");
                    }
                }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("OnActivityResult","OnActivityResult"+data.getData());
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                companyLogo.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if(requestCode==GET_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            File out = new File(getContext().getFilesDir(), "newImage.jpg");
            if(!out.exists()) {
                notifyUser("Error while capturing image");
                return;
            }
            bitmap= BitmapFactory.decodeFile(out.getAbsolutePath());
            selectedImage=getImageUri(getContext(),bitmap);
            companyLogo.setImageBitmap(bitmap);
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] imageBytes = bytes.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        //String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(encodedImage);
    }

    private void notifyUser(String message) {
        Snackbar.make(getView(),message,Snackbar.LENGTH_SHORT)
                .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    private void registerCompanyDetails() {
        progressBar.setVisibility(View.VISIBLE);
        Company companyData= Company.getInstance();
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Company> call=apiService.registerCompanyData(companyData,user.getId());
        call.enqueue(new Callback<Company>() {
            @Override
            public void onResponse(Call<Company> call, Response<Company> response) {
                Log.d(TAG,"Response"+response.body().getId());
                if(selectedImage!=null){
                    uploadUserImage(getRealPathFromURI(selectedImage),response.body().getId());
                }
                else{
                    progressBar.setVisibility(View.GONE);
                    ShowRegistrationDialog();
                }
            }

            @Override
            public void onFailure(Call<Company> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void ShowRegistrationDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        View view1 = getActivity().getLayoutInflater().inflate(R.layout.company_registration_request_dialog, null);
        dialogBuilder.setView(view1);
        TextView ok_btn = (TextView) view1.findViewById(R.id.okay_btn);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void uploadUserImage(String imagePath, Long companyId) {
        //Create Upload Server Client
        ApiInterface service =   ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        avatar = new File(imagePath);


        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatar);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", avatar.getName(), requestFile);

        Log.e("Data","Data"+body+"  "+body+" "+imagePath);
        Call<Void> resultCall = service.addCompanyLogo(body,companyId);
        resultCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                progressBar.setVisibility(View.GONE);
                if(response.isSuccessful()){
                    ShowRegistrationDialog();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG,"Error"+t.toString());
            }
        });
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
