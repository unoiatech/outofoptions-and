package com.unoiatech.outofoptions.model;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 6/8/2017.
 */

public class DraftResponse {
    private String title;
    private String description;
    private String category;
    private Long  userId;
    private Long id;
    private int budget;
    private Long createdAt;
    private Long timeline;
    private Double lat;
    private Double lng;
    private String place;
    private ArrayList<Job.Images> draftsImages;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }
    public void setPlace(String place) {
        this.place = place;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Long getTimeline() {
        return timeline;
    }

    public void setTimeline(Long timeline) {
        this.timeline = timeline;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public ArrayList<Job.Images> getDraftsImages() {
        return draftsImages;
    }

    public void setDraftsImages(ArrayList<Job.Images> draftsImages) {
        this.draftsImages = draftsImages;
    }
}
