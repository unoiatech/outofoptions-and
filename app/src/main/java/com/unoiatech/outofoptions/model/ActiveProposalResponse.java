package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 7/10/2017.
 */

public class ActiveProposalResponse
{
    private Job job;
    private Proposal proposal;
    private User user;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
