package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 7/12/2017.
 */

public class RequestProposalData
{
    private String time;
    private String response;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
