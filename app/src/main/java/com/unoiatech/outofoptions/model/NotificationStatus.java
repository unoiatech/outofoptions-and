package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 5/10/2017.
 */

public class NotificationStatus {
    private String doesProposalReceived;
    private String doesJobCompleteResponse;
    private String doesReceiveResponse;
    private String doesJobCancelledResponse;
    private String doesProposalCompleteResponse;

    public String getDoesProposalReceived() {
        return doesProposalReceived;
    }

    public void setDoesProposalReceived(String doesProposalReceived) {
        this.doesProposalReceived = doesProposalReceived;
    }

    public String getDoesJobCompleteResponse() {
        return doesJobCompleteResponse;
    }

    public void setDoesJobCompleteResponse(String doesJobCompleteResponse) {
        this.doesJobCompleteResponse = doesJobCompleteResponse;
    }

    public String getDoesReceiveResponse() {
        return doesReceiveResponse;
    }

    public void setDoesReceiveResponse(String doesReceiveResponse) {
        this.doesReceiveResponse = doesReceiveResponse;
    }

    public String getDoesJobCancelledResponse() {
        return doesJobCancelledResponse;
    }

    public void setDoesJobCancelledResponse(String doesJobCancelledResponse) {
        this.doesJobCancelledResponse = doesJobCancelledResponse;
    }

    public String getDoesProposalCompleteResponse() {
        return doesProposalCompleteResponse;
    }

    public void setDoesProposalCompleteResponse(String doesProposalCompleteResponse) {
        this.doesProposalCompleteResponse = doesProposalCompleteResponse;
    }
}
