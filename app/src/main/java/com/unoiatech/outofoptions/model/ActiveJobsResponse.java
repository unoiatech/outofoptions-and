package com.unoiatech.outofoptions.model;

import java.util.List;

/**
 * Created by Unoiatech on 4/17/2017.
 */
public class ActiveJobsResponse {
    private Job job;
    private User user;
    private List<String> companyProposals;
    private Proposal proposal;


    public List<String> getCompanyProposals() {
        return companyProposals;
    }

    public void setCompanyProposals(List<String> companyProposals) {
        this.companyProposals = companyProposals;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

   public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Job getJob() {
        return job;
    }
    public void setJob(Job job) {
        this.job = job;
    }
}
