package com.unoiatech.outofoptions.model;

/**
 * Created by unoiaAndroid on 4/4/2017.
 */

public class VerifyBankIdResponse {
    private String autoStartToken;
    private String orderRef;
    private String verifiedId;

    public String getAutoStartToken() {
        return autoStartToken;
    }

    public void setAutoStartToken(String autoStartToken) {
        this.autoStartToken = autoStartToken;
    }

    public String getOrderRef() {
        return orderRef;
    }

    public void setOrderRef(String orderRef) {
        this.orderRef = orderRef;
    }

    public String getVerifiedId() {
        return verifiedId;
    }

    public void setVerifiedId(String verifiedId) {
        this.verifiedId = verifiedId;
    }
}
