package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 9/8/2017.
 */

public class Customer {
    private String source;

    //Getter And Setter
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
