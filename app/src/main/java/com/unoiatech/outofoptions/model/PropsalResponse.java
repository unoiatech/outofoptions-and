package com.unoiatech.outofoptions.model;


import java.util.ArrayList;

/**
 * Created by Unoiatech on 4/26/2017.
 */
public class PropsalResponse
{
    //REcieved Proposals data
    private ArrayList<PropsalResponse> proposals;
    private Job job;

    //Recieved Proposals Detail
    private String average_review;
    private ArrayList<Reviews> seller_reviews;
    private ArrayList<Reviews> buyer_reviews;
    private User user;
    private Proposal proposal;

    //GEtter Setter

    public ArrayList<PropsalResponse> getProposals() {
        return proposals;
    }

    public void setProposals(ArrayList<PropsalResponse> proposals) {
        this.proposals = proposals;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }


    public String getAverage_review() {
        return average_review;
    }

    public void setAverage_review(String average_review) {
        this.average_review = average_review;
    }


    public ArrayList<Reviews> getSeller_reviews() {
        return seller_reviews;
    }

    public void setSeller_reviews(ArrayList<Reviews> seller_reviews) {
        this.seller_reviews = seller_reviews;
    }

    public ArrayList<Reviews> getBuyer_reviews() {
        return buyer_reviews;
    }

    public void setBuyer_reviews(ArrayList<Reviews> buyer_reviews) {
        this.buyer_reviews = buyer_reviews;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
