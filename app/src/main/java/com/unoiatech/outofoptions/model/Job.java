package com.unoiatech.outofoptions.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by unoiaAndroid on 3/14/2017.
 */

public class Job implements Parcelable {

    private Long id;
    private String title;
    private String category;
    private String description;
    private Double lat;
    private Double lng;
    private int budget;
    private String place;
    private Long timeline;
    private Long postingTime;
    private Long userId;
    private int isExpired;
    private Long completedTime;
    private Long hiringTime;
    private Long cancellationTime;
    private String jobStatus;
    private ArrayList<Images> images;
    private ArrayList<Proposal> proposals;
    private String startingTime;
    private String endingTime;
    private String preCancellationHours;
    private String whoCanSendProposal;

    public Job(){

    }

    public Job(Parcel in) {
        id=in.readLong();
        title = in.readString();
        timeline=in.readLong();
        budget = in.readInt();
        postingTime=in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeLong(timeline);
        dest.writeInt(budget);
        if(postingTime!=null){
        dest.writeLong(postingTime);}
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Job> CREATOR = new Creator<Job>() {
        @Override
        public Job createFromParcel(Parcel in) {
            return new Job(in);
        }

        @Override
        public Job[] newArray(int size) {
            return new Job[size];
        }
    };

    public String getPreCancellationHours() {
        return preCancellationHours;
    }

    public void setPreCancellationHours(String preCancellationHours) {
        this.preCancellationHours = preCancellationHours;
    }

    public String getWhoCanSendProposal() {
        return whoCanSendProposal;
    }

    public void setWhoCanSendProposal(String whoCanSendProposal) {
        this.whoCanSendProposal = whoCanSendProposal;
    }

    public String getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(String startingTime) {
        this.startingTime = startingTime;
    }

    public String getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(String endingTime) {
        this.endingTime = endingTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Long getTimeline() {
        return timeline;
    }

    public void setTimeline(Long timeline) {
        this.timeline = timeline;
    }

    public Long getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(Long postingTime) {
        this.postingTime = postingTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(int isExpired) {
        this.isExpired = isExpired;
    }

    public Long getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(Long completedTime) {
        this.completedTime = completedTime;
    }

    public Long getHiringTime() {
        return hiringTime;
    }

    public void setHiringTime(Long hiringTime) {
        this.hiringTime = hiringTime;
    }

    public Long getCancellationTime() {
        return cancellationTime;
    }

    public void setCancellationTime(Long cancellationTime) {
        this.cancellationTime = cancellationTime;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public ArrayList<Images> getImages() {
        return images;
    }

    public void setImages(ArrayList<Images> images) {
        this.images = images;
    }

    public ArrayList<Proposal> getProposals() {
        return proposals;
    }

    public void setProposals(ArrayList<Proposal> proposals) {
        this.proposals = proposals;
    }

    public static class Images implements Parcelable {
        private Long id;
        private String name;

        protected Images(Parcel in) {
            name = in.readString();
            id=in.readLong();
        }

        public static final Creator<Images> CREATOR = new Creator<Images>() {
            @Override
            public Images createFromParcel(Parcel in) {
                return new Images(in);
            }

            @Override
            public Images[] newArray(int size) {
                return new Images[size];
            }
        };

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(name);
            parcel.writeLong(id);
        }
    }
}
