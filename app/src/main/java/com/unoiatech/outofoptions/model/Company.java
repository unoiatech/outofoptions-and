package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 7/14/2017.
 */

public class Company {
    private Long id;
    private static Company object;
    private String orgNumber;
    private String businessName;
    private String bankgiro;
    private String phoneNumber;
    private String swishNumber;
    private String email;
    private String locationName;
    private Double lat;
    private Double lng;
    private String logo;
    private String about;
    private String tagline;
    private String userRole;
    private Long createdAt;
    private String companyStatus;
    private String benterprise;
    private String bstatus;
    private String ownerName;
    private String bsector;
    private String fskatt;
    private String registrationYear;
    private User user;

    /****Parameters to Request Cancel registration request***********/
    private Long response;
    private String details;



    public Company() {
       // left blank intentionally
    }
    public static Company getInstance() {
        if (object == null)
            object = new Company();
        return object;
    }

    //Getter and Setter
    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Long getResponse() {
        return response;
    }

    public void setResponse(Long response) {
        this.response = response;
    }

    public String getBenterprise() {
        return benterprise;
    }

    public void setBenterprise(String benterprise) {
        this.benterprise = benterprise;
    }

    public String getBstatus() {
        return bstatus;
    }

    public void setBstatus(String bstatus) {
        this.bstatus = bstatus;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getBsector() {
        return bsector;
    }

    public void setBsector(String bsector) {
        this.bsector = bsector;
    }

    public String getFskatt() {
        return fskatt;
    }

    public void setFskatt(String fskatt) {
        this.fskatt = fskatt;
    }

    public String getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(String registrationYear) {
        this.registrationYear = registrationYear;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logoImage) {
        this.logo = logoImage;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getOrgNumber() {
        return orgNumber;
    }

    public void setOrgNumber(String orgNumber) {
        this.orgNumber = orgNumber;
    }

    public String getBankgiro() {
        return bankgiro;
    }

    public void setBankgiro(String bankgiro) {
        this.bankgiro = bankgiro;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSwishNumber() {
        return swishNumber;
    }

    public void setSwishNumber(String swishNumber) {
        this.swishNumber = swishNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
