package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 10/13/2017.
 */

public class SellerTransaction {
    private String swishNumber;
    private Long amountRequested;
    private Long userId;

    //Getter Setter

    public Long getAmountRequested() {
        return amountRequested;
    }

    public void setAmountRequested(Long amountRequested) {
        this.amountRequested = amountRequested;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSwishNumber() {
        return swishNumber;
    }

    public void setSwishNumber(String swishNumber) {
        this.swishNumber = swishNumber;
    }
}
