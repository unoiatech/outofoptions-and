package com.unoiatech.outofoptions.model;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 5/9/2017.
 */

public class NotificationResponse {
    private String totalPages;
    private ArrayList<NotificationResponse> content;
    Notification notification;
    User user;
    public String type;
    private String jobUnreadCount;
    private ArrayList<Notification> jobNotifications;

    public NotificationResponse(String load) {
        this.type=load;
    }

    public NotificationResponse() {
    }

    public String getJobUnreadCount() {
        return jobUnreadCount;
    }

    public void setJobUnreadCount(String jobUnreadCount) {
        this.jobUnreadCount = jobUnreadCount;
    }

    public ArrayList<Notification> getJobNotifications() {
        return jobNotifications;
    }

    public void setJobNotifications(ArrayList<Notification> jobNotifications) {
        this.jobNotifications = jobNotifications;
    }

    public Notification getNotification(){
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<NotificationResponse> getContent() {
        return content;
    }

    public void setContent(ArrayList<NotificationResponse> content) {
        this.content = content;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }
}
