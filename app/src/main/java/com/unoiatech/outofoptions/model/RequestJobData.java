package com.unoiatech.outofoptions.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Unoiatech on 7/11/2017.
 */

public class RequestJobData implements Parcelable
{
    private Long jobId;
    private Long time;
    private String response;


    public RequestJobData(Parcel in) {
    }

    public static final Creator<RequestJobData> CREATOR = new Creator<RequestJobData>() {
        @Override
        public RequestJobData createFromParcel(Parcel in) {
            return new RequestJobData(in);
        }

        @Override
        public RequestJobData[] newArray(int size) {
            return new RequestJobData[size];
        }
    };

    public RequestJobData() {

    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
