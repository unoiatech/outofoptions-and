package com.unoiatech.outofoptions.model;

import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;

import de.measite.minidns.record.A;

/**
 * Created by Unoiatech on 11/22/2017.
 */

public class ShopData {
    private Long id;
    private String name;
    private String category;
    private String location;
    private String phoneNumber;
    private String website;
    private String about;
    private Double lat;
    private Double lng;
    private Long userId;
    private Uri[] imageUri;
    private ArrayList<BusinessHour> businessHourss;
    private ShopData shopDto;

    private static ShopData instance= null;

    public static ShopData getInstance() {
        if (instance == null)
            instance = new ShopData();
        return instance;
    }

    //Getter Setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public ArrayList<BusinessHour> getBusinessHourss() {
        return businessHourss;
    }

    public  void setBusinessHourss(ArrayList<BusinessHour>  businessHourss) {
        this.businessHourss = businessHourss;
    }

    public Uri[] getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri[] imageUri) {
        this.imageUri = imageUri;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public ShopData getShopDto() {
        return shopDto;
    }

    public void setShopDto(ShopData shopDto) {
        this.shopDto = shopDto;
    }
    /*public class BusinessHour {
        private String closeTime;
        private String openTime;
        private int dayOfWeek;

        //Getter Setter

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        public String getOpenTime() {
            return openTime;
        }

        public void setOpenTime(String openTime) {
            this.openTime = openTime;
        }

        public int getDayOfWeek() {
            return dayOfWeek;
        }

        public void setDayOfWeek(int dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }
    }*/
}
