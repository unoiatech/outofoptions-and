package com.unoiatech.outofoptions.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by unoiaAndroid on 4/24/2017.
 */

public class UserRating implements Serializable{

    private Float average_review;

    List<SellerReviewWithUserInfo> seller_reviews= new ArrayList<>();

    List<BuyerReviewWithUserInfo> buyer_reviews= new ArrayList<>();

    public Float getAverage_review() {
        return average_review;
    }

    public void setAverage_review(Float average_review) {
        this.average_review = average_review;
    }

    public List<SellerReviewWithUserInfo> getSeller_reviews() {
        return seller_reviews;
    }

    public void setSeller_reviews(List<SellerReviewWithUserInfo> seller_reviews) {
        this.seller_reviews = seller_reviews;
    }

    public List<BuyerReviewWithUserInfo> getBuyer_reviews() {
        return buyer_reviews;
    }

    public void setBuyer_reviews(List<BuyerReviewWithUserInfo> buyer_reviews) {
        this.buyer_reviews = buyer_reviews;
    }

   /*public class SellerReviewWithUserInfo{

        private User user;
        private Review review;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Review getReview() {
            return review;
        }

        public void setReview(Review review) {
            this.review = review;
        }
    }
*/
    /*public class BuyerReviewWithUserInfo{

        private User user;
        private BuyerReview review;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public BuyerReview getReview() {
            return review;
        }

        public void setReview(BuyerReview review) {
            this.review = review;
        }
    }*/
}
