package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 4/24/2017.
 */

public class AcceptProposal {
    private Long proposalId;
    private String time;
    private String response;

    public Long getProposalId() {
        return proposalId;
    }

    public void setProposalId(Long proposalId) {
        this.proposalId = proposalId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}
