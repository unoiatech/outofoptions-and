package com.unoiatech.outofoptions.model;

/**
 * Created by unoiaAndroid on 4/20/2017.
 */

public class JobWithUser {

    private Job job;
    private User user;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
