package com.unoiatech.outofoptions.model;
import com.unoiatech.outofoptions.home.fragment.adapter.JobListResponse;
import java.util.List;

/**
 * Created by unoiaAndroid on 4/27/2017.
 */

public class PageDetail {
    private Integer number;
    private Integer numberOfElements=0;
    private Integer size;
    private Boolean last;
    private Integer totalPages;
    private List<JobListResponse> content;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(Integer numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public Boolean getLast() {
        return last;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public List<JobListResponse> getContent() {
        return content;
    }

    public void setContent(List<JobListResponse> content) {
        this.content = content;
    }
}
