package com.unoiatech.outofoptions.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Unoiatech on 4/5/2017.
 */
public class JobImage implements Parcelable
{
    private String id;
    private String name;

    public JobImage(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public static final Creator<JobImage> CREATOR = new Creator<JobImage>() {
        @Override
        public JobImage createFromParcel(Parcel in) {
            return new JobImage(in);
        }

        @Override
        public JobImage[] newArray(int size) {
            return new JobImage[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }
}
