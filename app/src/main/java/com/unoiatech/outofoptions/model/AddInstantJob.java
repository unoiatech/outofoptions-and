package com.unoiatech.outofoptions.model;

import java.util.ArrayList;
import java.util.function.LongToDoubleFunction;

/**
 * Created by Unoiatech on 5/31/2017.
 */

public class AddInstantJob {
    private String title;
    private String category;
    private String description;
    private Double lat;
    private Double lng;
    private int budget;
    private String place;
    private String timeline;
    private String postingTime;
    private Long userId;
    private String jobStatus;
    private ArrayList<Images> images;
    private Long time;
    private String startingTime;
    private String endingTime;
    private String whoCanSendProposal;
    private String preCancellationHours;
    private String createdAt;

    //Add Proposal
    private String completionHours;
    private String offeredQuote;
    private String proposalDescription;
    private Long userID;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getWhoCanSendProposal() {
        return whoCanSendProposal;
    }

    public void setWhoCanSendProposal(String whoCanSendProposal) {
        this.whoCanSendProposal = whoCanSendProposal;
    }

    public String getPreCancellationHours() {
        return preCancellationHours;
    }

    public void setPreCancellationHours(String preCancellationHours) {
        this.preCancellationHours = preCancellationHours;
    }

    public String getOfferedQuote() {
        return offeredQuote;
    }

    public void setOfferedQuote(String offeredQuote) {
        this.offeredQuote = offeredQuote;
    }

    public String getProposalDescription() {
        return proposalDescription;
    }

    public void setProposalDescription(String proposalDescription) {
        this.proposalDescription = proposalDescription;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getCompletionHours() {
        return completionHours;
    }

    public void setCompletionHours(String completionHours) {
        this.completionHours = completionHours;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTimeline() {
        return timeline;
    }

    public void setTimeline(String timeline) {
        this.timeline = timeline;
    }

    public String  getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(String postingTime) {
        this.postingTime = postingTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public ArrayList<Images> getImages() {
        return images;
    }

    public void setImages(ArrayList<Images> images) {
        this.images = images;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(String startingTime) {
        this.startingTime = startingTime;
    }

    public String getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(String endingTime) {
        this.endingTime = endingTime;
    }

    public class Images
    {
        private Long id;
        private String name;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


}
