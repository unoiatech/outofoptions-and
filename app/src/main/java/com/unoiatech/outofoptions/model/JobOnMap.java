package com.unoiatech.outofoptions.model;


import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Unoiatech on 6/30/2017.
 */

public  class JobOnMap implements ClusterItem
{
    private String category;
    private int budget;
    private String description;
    private Long postingTime;
    private String jobTitle;
    private Double lat;
    private Double lng;
    private Long timeline;
    private Long jobId;
    private Long buyerId;
   private LatLng mPosition;
    private int reviews;
    private String imagePath;
    private String rating;

    public JobOnMap(LatLng position, String category, Long timeline, String title, int budget, Long postingTime, int reviews, String rating, String imagePath, Long userId,Long jobId) {
        Log.e("JobMap","JObMap"+category+" "+title);
        this.budget=budget;
        this.postingTime=postingTime;
        mPosition=position;
        this.category=category;
        this.timeline=timeline;
        this.jobTitle=title;
        this.rating=rating;
        this.reviews=reviews;
        this.imagePath=imagePath;
        this.buyerId=userId;
        this.jobId=jobId;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    //getTer  Setter
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public Long getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(Long postingTime) {
        this.postingTime = postingTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Long getTimeline() {
        return timeline;
    }

    public void setTimeline(Long timeline) {
        this.timeline = timeline;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public LatLng getmPosition() {
        return mPosition;
    }

    public void setmPosition(LatLng mPosition) {
        this.mPosition = mPosition;
    }

    public int getReviews() {
        return reviews;
    }

    public void setReviews(int reviews) {
        this.reviews = reviews;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
