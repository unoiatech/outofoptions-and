package com.unoiatech.outofoptions.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by unoiaAndroid on 3/14/2017.
 */

public class Proposal {
    private Long id;
    private Long userID;
    private int offeredQuote;
    private String proposalStatus;
    private Long offeredTimeline;
    private String proposalDescription;
    private Long postingTime;
    private Long completionTime;
    private Long confirmationTime;
    private Long rejectionTime;
    private Long startingTime;
    private int isRead;
    private  User user;
   /* private  Job job;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }*/

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCompletionHours() {
        return completionHours;
    }

    public void setCompletionHours(String completionHours) {
        this.completionHours = completionHours;
    }

    public String getGetCompletionMinutes() {
        return getCompletionMinutes;
    }

    public void setGetCompletionMinutes(String getCompletionMinutes) {
        this.getCompletionMinutes = getCompletionMinutes;
    }

    private String completionHours;
    private String getCompletionMinutes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public int getOfferedQuote() {
        return offeredQuote;
    }

    public void setOfferedQuote(int offeredQuote) {
        this.offeredQuote = offeredQuote;
    }

    public String getProposalStatus() {
        return proposalStatus;
    }

    public void setProposalStatus(String proposalStatus) {
        this.proposalStatus = proposalStatus;
    }

    public Long getOfferedTimeline() {
        return offeredTimeline;
    }

    public void setOfferedTimeline(Long offeredTimeline) {
        this.offeredTimeline = offeredTimeline;
    }

    public String getProposalDescription() {
        return proposalDescription;
    }

    public void setProposalDescription(String proposalDescription) {
        this.proposalDescription = proposalDescription;
    }

    public Long getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(Long postingTime) {
        this.postingTime = postingTime;
    }

    public Long getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(Long completionTime) {
        this.completionTime = completionTime;
    }

    public Long getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Long confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    public Long getRejectionTime() {
        return rejectionTime;
    }

    public void setRejectionTime(Long rejectionTime) {
        this.rejectionTime = rejectionTime;
    }

    public Long getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Long startingTime) {
        this.startingTime = startingTime;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public Proposal(){}
   /* protected Proposal(Parcel in) {
        offeredQuote = in.readInt();
        if(completionTime!=null){
        completionTime =in.readLong();}
        startingTime = in.readLong();
        postingTime = in.readLong();
        proposalDescription = in.readString();
        userID = in.readLong();
        id=in.readLong();
        proposalStatus = in.readString();
        if(offeredTimeline!=null)
        {
            offeredTimeline = in.readLong();
        }
    }

    public static final Creator<Proposal> CREATOR = new Creator<Proposal>() {
        @Override
        public Proposal createFromParcel(Parcel in) {
            return new Proposal(in);
        }

        @Override
        public Proposal[] newArray(int size) {
            return new Proposal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(offeredQuote);
        if(completionTime!=null){
        dest.writeLong(completionTime);}
        dest.writeLong(startingTime);
        dest.writeLong(postingTime);
        dest.writeString(proposalDescription);
        dest.writeLong(userID);
        dest.writeLong(id);
        dest.writeString(proposalStatus);
        if(offeredTimeline!=null){
            dest.writeLong(offeredTimeline);
        }
    }*/
}
