package com.unoiatech.outofoptions.model;


/**
 * Created by Unoiatech on 3/31/2017.
 */
public class Reviews {
    private String skills;
    private Long revieweeId;
    private Job jobs;
    private String qualityOfWork;
    private String reviewTime;
    private String communication;
    private String descriptin;
    private String stars;
    private String punctuality;
    private Long reviewerId;
    private String hireAgain;
    private String isJobCompleted;
    private Long jobId;


    //Getter Setter
    public Long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(Long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public Long getJobId()
    {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getSkills ()
    {
        return skills;
    }

    public void setSkills (String skills)
    {
        this.skills = skills;
    }

    public Long getRevieweeId ()
    {
        return revieweeId;
    }

    public void setRevieweeId (Long revieweeId) {this.revieweeId = revieweeId; }

    public Job getJobs ()
    {
        return jobs;
    }

    public void setJobs (Job jobs)
    {
        this.jobs = jobs;
    }

    public String getQualityOfWork ()
    {
        return qualityOfWork;
    }

    public void setQualityOfWork (String qualityOfWork)
    {
        this.qualityOfWork = qualityOfWork;
    }

    public String getReviewTime ()
    {
        return reviewTime;
    }

    public void setReviewTime (String reviewTime)
    {
        this.reviewTime = reviewTime;
    }

    public String getCommunication ()
    {
        return communication;
    }

    public void setCommunication (String communication)
    {
        this.communication = communication;
    }

    public String getDescriptin ()
    {
        return descriptin;
    }

    public void setDescriptin (String descriptin)
    {
        this.descriptin = descriptin;
    }

    public String getStars ()
    {
        return stars;
    }

    public void setStars (String stars)
    {
        this.stars = stars;
    }

    public String getPunctuality ()
    {
        return punctuality;
    }

    public void setPunctuality (String punctuality)
    {
        this.punctuality = punctuality;
    }

    public String getHireAgain ()
    {
        return hireAgain;
    }

    public void setHireAgain (String hireAgain)
    {
        this.hireAgain = hireAgain;
    }

    public String getIsJobCompleted ()
    {
        return isJobCompleted;
    }

    public void setIsJobCompleted (String isJobCompleted)
    {
        this.isJobCompleted = isJobCompleted;
    }

   /*@Override
    public String toString()
    {
        return "ClassPojo [jobs = "+jobs+", revieweeId = "+revieweeId+",skills = "+skills+" , qualityOfWork = "+qualityOfWork+", reviewTime = "+reviewTime+", communication = "+communication+", descriptin = "+descriptin+", stars = "+stars+", punctuality = "+punctuality+", reviewerId = "+reviewerId+", hireAgain = "+hireAgain+", isJobCompleted = "+isJobCompleted+"]";
    }*/
}
