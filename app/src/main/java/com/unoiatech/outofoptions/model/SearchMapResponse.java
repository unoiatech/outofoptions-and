package com.unoiatech.outofoptions.model;

import java.util.List;

/**
 * Created by unoiaAndroid on 4/27/2017.
 */

public class SearchMapResponse {
    private Job job;
    private User user;
    private String average_review;
    private List seller_reviews;
    private List buyer_reviews;
    private String rating;
    private int reviews;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getReviews() {
        return reviews;
    }

    public void setReviews(int reviews) {
        this.reviews = reviews;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAverage_review() {
        return average_review;
    }

    public void setAverage_review(String average_review) {
        this.average_review = average_review;
    }

    public List getSeller_reviews() {
        return seller_reviews;
    }

    public void setSeller_reviews(List seller_reviews) {
        this.seller_reviews = seller_reviews;
    }

    public List getBuyer_reviews() {
        return buyer_reviews;
    }

    public void setBuyer_reviews(List buyer_reviews) {
        this.buyer_reviews = buyer_reviews;
    }
}
