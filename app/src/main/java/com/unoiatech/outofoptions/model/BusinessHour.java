package com.unoiatech.outofoptions.model;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 11/22/2017.
 */

public class BusinessHour extends ArrayList<BusinessHour> {
    private String closeTime;
    private String openTime;
    private int dayOfWeek;

    //Getter Setter

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}
