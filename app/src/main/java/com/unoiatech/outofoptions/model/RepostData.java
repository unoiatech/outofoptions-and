package com.unoiatech.outofoptions.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.security.PrivateKey;
import java.util.ArrayList;

/**
 * Created by Unoiatech on 6/9/2017.
 */

public class RepostData implements Parcelable{
    private String title;
    private String description;
    private Long timeline;
    private int budget;
    private Double lat;
    private Double lng;
    private String place;
    private Long userId;
    private Long id;
    private String preCancellationHours;
    private String whoCanSendProposal;

    //constnt field to check job status
    private String jobStatus;
    private ArrayList<Job.Images> images;


    public  RepostData(){

    }
    //Parcelable implementation
    protected RepostData(Parcel in) {
        title=in.readString();
        description=in.readString();
        budget=in.readInt();
        timeline=in.readLong();
        lat=in.readDouble();
        lng=in.readDouble();
        images=in.readArrayList(Job.Images.class.getClassLoader());
        place=in.readString();
        preCancellationHours=in.readString();
        whoCanSendProposal=in.readString();
        jobStatus=in.readString();
        userId=in.readLong();
        id=in.readLong();
    }

    public static final Creator<RepostData> CREATOR = new Creator<RepostData>() {
        @Override
        public RepostData createFromParcel(Parcel in) {
            return new RepostData(in);
        }

        @Override
        public RepostData[] newArray(int size) {
            return new RepostData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeInt(budget);
        parcel.writeLong(timeline);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeList(images);
        parcel.writeString(place);
        parcel.writeString(preCancellationHours);
        parcel.writeString(whoCanSendProposal);
        parcel.writeString(jobStatus);
        parcel.writeLong(userId);
        parcel.writeLong(id);
    }

    //getter setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public ArrayList<Job.Images> getImages() {
        return images;
    }

    public void setImages(ArrayList<Job.Images> images) {
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTimeline() {
        return timeline;
    }

    public void setTimeline(Long timeline) {
        this.timeline = timeline;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPreCancellationHours() {
        return preCancellationHours;
    }

    public void setPreCancellationHours(String preCancellationHours) {
        this.preCancellationHours = preCancellationHours;
    }

    public String getWhoCanSendProposal() {
        return whoCanSendProposal;
    }

    public void setWhoCanSendProposal(String whoCanSendProposal) {
        this.whoCanSendProposal = whoCanSendProposal;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }
}
