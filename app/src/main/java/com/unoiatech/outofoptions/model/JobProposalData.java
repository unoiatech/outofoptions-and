package com.unoiatech.outofoptions.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Unoiatech on 7/11/2017.
 */

public class JobProposalData implements Parcelable
{
    private String jobTitle;
    private String proposalDescription;
    private String sellerName;
    private Long sellerId;
    private Long proposalId;
    private Long proposalStartingTime;
    private String viewId;
    private int budget;
    private int offeredQuote;
    private Long jobId;
    private Double lat;
    private Double lng;
    private String category;
    private Long userId;

    private Long proposalCompletionTime;
    private String proposalStatus;

    public String getProposalStatus() {
        return proposalStatus;
    }

    public void setProposalStatus(String proposalStatus) {
        this.proposalStatus = proposalStatus;
    }

    public Long getProposalCompletionTime() {
        return proposalCompletionTime;
    }

    public void setProposalCompletionTime(Long proposalCompletionTime) {
        this.proposalCompletionTime = proposalCompletionTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getProposalDescription() {
        return proposalDescription;
    }

    public void setProposalDescription(String proposalDescription) {
        this.proposalDescription = proposalDescription;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getProposalId() {
        return proposalId;
    }

    public void setProposalId(Long proposalId) {
        this.proposalId = proposalId;
    }

    public Long getProposalStartingTime() {
        return proposalStartingTime;
    }

    public void setProposalStartingTime(Long proposalStartingTime) {
        this.proposalStartingTime = proposalStartingTime;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public int getOfferedQuote() {
        return offeredQuote;
    }

    public void setOfferedQuote(int offeredQuote) {
        this.offeredQuote = offeredQuote;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public JobProposalData(Parcel in) {
        jobId=in.readLong();
        jobTitle = in.readString();
        viewId = in.readString();
        lat=in.readDouble();
        lng=in.readDouble();
        category=in.readString();
        sellerId=in.readLong();
        proposalDescription = in.readString();
        proposalId=in.readLong();
        proposalStartingTime=in.readLong();
        offeredQuote=in.readInt();
        sellerName=in.readString();
        userId=in.readLong();
        proposalCompletionTime=in.readLong();
        proposalStatus=in.readString();
    }

    public static final Creator<JobProposalData> CREATOR = new Creator<JobProposalData>() {
        @Override
        public JobProposalData createFromParcel(Parcel in) {
            return new JobProposalData(in);
        }

        @Override
        public JobProposalData[] newArray(int size) {
            return new JobProposalData[size];
        }
    };

    public JobProposalData() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(jobId);
        parcel.writeString(jobTitle);
        parcel.writeString(viewId);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeString(category);
        parcel.writeLong(sellerId);
        parcel.writeString(proposalDescription);
        parcel.writeLong(proposalId);
        parcel.writeLong(proposalStartingTime);
        parcel.writeInt(offeredQuote);
        parcel.writeString(sellerName);
        parcel.writeLong(userId);
        try{
        parcel.writeLong(proposalCompletionTime);}
        catch (NullPointerException ex){
            ex.printStackTrace();
        }
        parcel.writeString(proposalStatus);
    }
}
