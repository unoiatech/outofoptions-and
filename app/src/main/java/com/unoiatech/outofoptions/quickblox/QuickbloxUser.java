package com.unoiatech.outofoptions.quickblox;

import android.content.Context;
import android.os.Bundle;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.QBSettings;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.QuickbloxConstants;
import java.io.File;
import java.io.InputStream;

/**
 * Created by unoiaAndroid on 5/4/2017.
 */

public class QuickbloxUser {
    static  QuickbloxUser quickbloxUser;

    private  QuickbloxUser()
    {
    }
    public static QuickbloxUser getInstance()
    {
        quickbloxUser= new QuickbloxUser();
        return quickbloxUser;
    }

    public void initializeSession(Context context){
        QBSettings.getInstance().init(context, QuickbloxConstants.APP_ID, QuickbloxConstants.AUTH_KEY,
                QuickbloxConstants.AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(QuickbloxConstants.ACCOUNT_KEY);
    }

    //Create session
    public void createSession() {
        QBAuth.createSession(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                // You have successfully created the session
                //
                // Now you can use QuickBlox API!
            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }

    //Create new User (registration)
    public void signup(User o3user) {
        final QBUser user = new QBUser();
        user.setExternalId(String.valueOf(o3user.getId()));
        user.setEmail(o3user.getEmail());
        user.setFullName(o3user.getFirstName() + " " + o3user.getLastName());
        user.setPassword(o3user.getUserPassword());

        QBUsers.signUp(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }

    //Sign In with email & password
    public void signIn(User o3User) {
        QBUser user = new QBUser();
        user.setEmail(o3User.getEmail());
        user.setPassword(o3User.getUserPassword());

        QBUsers.signIn(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle params) {

            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }


    //Update own profile
    public void updateProfile(User o3user, Integer qbId) {
        QBUser user = new QBUser();
        user.setId(qbId);
        user.setFullName(o3user.getFirstName() + " " + o3user.getLastName());
        user.setEmail(o3user.getEmail());

        QBUsers.updateUser(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {

            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }

    //Update profile picture (avatar)
    public void uploadProfilePic(File avatar, final Integer quickbloxUserId) {



// Upload new avatar to Content module
        Boolean fileIsPublic = false;

        QBContent.uploadFileTask(avatar, fileIsPublic, null, new QBEntityCallback<QBFile>() {
            @Override
            public void onSuccess(QBFile qbFile, Bundle params) {

                int uploadedFileID = qbFile.getId();

                // Connect image to user
                QBUser user = new QBUser();
                user.setId(quickbloxUserId);
                user.setFileId(uploadedFileID);

                QBUsers.updateUser(user, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser user, Bundle args) {

                    }

                    @Override
                    public void onError(QBResponseException errors) {

                    }
                });
            }

            @Override
            public void onError(QBResponseException errors) {

            }
        }, new QBProgressCallback() {
            @Override
            public void onProgressUpdate(int progress) {

            }
        });
    }


    //download profile picture (avatar)
    public void downloadProfilePic(QBUser qbuser) {
        int userProfilePictureID = qbuser.getFileId(); // user - an instance of QBUser class

        QBContent.downloadFileById(userProfilePictureID, new QBEntityCallback<InputStream>() {
            @Override
            public void onSuccess(InputStream inputStream, Bundle params) {

            }

            @Override
            public void onError(QBResponseException errors) {

            }
        }, new QBProgressCallback() {
            @Override
            public void onProgressUpdate(int progress) {

            }
        });
    }


    //Remove User's session (logout)
 /*  public void signOut()
    {
        QBUsers.signOut(new QBEntityCallback(){
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(QBResponseException errors) {

            }
        });
    }*/



}
