package com.unoiatech.outofoptions;

import com.unoiatech.outofoptions.util.Foreground;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by unoiaAndroid on 3/15/2017.
 */

public class Application extends android.app.Application  {
    private static Application instance;

    @Override
    public void onCreate() {
        instance = this;
        //Add Support for Custom font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Light.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        Foreground.init(this);
    }

    public static synchronized Application getInstance() {
        return instance;
    }
}
